package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TempInviteesActivity extends AppCompatActivity implements Button.OnClickListener {
    private ListView listView;
    private LinearLayout button;
    List<Phonebook> contacts = Collections.emptyList();
    TempInviteeAdapter adapter;
    String TAG;
    private FirebaseAnalytics analytics;

    int REQUEST_INVITE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitees_list);

        findViewsById();

        contacts = new ArrayList<>();

        TAG = getIntent().getClass().getSimpleName();

        adapter = new TempInviteeAdapter(this, contacts);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);

        button.setOnClickListener(this);

        ConnectionDetector connectionDetector = new ConnectionDetector(TempInviteesActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetInvitees(TempInviteesActivity.this).execute();

        } else {
            Toast.makeText(TempInviteesActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                List<String> selected = getSelectedItems();
                String logString = "Selected items: " + TextUtils.join(", ", selected);

                Log.d("MainActivity", logString);
                //Toast.makeText(this, logString, Toast.LENGTH_SHORT).show();

                //this.finish();

                //onInviteClicked();
                //shareShortDynamicLink();
                shareLongDynamicLink();
                //shareDynamicLink();

                break;
        }
    }

    private void findViewsById() {
        listView = (ListView) findViewById(R.id.recyler_view);
        button = (LinearLayout) findViewById(R.id.button);
    }

    private List<String> getSelectedItems() {
        List<String> result = new ArrayList<>();

            SparseBooleanArray checkedItems = listView.getCheckedItemPositions();

                for (int i = 0; i < checkedItems.size(); ++i) {
                    if (checkedItems.valueAt(i)) {

                        Phonebook phn = (Phonebook) listView.getItemAtPosition(checkedItems.keyAt(i));
                        result.add(phn.getPhone_number());

                    }
                }


        return result;
    }

    public class GetInvitees extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetInvitees(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();



                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(TempInviteesActivity.this);

                    //Remove the + sign

                    String phone_num = cache.getPhone_number().replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    try {

                        filterJson.put("phone", phone_num);


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            // .header("Authorization", cache.getId())
                            .get()
                            .url(Urls.POST_MEMBERS_INVITEES+"/"+phone_num+"/"+"inviteMembers?access_token="+cache.getId())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try { Log.e(TAG+":"+Urls.POST_MEMBERS_INVITEES,result); } catch (Exception e) { Log.e(TAG,"result crush"); }


                try {
                    Object json = new JSONTokener(result).nextValue();

                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(TempInviteesActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else if(jsonObject.has(Keys.VehicleKeys.KEY_JSON_RESPONSE) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_RESPONSE)){

                            JSONArray phoneNumberArray = jsonObject.getJSONArray(Keys.VehicleKeys.KEY_JSON_RESPONSE);

                                //Default value for input

                            if(phoneNumberArray.length() > 0){
                                try {
                                    DatabasePhonebook db = new DatabasePhonebook(getApplicationContext());
                                    db.open();

                                    for (int i = 0; i < phoneNumberArray.length(); i++) {

                                        Phonebook phonebook = new Phonebook();
                                        String name = db.getName(phoneNumberArray.getString(i));
                                        phonebook.setUsername(name);
                                        phonebook.setPhone_number(Constant.PLUS + phoneNumberArray.getString(i));
                                        contacts.add(phonebook);
                                    }

                                    db.close();

                                } catch (Exception e) {
                                    Log.e(TAG, e.toString());
                                }
                            }else{

                                Toast.makeText(TempInviteesActivity.this,getString(R.string.no_friends_invite),Toast.LENGTH_SHORT).show();

                            }

                                    //Update the ListView
                                    adapter = new TempInviteeAdapter(TempInviteesActivity.this, contacts);
                                    adapter.notifyDataSetChanged();
                                    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                                    listView.setAdapter(adapter);


                        }

                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exp"+e.toString());
                }
            }

    }


    private void onInviteClicked() {
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                .setCallToActionText(getString(R.string.invitation_cta))
                .build();
        startActivityForResult(intent, REQUEST_INVITE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.d(TAG, "onActivityResult: sent invitation " + id);
                }
            } else {
                // Sending failed or it was canceled, show failure message to the user
                // ...
            }
        }
    }

    public void shareLongDynamicLink() {
        Intent intent = new Intent();
        String msg = getString(R.string.invitation_amini_description)+": " + buildDynamicLink();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        intent.setType("text/plain");
        startActivity(intent);
    }


    private String buildDynamicLink() {
        //more info at https://firebase.google.com/docs/dynamic-links/create-manually

        String uid = new Cache(this).getUserId();

         return  getString(R.string.invitation_link)+"?invitedby=" + uid+
                "&link=" +getString(R.string.invitation_amini_link)+
                "&apn=" + getPackageName()+
                "&st=" +getString(R.string.invitation_amini_title)+
                "&utm_source=" +getString(R.string.invitation_amini_source);

    }




    public void shareShortDynamicLink() {
        Task<ShortDynamicLink> createLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(buildDynamicLink()))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink(); //flowchart link is a debugging URL

                            Log.d(TAG, shortLink.toString());
                            Log.d(TAG, flowchartLink.toString());
                            Intent intent = new Intent();
                            String msg = getString(R.string.invitation_message) + shortLink.toString();
                            intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, msg);
                            intent.setType("text/plain");
                            startActivity(intent);

                        } else {
                            // Error
                            Log.e(TAG,"\nError building short link");
                        }
                    }
                });
    }
}
