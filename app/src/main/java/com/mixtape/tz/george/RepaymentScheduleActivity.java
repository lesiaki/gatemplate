package com.mixtape.tz.george;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RepaymentScheduleActivity extends AppCompatActivity implements ExistingLoanOnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    AdapterRepayment adapter;
    private List<Repayments> data = Collections.emptyList();
    RecyclerView recyclerView;

    String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
    String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    String TAG;
    String loanID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repayment_schedule);
        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        data = new ArrayList<>();


        try {

            loanID = getIntent().getStringExtra(Constant.ID);

        }catch (Exception e){loanID = Constant.ZERO;}

        // Read and show the contacts
        //showContacts();

        adapter = new AdapterRepayment(this, data);
        adapter.ExistingLoanOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());



        ConnectionDetector connectionDetector = new ConnectionDetector(RepaymentScheduleActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetRepayments(RepaymentScheduleActivity.this).execute();

        } else {
            Toast.makeText(RepaymentScheduleActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onItemClick(View view, int position, List<Loans> data) {
        switch (view.getId()){
            case R.id.line1:
                Intent intent = new Intent(RepaymentScheduleActivity.this,MainActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onItemLongClick(View view, int position, List<Loans> data) {

    }


    /**
     * Show the contacts in the ListView.
     */
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {

            getSupportLoaderManager().initLoader(1, null, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }




    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {

        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(this, CONTENT_URI, null,null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {

        Guranter guranter;

        StringBuilder sb = new StringBuilder();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            guranter = new Guranter();

            guranter.setFname(cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)));
            guranter.setPhone_number(cursor.getString(cursor.getColumnIndex(NUMBER)));


            //sb.append("\n" + data.getString(data.getColumnIndex(DISPLAY_NAME)));
            //sb.append(":" + data.getString(data.getColumnIndex(NUMBER)));

            //data.add(guranter);
            cursor.moveToNext();
        }
        //textView.setText(sb);

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }


    public class GetRepayments extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetRepayments(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("LOAN_REPAYMENT_URL",Urls.POST_LOAN_REPAYMENTS_SCHEDULE);

                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(RepaymentScheduleActivity.this);

                    try {
                        filterJson.put("loanId", loanID);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }



                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_LOAN_REPAYMENTS_SCHEDULE+"?="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_REQUEST,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();


                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(RepaymentScheduleActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                        JSONArray jsonArray = new JSONArray(result);

                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {



                                //Default value for input
                                String id = Constant.ERROR_100;
                                String status = Constant.ERROR_100;
                                String created = Constant.ERROR_100;
                                String modified = Constant.ERROR_100;
                                String amountDue = Constant.ERROR_100;
                                String amountPaid = Constant.ERROR_100;
                                String penaltyAmount = Constant.ERROR_100;
                                String dateDue = Constant.ERROR_100;
                                String datePaid = Constant.ERROR_100;


                                try {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    Repayments repayments = new Repayments();

                                    //Check for json data if has KEY and KEY_VALUE is not Null

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                        id = object.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_AMOUNT_DUE) && !object.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT_DUE)) {
                                        amountDue = object.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT_DUE);
                                    }

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_AMOUNT_PAID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT_PAID)) {
                                        amountPaid = object.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT_PAID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_PENALTY_AMOUNT) && !object.isNull(Keys.VehicleKeys.KEY_JSON_PENALTY_AMOUNT)) {
                                        penaltyAmount = object.getString(Keys.VehicleKeys.KEY_JSON_PENALTY_AMOUNT);
                                    }

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_DATE_DUE) && !object.isNull(Keys.VehicleKeys.KEY_JSON_DATE_DUE)) {
                                        dateDue = object.getString(Keys.VehicleKeys.KEY_JSON_DATE_DUE);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_DATE_PAID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_DATE_PAID)) {
                                        datePaid = object.getString(Keys.VehicleKeys.KEY_JSON_DATE_PAID);
                                    }

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !object.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                        status = object.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                        created = object.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                        modified = object.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                                    }



                                    repayments.setId(id);
                                    repayments.setAmountDue(amountDue);
                                    repayments.setAmountPaid(amountPaid);
                                    repayments.setDateDue(dateDue);
                                    repayments.setDatePaid(datePaid);
                                    repayments.setStatus(status);
                                    repayments.setCreated(created);
                                    repayments.setModified(modified);
                                    repayments.setPenaltyAmount(penaltyAmount);


                                    data.add(repayments);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            adapter = new AdapterRepayment(RepaymentScheduleActivity.this,data);
                            adapter.notifyDataSetChanged();

                            adapter.ExistingLoanOnItemClickListener(RepaymentScheduleActivity.this);

                            recyclerView.setAdapter(adapter);

                            recyclerView.setLayoutManager(new LinearLayoutManager(RepaymentScheduleActivity.this));

                            recyclerView.setItemAnimator(new DefaultItemAnimator());



                        } else {
                            Toast.makeText(RepaymentScheduleActivity.this,getString(R.string.empty),Toast.LENGTH_SHORT).show();
                        }



                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }

}
