package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.raycoarana.codeinputview.CodeInputView;
import com.raycoarana.codeinputview.OnCodeCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//https://github.com/raycoarana/material-code-input

public class OTPActivity extends AppCompatActivity implements View.OnClickListener {



    TextView textViewLogin;
    Button buttonSignUP;
    EditText editTextFirstName;
    EditText editTextLastName;
    EditText editTextPassword;
    CodeInputView codeInputView;
    TextView textViewPhoneNumber;
    TextView textViewResendOTP;



    String password;
    String code;
    String TAG;
    String phone_number;
    String OTP = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        textViewLogin = (TextView)findViewById(R.id.text_view_login);
        buttonSignUP = (Button)findViewById(R.id.button_signup);
        editTextFirstName = (EditText)findViewById(R.id.firstname);
        editTextLastName = (EditText)findViewById(R.id.lastname);
        codeInputView = (CodeInputView) findViewById(R.id.code_input);
        editTextPassword = (EditText)findViewById(R.id.password);
        textViewPhoneNumber = (TextView)findViewById(R.id.tv_phone_number);
        textViewResendOTP = (TextView)findViewById(R.id.resend_otp);


        textViewLogin.setOnClickListener(this);
        buttonSignUP.setOnClickListener(this);
        textViewResendOTP.setOnClickListener(this);


        TAG = getIntent().getClass().getSimpleName();

        try {
            phone_number = getIntent().getStringExtra(Constant.PHONE_NUMBER);
            textViewPhoneNumber.setText(phone_number);
        } catch (Exception e) {
            e.printStackTrace();
        }




    codeInputView.addOnCompleteListener(new OnCodeCompleteListener() {
        @Override
        public void onCompleted(String code) {

            if(validateInputs()){

                ConnectionDetector connectionDetector = new ConnectionDetector(OTPActivity.this);
                if (connectionDetector.isInternetConnected()) {

                    new PostMembers(OTPActivity.this).execute();

                } else {
                    Toast.makeText(OTPActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                }

            }else{

            }

        }
    });



    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.text_view_login:

                startActivity(new Intent(OTPActivity.this,LoginActivity.class));

                break;

            case R.id.button_signup:


                if(validateInputs()){

                    ConnectionDetector connectionDetector = new ConnectionDetector(OTPActivity.this);
                    if (connectionDetector.isInternetConnected()) {

                        new PostMembers(OTPActivity.this).execute();

                    } else {
                        Toast.makeText(OTPActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                    }

                }else{

                }

                break;

            case R.id.resend_otp:

                ConnectionDetector connectionDetector = new ConnectionDetector(OTPActivity.this);
                if (connectionDetector.isInternetConnected()) {

                    new SendOTP(OTPActivity.this).execute();

                } else {
                    Toast.makeText(OTPActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                }

                break;

        }

    }


    private boolean validateInputs(){

        code = codeInputView.getCode();

        if(codeInputView.getCode().length() < 4){

            Toast.makeText(this,getString(R.string.require_code),Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;


    }


    public class PostMembers extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostMembers(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.verifie));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {


            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    //Remove the + sign

                    String phone_num = phone_number.replaceAll("[\\D]", "").trim();
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    JSONObject jsonObject = new JSONObject();
                    try {


                        jsonObject.put("username", phone_num);
                        jsonObject.put("phone", phone_num);
                        jsonObject.put("otp", code);


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_MEMBERS_VERIFY_PHONE)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            codeInputView.setCode("");
            codeInputView.setEditable(true);

            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS,result); } catch (Exception e) {
                e.printStackTrace();
            }

            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);


                            Log.e(TAG,message);
                            Toast.makeText(OTPActivity.this,getString(R.string.invalid_code),Toast.LENGTH_SHORT).show();


                        }else{

                            String phoneVerified = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_PHONE_VERIFIED);

                            if(Boolean.parseBoolean(phoneVerified)){

                                //NOTE the id and redirect to sign-up

                                Intent intentPassword = new Intent(OTPActivity.this,LoginActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constant.PHONE_NUMBER,phone_number);
                                intentPassword.putExtras(bundle);
                                startActivity(intentPassword);


                            }else{

                            }
                        }


                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }



    public class SendOTP extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public SendOTP(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";

                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    JSONObject jsonObject = new JSONObject();

                    //Remove the + sign

                    String phone_num = phone_number.replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_SEND_OTP+"?phone="+phone_num)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(OTPActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{
                            String sent = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_SENT);

                            if(Boolean.parseBoolean(sent)){

                                Toast.makeText(OTPActivity.this,getString(R.string.otp_sent_noti),Toast.LENGTH_LONG).show();

                            }
                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }






}
