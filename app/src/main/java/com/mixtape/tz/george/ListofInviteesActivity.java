package com.mixtape.tz.george;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListofInviteesActivity extends AppCompatActivity {

    List<Phonebook> contacts = Collections.emptyList();
    TempInviteeAdapter adapter;
    ListView recyclerView;
    String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitees_list);
        recyclerView = (ListView)findViewById(R.id.recyler_view);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TAG = getIntent().getClass().getSimpleName();

        contacts = new ArrayList<>();



        try {
            DatabasePhonebook db = new DatabasePhonebook(ListofInviteesActivity.this);
            db.open();
            Log.e(TAG+":-Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
            Log.e(TAG+":-Length",Integer.toString(db.getInvitees().size()));

            if(db.getInvitees().size() > 0){
                contacts = db.getInvitees();
            }else{
                Toast.makeText(this,getString(R.string.no_contacts),Toast.LENGTH_SHORT).show();
            }

            db.close();


            adapter = new TempInviteeAdapter(this, contacts);
            //adapter.CustomGuranterOnItemClickListener(this);
            recyclerView.setAdapter(adapter);
           /* recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());*/


        }catch (Exception e) {
            Log.e(TAG,e.toString());
        }


    }




}
