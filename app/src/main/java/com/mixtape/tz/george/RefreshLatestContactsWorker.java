package com.mixtape.tz.george;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import androidx.work.Data;
import androidx.work.Result;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;



public class RefreshLatestContactsWorker extends Worker {


    String TAG = "RefreshLatestContactsWorker";
    JSONArray phoneBook = null;

    public RefreshLatestContactsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }


    @NonNull
    @Override
    public Result doWork() {
        //read input argument
        String workType = getInputData().getString("workType");
        Log.i("refresh cpn work", "type of work request: "+workType);

        Context context = getApplicationContext();
        String result =  Constant.NIL;


            try {

                String root_username = "RootAdmin";
                String root_password = "RootAdmin";
                String credentials = root_username + ":" + root_password;

                final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                OkHttpClient okHttpClient = new OkHttpClient();

                JSONObject jsonObject = new JSONObject();

                Cache cache = new Cache(context);
                String full_name = cache.getFname()+" "+cache.getLname();

                //Remove the + sign

                String phone_num = cache.getPhone_number().replaceAll("[\\D]", "");
                Log.e(TAG+":- Removed Phn + signed",phone_num);


                try {
                    DatabasePhonebook db = new DatabasePhonebook(context);
                    db.open();
                    phoneBook = listofphn(db.getPhoneBookByStatus());
                    db.close();

                }catch (Exception e) {
                    Log.e("TAG",e.toString());
                }


                try {
                    jsonObject.put("name", full_name);
                    jsonObject.put("phone",phone_num);
                    jsonObject.put("phoneBook", phoneBook);
                    jsonObject.put("friendsPhones","");
                    jsonObject.put("inviteesPhone","");

                } catch (JSONException e) {
                    Log.e(context.getString(R.string.invalid_json),e.toString());
                }


                MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                RequestBody postData = RequestBody.create(JSON, jsonObject.toString());


                Log.e("Paramemter", jsonObject.toString());

                Request request = new Request.Builder()
                        /*.header("Authorization", basic)*/
                        .header("Authorization", cache.getId())
                        .post(postData)
                        .url(Urls.POST_MEMBERS_ADDPHNBOOK)
                        .build();

                Response response = okHttpClient.newCall(request).execute();
                result = response.body().string();


            } catch (Exception e) {Log.e(TAG,"Fail:"+e.toString());Result.failure();}

        //sending work status to caller
        //return Worker.Result.SUCCESS;

        //sendNotification("Simple Work Manager", result);


        updateMemberToLocalDB(result);

        Data output = new Data.Builder().putString(Constant.RESPONSE, result).build();
        return Result.success(output);

    }


    private void updateSelectedDB(JSONArray JAPhoneBook){

        Log.e("Starts Update Status:",JAPhoneBook.toString());

        try {
            DatabasePhonebook db = new DatabasePhonebook(getApplicationContext());
            db.open();

            for(int i = 0; i < JAPhoneBook.length(); i++){

                    if(db.updateStatus(Constant.PLUS+JAPhoneBook.getString(i))  > 0){

                        Log.e("Ustatus_Updated:",JAPhoneBook.getString(i));
                    }
            }
            Log.e(TAG+":-Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
            db.close();


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public JSONArray listofphn(List<Phonebook> contacts){

        JSONArray jsonArray = new JSONArray();

        String[] phn = new String[contacts.size()];

        for(int i = 0; i < contacts.size(); i++) {

            String single  = contacts.get(i).phone_number.replace("+","").trim();
            //phn[i] =  single;

            jsonArray.put(single);
        }

        return jsonArray;
    }


    public void sendNotification(String title, String message) {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        //If on Oreo then notification required a notification channel.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManager.notify(1, notification.build());
    }


    private  void updateMemberToLocalDB(String result){
        try {
            Log.e(TAG+":Graph Result",result);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(result != null){

            try {

                Object json = new JSONTokener(result).nextValue();
                if (json instanceof JSONObject){

                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_RESPONSE) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_RESPONSE)) {


                        if(jsonObject.getString(Keys.VehicleKeys.KEY_JSON_RESPONSE).equals("0")){

                            Log.e(TAG,getApplicationContext().getString(R.string.al_member));


                        }else{

                            Log.e(TAG,getApplicationContext().getString(R.string.done));

                            JSONArray jsonArray = jsonObject.getJSONArray(Keys.VehicleKeys.KEY_JSON_RESPONSE);


                            List<Phonebook> tempContacts = new ArrayList<>();

                            for(int i = 0; i < jsonArray.length(); i++){

                                JSONObject innerJsonObject = jsonArray.getJSONObject(i);
                                Phonebook  phonebook = new Phonebook();


                                String id = Constant.ERROR_100;
                                String phone = Constant.ERROR_100;
                                String userId = Constant.ERROR_100;
                                String ttl = Constant.ERROR_100;
                                String created = Constant.ERROR_100;
                                String otpVerified = Constant.ERROR_100;
                                String fname = Constant.ERROR_100;
                                String lname = Constant.ERROR_100;


                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_ID) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                    id = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                }
                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_USER_ID) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_USER_ID)) {
                                    userId = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_USER_ID);
                                }
                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_TTL) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_TTL)) {
                                    ttl = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_TTL);
                                }
                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                    created = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                }
                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED)) {
                                    otpVerified = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED);
                                }
                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_FNAME) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_FNAME)) {
                                    fname = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_FNAME);
                                }

                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_LNAME) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_LNAME)) {
                                    lname = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_LNAME);
                                }
                                if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_PHONE) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_PHONE)) {
                                    phone = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_PHONE);
                                }

                                phonebook.setUser_id(id);
                                phonebook.setPhone_number(phone);
                                phonebook.setUsername(fname);

                                tempContacts.add(phonebook);

                            }


                            //Update Member Local db
                            try {
                                DatabasePhonebook db = new DatabasePhonebook(getApplicationContext());
                                db.open();

                                for (int i = 0; i < tempContacts.size(); i++){

                                    if(db.updateUserID(Constant.PLUS+tempContacts.get(i).getPhone_number(),tempContacts.get(i).getUser_id()) > 0){

                                        Log.e("UserID_Updated:",tempContacts.get(i).getPhone_number());
                                    }
                                }

                                Log.e(TAG+":-MemberUpdate:Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
                                db.close();

                            }catch (Exception e) {
                                Log.e(TAG,e.toString());
                            }

                            //Update User selected local db

                            updateSelectedDB(phoneBook);

                        }

                    }else{



                    }

                } else if (json instanceof JSONArray){

                    Log.e(TAG, "Data is J-AR ");
                    Log.e(TAG,getApplicationContext().getString(R.string.unable_to_in));

                }else{

                    //Neither JsonObject Nor JsonArray

                    Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                    Log.e(TAG,getApplicationContext().getString(R.string.unable_to_in));


                }

            } catch (Exception e) {

                Log.e(TAG, "Json Fail "+e.toString());
                Log.e(TAG,getApplicationContext().getString(R.string.unable_to_in));
            }
        }



    }


}




