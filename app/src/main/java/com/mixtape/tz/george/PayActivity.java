package com.mixtape.tz.george;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PayActivity extends AppCompatActivity implements ExistingLoanOnItemClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {


    List<Loans> contacts = Collections.emptyList();
    List<Loans> tempData = Collections.emptyList();
    AdapterPay adapter;
    RecyclerView recyclerView;
    TextView txtVTotalBalToBePaidByG;
    TextView textViewReportingDate;
    TextView textViewInterestRate;
    String TAG;

    LinearLayout lyFooter;
    Resources res;
    String country_code;
    int mSelectedItem;
    TextView textViewInfo;
    TextView textViewStatus;
    TextView textViewInterestAmount;
    TextView textViewNoOfInstallments;
    TextView textViewExpireDate;
    TextView textViewGuaranteeBalance;
    TextView textViewPersonalGuarantee;
    TextView textViewLoanAmount;
    TextView textViewExpectedCollection;
    TextView textViewAccountNumber;
    TextView textViewUsername;
    TextView textViewPhoneNumber;
    RelativeLayout relativeLayout;
    LinearLayout lFooter;



    String gLoanID;
    String guarantorId;
    String status;
    String created;
    String modified;
    String gAmount;
    String memberId;
    ImageView imageViewInfo;
    Loans loans;


    int x = 0;
    int y = 0;
    int z = 0;
    int q = 0;



    /*
     * Varibale for firebase
     * */

    String to;
    String message;
    String gName;
    String gFirebaseToken;

    Cache cache;

    BroadcastReceiver mBroadcastReceiver;
    public static boolean isAppRunning;


    String LOAN_AMOUNT;
    String GUARANTER_ID;
    String LOAN_ID;
    String FULLNAME;
    String PHONE_NUMBER;
    String ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isAppRunning = true;

        setContentView(R.layout.layout_pay_activity);
        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);
        textViewInterestRate = (TextView)findViewById(R.id.txtv_interest_rate);
        textViewReportingDate = (TextView)findViewById(R.id.txtv_report_date);
        imageViewInfo = (ImageView)findViewById(R.id.image_view_info);
        textViewInfo = (TextView)findViewById(R.id.txt_pw);
        textViewInterestAmount = (TextView)findViewById(R.id.txtv_interest_amt);
        textViewNoOfInstallments = (TextView)findViewById(R.id.txtv_no_installments);
        textViewExpireDate = (TextView)findViewById(R.id.txtv_expire_date);
        textViewStatus = (TextView)findViewById(R.id.txtv_status);
        textViewGuaranteeBalance = (TextView)findViewById(R.id.txtv_gneeded);
        textViewLoanAmount = (TextView)findViewById(R.id.txtv_loan_amount);
        txtVTotalBalToBePaidByG = (TextView)findViewById(R.id.txtview_friendsg);
        textViewPersonalGuarantee= (TextView)findViewById(R.id.txtv_personalg);
        textViewAccountNumber = (TextView)findViewById(R.id.txtv_ac_no);
        textViewExpectedCollection = (TextView)findViewById(R.id.txtv_expected_collection);
        textViewUsername = (TextView)findViewById(R.id.txtv_username);
        textViewPhoneNumber = (TextView)findViewById(R.id.txtv_phone_number);
        relativeLayout = (RelativeLayout)findViewById(R.id.relative_layout);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        isAppRunning = true;
        cache = new Cache(PayActivity.this);


        //getIntent

        try {

             LOAN_AMOUNT = getIntent().getStringExtra(Constant.LOAN_AMOUNT);
             GUARANTER_ID = getIntent().getStringExtra(Constant.GUARANTER_ID);
             LOAN_ID = getIntent().getStringExtra(Constant.LOAN_ID);
             FULLNAME = getIntent().getStringExtra(Constant.FULLNAME);
             PHONE_NUMBER = getIntent().getStringExtra(Constant.PHONE_NUMBER);
             ID = getIntent().getStringExtra(Constant.ID);

             Log.e("Intent","Amt="+LOAN_AMOUNT+": gID="+GUARANTER_ID+":LID="+LOAN_ID+":ID="+ID+":Name="+FULLNAME+":phone="+PHONE_NUMBER);

        } catch (Exception e) {
            e.printStackTrace();
        }

        // on some click or some loading we need to wait for...


        TAG = getIntent().getClass().getSimpleName();

        res = getResources();
        country_code = getString(R.string.tz);

        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),putCommer(LOAN_AMOUNT));
        textViewLoanAmount.setText(accountBalanceTxtHolder);

        String username = String.format(res.getString(R.string.fullname),FULLNAME);
        textViewUsername.setText(username);

        String phone_number = String.format(res.getString(R.string.user_phone),PHONE_NUMBER);
        textViewPhoneNumber.setText(phone_number);

        String gm = String.format(res.getString(R.string.tgm),putCommer(Integer.toString(y)));
        txtVTotalBalToBePaidByG.setText(gm);

        String personal_guarantee = String.format(res.getString(R.string.personal_guarantoee),putCommer(Constant.ZERO_AS_VALUE));
        textViewPersonalGuarantee.setText(personal_guarantee);

        String message_balance = String.format(res.getString(R.string.guarantee_needed),putCommer(Constant.ZERO_AS_VALUE));
        textViewGuaranteeBalance.setText(message_balance);

        String interest = String.format(res.getString(R.string.interest),Constant.ZERO_AS_VALUE);
        textViewInterestRate.setText(interest);

        String account_number = String.format(res.getString(R.string.ac_no),Constant.ZERO_AS_VALUE);
        textViewAccountNumber.setText(account_number);

        String interest_amount = String.format(res.getString(R.string.interest_amount),Constant.ZERO_AS_VALUE);
        textViewInterestAmount.setText(interest_amount);

        String no_of_installments = String.format(res.getString(R.string.no_of_installments),Constant.ZERO_AS_VALUE);
        textViewNoOfInstallments.setText(no_of_installments);

        String expire_date = String.format(res.getString(R.string.expire_date),Constant.ZERO_AS_VALUE);
        textViewExpireDate.setText(expire_date);

        String expected_colletion = String.format(res.getString(R.string.expected_colletion),Constant.ZERO_AS_VALUE);
        textViewExpectedCollection.setText(expected_colletion);


        final String loan_status = String.format(res.getString(R.string.loan_status),getLoanStatus(Constant.ZERO_AS_VALUE));
        textViewStatus.setText(loan_status);


        contacts = new ArrayList<>();

        // Read and show the contacts
        //showContacts();

        adapter = new AdapterPay(this, contacts);
        adapter.setOnLoanItemClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        ConnectionDetector connectionDetector = new ConnectionDetector(PayActivity.this);
        if (connectionDetector.isInternetConnected()) {

            //new GetAccount(PayActivity.this).execute();

        } else {
            Toast.makeText(PayActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }



        imageViewInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PayActivity.this,RepaymentScheduleActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        // adding item touch helper
        // only ItemTouchHelper.LEFT added to detect Right to Left swipe
        // if you want both Right -> Left and Left -> Right
        // add pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT as param
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);





    }


    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof AdapterLoans.GuranterViewHolder) {
            // get the removed item name to display it in snack bar
            String amount = contacts.get(viewHolder.getAdapterPosition()).getAmount();
            mSelectedItem = position;
            gLoanID = contacts.get(viewHolder.getAdapterPosition()).getLoanId();
            guarantorId = contacts.get(viewHolder.getAdapterPosition()).getGuarantorId();
            status = contacts.get(viewHolder.getAdapterPosition()).getStatus();

            // backup of removed item for undo purpose
            final Loans deletedItem = contacts.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            // remove the item from recycler view
            adapter.removeItem(viewHolder.getAdapterPosition());


            if(Integer.parseInt(status) == 1){

                adapter.restoreItem(deletedItem, deletedIndex);

                String request_dec = String.format(getString(R.string.delete_warning),getString(R.string.tz),putCommer(amount));

                Snackbar snackbar = Snackbar.make(relativeLayout,request_dec, Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction(getString(R.string.okay), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // undo is selected, restore the deleted item
                        //adapter.restoreItem(deletedItem, deletedIndex);

                    }
                });
                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
                }else{



                ConnectionDetector connectionDetector = new ConnectionDetector(PayActivity.this);
                if (connectionDetector.isInternetConnected()) {

                    new PostDelete(PayActivity.this).execute();

                } else {
                    Toast.makeText(PayActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                }
            }





        }
    }



    private int removeCommer(String amount){
        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }


    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }



        return amt;
    }

    @Override
    public void onItemClick(View view, int position, List<Loans> data) {

        mSelectedItem = position;
        tempData = data;
        gLoanID = data.get(position).getLoanId();
        gAmount = Integer.toString(removeCommer(data.get(position).getAmount()));
        guarantorId = data.get(position).getGuarantorId();
        status = data.get(position).getStatus();
        created = data.get(position).getCreated();
        memberId = data.get(position).getMemberId();


        switch (view.getId()){

            case R.id.button:

                    ConnectionDetector connectionDetector = new ConnectionDetector(PayActivity.this);
                    if (connectionDetector.isInternetConnected()) {

                        new PostAccept(PayActivity.this).execute();

                    } else {
                        Toast.makeText(PayActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                    }


                break;


            case R.id.line1:

                Intent intent = new Intent(this,FriendsProfileActivity.class);
                Bundle bundle = new Bundle();

                bundle.putString(Constant.AMOUNT,gAmount);
                bundle.putString(Constant.LOAN_ID,gLoanID);
                bundle.putString(Constant.GUARANTER_ID,guarantorId);
                bundle.putString(Constant.STATUS,status);
                bundle.putString(Constant.CREATED,created);
                bundle.putString(Constant.MEMBER_ID,memberId);

                intent.putExtras(bundle);

                startActivity(intent);


                break;


        }

    }

    @Override
    public void onItemLongClick(View view, int position, List<Loans> data) {

    }


    public class GetAccount extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetAccount(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = Constant.ROOT_USER;
                    String root_password = Constant.ROOT_PASSWORD;
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();



                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(PayActivity.this);
                    //{"where":{"guarantorId":"80","status": {"between": [0,2]}}, "order":"created DESC"}

                    try {
                        filterJson.put("where", new JSONObject().put("memberId",cache.getUserId()));
                        filterJson.put("order", "id DESC");
                        filterJson.put("limit",1);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.GET_ACCOUNT_BALANCE+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS_ACCOUNT,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    JSONArray jsonArray = new JSONArray(result);

                    if (json instanceof JSONArray){

                        JSONObject innerJson = jsonArray.getJSONObject(0);

                        String accountNumber = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_NUMBER);
                        String accountBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_BALANCE);
                        String guaranteeBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_BALANCE);
                        String created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                        String modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                        String id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);

                        String accountNumberTxtHolder = String.format(res.getString(R.string.ac_no),accountNumber);
                        textViewAccountNumber.setText(accountNumberTxtHolder);

                        x = Integer.parseInt(accountBalance);
                        q= Integer.parseInt(guaranteeBalance);

                        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),putCommer(Integer.toString(x)));
                        textViewLoanAmount.setText(accountBalanceTxtHolder);

                        String gm = String.format(res.getString(R.string.tgm),putCommer(Integer.toString(q)));
                        txtVTotalBalToBePaidByG.setText(gm);

                        textViewInfo.setVisibility(View.VISIBLE);

                        ConnectionDetector connectionDetector = new ConnectionDetector(PayActivity.this);
                        if (connectionDetector.isInternetConnected()) {

                            new GetGuarantees(PayActivity.this).execute();

                        } else {
                            Toast.makeText(PayActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                        }


                    } else if (json instanceof JSONObject){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(PayActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }else{

                        //Neither JsonObject Nor JsonArray
                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(PayActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {
                    //Return is []
                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(PayActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();

                }
            }


        }
    }


    public  class GetGuarantees extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;


        public GetGuarantees(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject filterJson = new JSONObject();



                    //Remove the + sign

                    String phone_num = cache.getPhone_number().replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    //{"where":{"guarantorId":"80","status":{"between": [0,2]}}, "order":"id DESC","include":"loan"}

                    try {

                        JSONArray boundaries =  new JSONArray().put("0").put("1");

                        filterJson.put("where", new JSONObject()
                                        .put("guarantorId",cache.getUserId())
                                        .put("status",new JSONObject().put("between",boundaries)));

                        filterJson.put("order", "id DESC");
                        filterJson.put("include", "loan");


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                           // .header("Authorization", cache.getId())
                            .get()
                            .url(Urls.GET_LOAN_GUARANTEES+"?filter="+filterJson)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try { Log.e(TAG+":"+Urls.GET_LOAN_GUARANTEES,result); } catch (Exception e) { Log.e(TAG,"result crush"); }
            textViewInfo.setVisibility(View.GONE);

                try {
                    Object json = new JSONTokener(result).nextValue();

                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);



                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(PayActivity.this,message,Toast.LENGTH_SHORT).show();


                        }

                    }else {


                        JSONArray jsonArray = new JSONArray(result);

                        if (jsonArray.length() > 0) {

                            //Default value for input
                            String id = Constant.ERROR_100;

                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;
                            String loan_id = Constant.ERROR_100;
                            String guarantor_id = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String memberId = Constant.ERROR_100;

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    Loans loans = new Loans();

                                    //Check for json data if has KEY and KEY_VALUE is not Null

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                        id = object.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_LOAN_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_LOAN_ID)) {
                                        loan_id = object.getString(Keys.VehicleKeys.KEY_JSON_LOAN_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !object.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                        amount = object.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                                        guarantor_id = object.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !object.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                        status = object.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                        created = object.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                        modified = object.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                                    }

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_LOAN) && !object.isNull(Keys.VehicleKeys.KEY_JSON_LOAN)) {

                                                JSONObject innerObject = object.getJSONObject(Keys.VehicleKeys.KEY_JSON_LOAN);

                                                if (innerObject.has(Keys.VehicleKeys.KEY_JSON_MEMBER_ID) && !innerObject.isNull(Keys.VehicleKeys.KEY_JSON_MEMBER_ID)) {

                                                    memberId = innerObject.getString(Keys.VehicleKeys.KEY_JSON_MEMBER_ID);
                                                    Log.e(TAG,"MemberID:"+memberId);

                                                }

                                        }


                                    loans.setGuarantorId(guarantor_id);
                                    loans.setModified(modified);
                                    loans.setCreated(created);
                                    loans.setStatus(status);
                                    loans.setAmount(amount);
                                    loans.setId(id);
                                    loans.setLoanId(loan_id);
                                    loans.setMemberId(memberId);
                                    loans.setGuaranteeAccepted(Integer.parseInt(status));

                                    //q =q+Integer.parseInt(amount);

                                    contacts.add(loans);


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }


                            //Update Main View

                            adapter.notifyDataSetChanged();
                            adapter = new AdapterPay(PayActivity.this,contacts);
                            adapter.setOnLoanItemClickListener(PayActivity.this);

                            recyclerView.setAdapter(adapter);

                            recyclerView.setLayoutManager(new LinearLayoutManager(PayActivity.this));
                            recyclerView.setItemAnimator(new DefaultItemAnimator());



                        } else {

                            textViewInfo.setVisibility(View.VISIBLE);
                            textViewInfo.setText(getString(R.string.no_any_request));
                        }


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    textViewInfo.setVisibility(View.VISIBLE);
                    textViewInfo.setText(getString(R.string.unable_to_update));
                }


        }
    }

    private String getLoanStatus(String status){
        String result;
        if(status.equals(Constant.ONE_AS_VALUE)){
            result = getString(R.string.approved);
        }else{
            result = getString(R.string.pending);
        }
        return result;
    }


    private String formatDate(String dt){

        DateTimeFormatter inputFormatter = null;
        DateTimeFormatter outputFormatter = null;
        LocalDate date = null;
        String formattedDate = dt;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            date = LocalDate.parse(dt, inputFormatter);
            formattedDate = outputFormatter.format(date);

        }else{
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date2 = null;
            try {
                date2 = inputFormat.parse(dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            formattedDate = outputFormat.format(date2);

        }

        return formattedDate;
    }

    String  CapsFL(String word){
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }



    public class PostAccept extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostAccept(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.sending_req));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    //{"loanId":23, "guarantorId":6, "amount":50000 }

                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("loanId", gLoanID); //Not Yet
                        jsonObject.put("guarantorId", guarantorId); //Not Yet
                        jsonObject.put("amount",gAmount ); //Done


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("Paramemter", jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_LOAN_GUARANTEES_ACCEPT)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_GUARANTEES_ACCEPT,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(PayActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                            String id = Constant.ERROR_100;

                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;
                            String loan_id = Constant.ERROR_100;
                            String guarantor_id = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String memberId = Constant.ERROR_100;

                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_RESULT);

                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_LOAN_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_LOAN_ID)) {
                                loan_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_LOAN_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                amount = innerJson.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                                guarantor_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                status = innerJson.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                            }


                            if(Integer.parseInt(status) > 0){

                                tempData.get(mSelectedItem).setGuaranteeAccepted(Integer.parseInt(status));
                                adapter.notifyDataSetChanged();

                                //update view
                                y = Integer.parseInt(amount);
                                x = getZ();
                                int tempQ = q+ Integer.parseInt(amount);

                                String gm = String.format(res.getString(R.string.tgm),putCommer(Integer.toString(tempQ)));
                                txtVTotalBalToBePaidByG.setText(gm);

                                String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),putCommer(Integer.toString(x)));
                                textViewLoanAmount.setText(accountBalanceTxtHolder);

                                String expected_colletion = String.format(res.getString(R.string.expected_colletion),Constant.ZERO_AS_VALUE);
                                textViewExpectedCollection.setText(expected_colletion);


                                /*Send firebase notification to guaranter*/

                                ConnectionDetector connectionDetector = new ConnectionDetector(PayActivity.this);
                                if (connectionDetector.isInternetConnected()) {

                                    new PostNotification(PayActivity.this).execute();

                                } else {
                                    Toast.makeText(PayActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                }

                            }



                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(PayActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(PayActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(PayActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();
                }
            }


        }
    }


    public class PostDelete extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostDelete(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.sending_req));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    //{"loanId":23, "guarantorId":6, "amount":50000 }

                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("loanId", gLoanID); //Not Yet
                        jsonObject.put("guarantorId", guarantorId); //Not Yet


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("Paramemter", jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_LOAN_GUARANTEES_DECLINE)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_GUARANTEES_DECLINE,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(PayActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                            String id = Constant.ERROR_100;

                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;
                            String loan_id = Constant.ERROR_100;
                            String guarantor_id = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String memberId = Constant.ERROR_100;

                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_RESULT);

                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_LOAN_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_LOAN_ID)) {
                                loan_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_LOAN_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                amount = innerJson.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                                guarantor_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                status = innerJson.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                            }


                            if(Integer.parseInt(status) > 0){

                                /*tempData.get(mSelectedItem).setGuaranteeAccepted(Integer.parseInt(status));
                                adapter.notifyDataSetChanged();

                                //update view
                                y = Integer.parseInt(amount);
                                x = getZ();
                                int tempQ = q+ Integer.parseInt(amount);

                                String gm = String.format(res.getString(R.string.tgm),putCommer(Integer.toString(tempQ)));
                                txtVTotalBalToBePaidByG.setText(gm);

                                String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),putCommer(Integer.toString(x)));
                                textViewLoanAmount.setText(accountBalanceTxtHolder);

                                String expected_colletion = String.format(res.getString(R.string.expected_colletion),Constant.ZERO_AS_VALUE);
                                textViewExpectedCollection.setText(expected_colletion);*/

                            }



                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(PayActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(PayActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(PayActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();
                }
            }


        }
    }


    public class PostNotification extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostNotification(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String server_key = "key=AAAA1Ck1sJg:APA91bFxFZ1G8fRo-aw012WXmd3w1GKFHIoTJbrCqBllYpCzib1_ieWEV0fbSolSq9g5bRr8NiktPiAZgIJ7xmYBXyg7saO0XQGSpPFpbRO1biUBb9yTXbJEICrYY59Yfxqkne_ni0wt";

                    String img_url = Constant.IMG_URL_SAMPLE;

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject jsonObject = new JSONObject();

                    String fullname = CapsFL(cache.getFname())+" "+CapsFL(cache.getLname());

                    message = fullname+": Nimepitisha kiasi cha  "+getString(R.string.tz)+ putCommer(gAmount);

                    try {
                        jsonObject.put("to", gFirebaseToken);
                        jsonObject.put("data",new JSONObject()
                                .put("title","Mkopo Umepitishwa")
                                .put("message",message)
                                .put("image-url",img_url)
                                .put(Constant.LOAN_AMOUNT,gAmount)
                                .put(Constant.LOAN_ID,gLoanID)
                                .put(Constant.MEMBER_ID,cache.getUserId())
                                .put(Constant.ACTIVITY_TYPE,YourLoansActivity.class.getSimpleName())


                        );

                    }catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());


                    Log.e("Parameter",jsonObject.toString());

                    Request request = new Request.Builder()
                            .header("Authorization", server_key).addHeader("Content-Type","application/json")
                            .post(postData)
                            .url(Urls.POST_FCM_FIREBASE)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e("Result"+":"+Urls.POST_MEMBERS,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();


        }
    }


    int getX(){
        return x;
    }

    int getY(int y1){

        return y1;
    }

    int getZ(){
        return x-y;
    }


    @Override
    protected void onResume() {
        super.onResume();

        //register new push message receiver // by doing this, the activity will be notified each time a new message arrives

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)){
                    Snackbar snackbar = Snackbar
                            .make(relativeLayout, intent.getStringExtra(Constant.MESSAGE), Snackbar.LENGTH_INDEFINITE)
                            .setAction("View", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    String ACTIVITY = intent.getStringExtra(Constant.ACTIVITY_TYPE);

                                    Class<?> myClass = null;
                                    Activity myActivity = null;

                                    try {
                                        myClass = Class.forName(getPackageName()+"."+ACTIVITY);
                                        myActivity = (Activity) myClass.newInstance();
                                        Log.e("myActivity",myClass.toString());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    startActivity(new Intent(PayActivity.this, myClass));

                                }
                            });

                    snackbar.show();
                }

            }
        }, new IntentFilter(Constant.PUSH_NOTIFICATION));



    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        Log.e(TAG,"Unregister Broadcast");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }


}
