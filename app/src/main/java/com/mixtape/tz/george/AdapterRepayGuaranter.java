package com.mixtape.tz.george;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by georgejohn on 21/08/2016.
 * https://www.androidbegin.com/tutorial/android-delete-multiple-selected-items-listview-tutorial/
 */

public class AdapterRepayGuaranter extends RecyclerView.Adapter<AdapterRepayGuaranter.GuranterViewHolder> {

    private LayoutInflater inflater;
    Context context;

    List<Phonebook> data = Collections.emptyList();

    ClickListernerToGuarantee mItemClickListener;
    GuranterViewHolder hodler;

    public AdapterRepayGuaranter(Context context, List<Phonebook> data) {

        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;

    }

    @Override
    public GuranterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.custome_row_repay_guaranter, parent, false);

        hodler = new GuranterViewHolder(view, new MyCustomEditTextListener());

        return hodler;
    }

    @Override
    public void onBindViewHolder(final GuranterViewHolder holder, final int position) {

                String name = data.get(position).getUsername();

                try {
                    holder.txtViewUsername.setText(name);
                    holder.textViewPhoneNumber.setText(data.get(position).getPhone_number());

                    holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
                    holder.editTextAmount.setText(data.get(holder.getAdapterPosition()).getAmount());

                    if(data.get(position).getGselected() > 0){

                        Drawable btnborder = context.getResources().getDrawable(R.drawable.round_border_blue);
                        holder.textViewButton.setBackground(btnborder);
                        holder.textViewButton.setTextColor(context.getResources().getColor(R.color.bluel1));
                        holder.editTextAmount.setTextColor(context.getResources().getColor(R.color.mid_gray));
                        holder.textViewButton.setClickable(false);
                        holder.editTextAmount.setEnabled(false);


                    }else{
                        Drawable btnborder = context.getResources().getDrawable(R.drawable.round_solid_blue);
                        holder.textViewButton.setBackground(btnborder);
                        holder.textViewButton.setTextColor(context.getResources().getColor(R.color.white));
                        holder.editTextAmount.setTextColor(context.getResources().getColor(R.color.bluel1));
                        holder.textViewButton.setClickable(true);
                        holder.editTextAmount.setEnabled(true);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

    }



    @Override
    public int getItemCount() {
            return data.size();
    }

    public void setCustomOnItemClickListener(ClickListernerToGuarantee mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }



    class GuranterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtViewUsername;
        TextView textViewPhoneNumber;
        EditText editTextAmount;
        ImageView imageCall;
        ImageView profileImage;
        LinearLayout linearLayout;
        TextView textViewButton;

        public MyCustomEditTextListener myCustomEditTextListener;

        public GuranterViewHolder(View itemView, MyCustomEditTextListener myCustomEditTextListener) {
            super(itemView);

            txtViewUsername = (TextView) itemView.findViewById(R.id.username_txtview);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_icon_dalali);
            textViewPhoneNumber = (TextView)itemView.findViewById(R.id.phone_number_txtview);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.line1);
            textViewButton = (TextView)itemView.findViewById(R.id.button_request);

            editTextAmount = (EditText) itemView.findViewById(R.id.input_amount);
            this.myCustomEditTextListener = myCustomEditTextListener;
            editTextAmount.addTextChangedListener(myCustomEditTextListener);
            textViewButton.setOnClickListener(this);



            editTextAmount.addTextChangedListener(new TextWatcher() {

                boolean isManualChange = false;

                @Override
                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    if (isManualChange) {
                        isManualChange = false;
                        return;
                    }

                    try {
                        String value = s.toString().replace(",", "");
                        String reverseValue = new StringBuilder(value).reverse()
                                .toString();
                        StringBuilder finalValue = new StringBuilder();
                        for (int i = 1; i <= reverseValue.length(); i++) {
                            char val = reverseValue.charAt(i - 1);
                            finalValue.append(val);
                            if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                                finalValue.append(",");
                            }
                        }
                        isManualChange = true;
                        editTextAmount.setText(finalValue.reverse());
                        editTextAmount.setSelection(finalValue.length());
                    } catch (Exception e) {
                        // Do nothing since not a number
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });



        }


        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {

               mItemClickListener.onItemClick(v, getPosition(),data);

            }
        }

    }

    


    // we make TextWatcher to be aware of the position it currently works with
    // this way, once a new item is attached in onBindViewHolder, it will
    // update current position MyCustomEditTextListener, reference to which is kept by ViewHolder
    private class MyCustomEditTextListener implements TextWatcher {
        private int position;
        boolean isManualChange = false;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            if (isManualChange) {
                isManualChange = false;
                return;
            }

            try {
                String value = charSequence.toString().replace(",", "");
                String reverseValue = new StringBuilder(value).reverse()
                        .toString();
                StringBuilder finalValue = new StringBuilder();
                for (int k = 1; k <= reverseValue.length(); k++) {
                    char val = reverseValue.charAt(k - 1);
                    finalValue.append(val);
                    if (k % 3 == 0 && k != reverseValue.length() && k > 0) {
                        finalValue.append(",");
                    }
                }
                isManualChange = true;
                //hodler.editTextAmount.setText(finalValue.reverse());
                //hodler.editTextAmount.setSelection(finalValue.length());

                data.get(position).setAmount(finalValue.reverse().toString());

            } catch (Exception e) {
                // Do nothing since not a number
            }

            //data.get(position).setAmount(charSequence.toString());


        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }


}


