package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ExistingLoanActivity extends AppCompatActivity implements ClickListernerToGuarantee {


    List<Phonebook> contacts = Collections.emptyList();
    List<Phonebook> tempData = Collections.emptyList();
    AdapterYourLoan adapter;
    RecyclerView recyclerView;
    TextView textViewBalance;
    TextView textViewReportingDate;
    TextView textViewInterestRate;
    String TAG;
    String global_loan_amount;
    LinearLayout lyFooter;
    Resources res;
    String country_code;
    int mSelectedItem;
    TextView textViewInfo;
    TextView textViewStatus;
    TextView textViewInterestAmount;
    TextView textViewNoOfInstallments;
    TextView textViewExpireDate;
    LinearLayout lFooter;


    String loanID;
    String guarantorId;
    String status;
    String created;
    String modified;
    String gAmount;
    ImageView imageViewInfo;
    Loans loans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_loan);
        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);
        textViewBalance = (TextView)findViewById(R.id.txtview_balance);
        textViewInterestRate = (TextView)findViewById(R.id.txtv_interest_rate);
        textViewReportingDate = (TextView)findViewById(R.id.txtv_report_date);
        imageViewInfo = (ImageView)findViewById(R.id.image_view_info);
        textViewInfo = (TextView)findViewById(R.id.txt_pw);
        textViewInterestAmount = (TextView)findViewById(R.id.txtv_interest_amt);
        textViewNoOfInstallments = (TextView)findViewById(R.id.txtv_no_installments);
        textViewExpireDate = (TextView)findViewById(R.id.txtv_expire_date);
        textViewStatus = (TextView)findViewById(R.id.txtv_status);
        lFooter = (LinearLayout)findViewById(R.id.footer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // on some click or some loading we need to wait for...

        textViewInfo.setVisibility(View.GONE);



        lyFooter = (LinearLayout)findViewById(R.id.footer);

        TAG = getIntent().getClass().getSimpleName();

        res = getResources();
        country_code = getString(R.string.tz);


        String interest = String.format(res.getString(R.string.interest),Constant.NIL);
        textViewInterestRate.setText(interest);

        String reporting_date = String.format(res.getString(R.string.reporting_date),Constant.NIL);
        textViewReportingDate.setText(reporting_date);

        String interest_amount = String.format(res.getString(R.string.interest_amount),Constant.NIL);
        textViewInterestAmount.setText(interest_amount);

        String no_of_installments = String.format(res.getString(R.string.no_of_installments),Constant.NIL);
        textViewNoOfInstallments.setText(no_of_installments);

        String expire_date = String.format(res.getString(R.string.expire_date),Constant.NIL);
        textViewExpireDate.setText(expire_date);


        final String loan_status = String.format(res.getString(R.string.loan_status),getLoanStatus(Constant.NIL));
        textViewStatus.setText(loan_status);


        contacts = new ArrayList<>();


        ConnectionDetector connectionDetector = new ConnectionDetector(ExistingLoanActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetLoans(ExistingLoanActivity.this).execute();

        } else {
            Toast.makeText(ExistingLoanActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }



        imageViewInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExistingLoanActivity.this,RepaymentScheduleActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        lFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentPassword = new Intent(ExistingLoanActivity.this,ListFindsGuranteesActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constant.ID,loans.getId());
                bundle.putString(Constant.LOAN_AMOUNT,loans.getAmount());
                bundle.putString(Constant.INTEREST_RATE,loans.getInterestRate());
                bundle.putString(Constant.GRACE_PERIOD,loans.gracePeriod);
                bundle.putString(Constant.DAILY_PENALTY,loans.getDailyPenalty());
                bundle.putString(Constant.STATUS,loans.getStatus());
                bundle.putString(Constant.CREATED,loans.getCreated());
                bundle.putString(Constant.MODIFIED,loans.getModified());
                intentPassword.putExtras(bundle);
                startActivity(intentPassword);
                ExistingLoanActivity.this.finish();
            }
        });

    }


    @Override
    public void onItemClick(View view, int position, List<Phonebook> data) {

        switch (view.getId()){

            case R.id.button_request:

                if(removeCommer(calculateGAmount(textViewBalance.getText().toString())) < 1){

                    String alert_msg = String.format(res.getString(R.string.max_amt_reach));
                    Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();


                }else if(data.get(position).getAmount().equals("")){

                    Toast.makeText(this, getString(R.string.prompt_guarantee_amount), Toast.LENGTH_SHORT).show();

                } else if(removeCommer(data.get(position).getAmount()) < 1){

                    Toast.makeText(this, getString(R.string.prompt_guarantee_amount), Toast.LENGTH_SHORT).show();

                }else{


                    if(removeCommer(data.get(position).getAmount()) > removeCommer(global_loan_amount)){

                        //Toast.makeText(this, getString(R.string.u_reach_ur_amt), Toast.LENGTH_SHORT).show();
                        String alert_msg = String.format(res.getString(R.string.max_blc_for_g), country_code, putCommer(calculateGAmount(textViewBalance.getText().toString())));
                        Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();

                    }
                    else if(removeCommer(getAmountRequested(data.get(position).getAmount())) > removeCommer(global_loan_amount)){

                        String alert_msg = String.format(res.getString(R.string.max_blc_for_g), country_code, putCommer(calculateGAmount(textViewBalance.getText().toString())));
                        Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();

                    }else{

                        mSelectedItem = position;
                        tempData = data;
                        gAmount = Integer.toString(removeCommer(data.get(position).getAmount()));



                    }
                }


            break;
        }


    }

    @Override
    public void onItemLongClick(View view, int position, List<Phonebook> data) {

    }


    private String getAmountRequested(String amount){

        int initial_amount = 0;
        int final_amount = 0;
        int sum;

        String defult_amount = textViewBalance.getText().toString();

        if(!defult_amount.equals(getString(R.string.default_balance))){

             initial_amount = Integer.parseInt(defult_amount.replace(",","").trim());
             final_amount = Integer.parseInt(amount.replace(",","").trim());
            sum = initial_amount + final_amount;

        }else{

            sum = Integer.parseInt(amount.replace(",","").trim());
        }


        Log.e(TAG,"CalAmt:"+sum);
        return Integer.toString(sum);

    }


    private String calculateGAmount(String amount){

        int initial_amount = 0; //
        int final_amount = 0;
        int sum;

            initial_amount = Integer.parseInt(global_loan_amount.replace(",","").trim());
            final_amount = Integer.parseInt(amount.replace(",","").trim());
            sum = initial_amount - final_amount;

        return Integer.toString(sum);

    }


    private int removeCommer(String amount){
        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }


    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }



        return amt;
    }





    public class GetLoans extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetLoans(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("LOAN_REQUEST_URL",Urls.POST_LOAN_REQUEST);

                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(ExistingLoanActivity.this);

                    //filter={"where":{"memberId":25}, "order":"created DESC", "limit":1}

                    try {
                        filterJson.put("where", new JSONObject().put("memberId",cache.getUserId()));
                        filterJson.put("order", "id DESC");
                        filterJson.put("limit", "1");
                        //filter[order] =

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }



                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_LOAN_REQUEST+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_REQUEST,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();

            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();


                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(ExistingLoanActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                        JSONArray jsonArray = new JSONArray(result);

                        if (jsonArray.length() > 0) {

                            //Default value for input
                            String id = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String interestRate = Constant.ERROR_100;
                            String gracePeriod = Constant.ERROR_100;
                            String dailyPenalty = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    loans = new Loans();

                                    //Check for json data if has KEY and KEY_VALUE is not Null

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                        id = object.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !object.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                        amount = object.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_INTEREST_RATE) && !object.isNull(Keys.VehicleKeys.KEY_JSON_INTEREST_RATE)) {
                                        interestRate = object.getString(Keys.VehicleKeys.KEY_JSON_INTEREST_RATE);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_GRACE_PERIOD) && !object.isNull(Keys.VehicleKeys.KEY_JSON_GRACE_PERIOD)) {
                                        gracePeriod = object.getString(Keys.VehicleKeys.KEY_JSON_GRACE_PERIOD);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_DAILY_PENALTY) && !object.isNull(Keys.VehicleKeys.KEY_JSON_DAILY_PENALTY)) {
                                        dailyPenalty = object.getString(Keys.VehicleKeys.KEY_JSON_DAILY_PENALTY);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !object.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                        status = object.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                        created = object.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                        modified = object.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                                    }


                                    loanID = id;

                                    loans.setId(id);
                                    loans.setAmount(amount);
                                    loans.setDailyPenalty(dailyPenalty);
                                    loans.setGracePeriod(gracePeriod);
                                    loans.setInterestRate(interestRate);
                                    loans.setStatus(status);
                                    loans.setCreated(created);
                                    loans.setModified(modified);


                                    //data.add(loans);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }


                            //Update Main View


                            textViewBalance.setText(putCommer(amount));

                            String interest = String.format(res.getString(R.string.interest),interestRate);
                            textViewInterestRate.setText(interest);

                            String reporting_date = String.format(res.getString(R.string.reporting_date),formatDate(created));
                            textViewReportingDate.setText(reporting_date);

                            String interest_amount = String.format(res.getString(R.string.interest_amount),Constant.ZERO_AS_VALUE);
                            textViewInterestAmount.setText(interest_amount);

                            String no_of_installments = String.format(res.getString(R.string.no_of_installments),Constant.ZERO_AS_VALUE);
                            textViewNoOfInstallments.setText(no_of_installments);

                            String expire_date = String.format(res.getString(R.string.expire_date),Constant.ZERO_AS_VALUE);
                            textViewExpireDate.setText(expire_date);


                            String loan_status = String.format(res.getString(R.string.loan_status),getLoanStatus(status));
                            textViewStatus.setText(loan_status);


                            textViewInfo.setVisibility(View.VISIBLE);

                            ConnectionDetector connectionDetector = new ConnectionDetector(ExistingLoanActivity.this);
                            if (connectionDetector.isInternetConnected()) {

                                new GetGuarantees(ExistingLoanActivity.this).execute();

                            } else {
                                Toast.makeText(ExistingLoanActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                            }



                        } else {
                            Toast.makeText(ExistingLoanActivity.this,getString(R.string.empty),Toast.LENGTH_SHORT).show();
                        }



                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }


    }


    public class GetGuarantees extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetGuarantees(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = null;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("LOAN_REQUEST_URL",Urls.POST_LOAN_REQUEST);

                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(ExistingLoanActivity.this);

                    //filter={"where":{"memberId":25}, "order":"created DESC", "limit":1}

                    try {
                        filterJson.put("where", new JSONObject().put("loanId",loanID));
                        filterJson.put("order", "id DESC");
                        filterJson.put("include", "guarantor");


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }



                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_LOAN_GUARANTEES+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_REQUEST,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            //progressDialog.dismiss();


            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();


                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(ExistingLoanActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                        JSONArray jsonArray = new JSONArray(result);

                        if (jsonArray.length() > 0) {

                            textViewInfo.setVisibility(View.GONE);

                            //Default value for input
                            String id = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;
                            String guarantorId = Constant.ERROR_100;
                            String fname = Constant.ERROR_100;
                            String lname = Constant.ERROR_100;
                            String phone_number = Constant.ERROR_100;

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    Phonebook phonebook = new Phonebook();

                                    //Check for json data if has KEY and KEY_VALUE is not Null

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                        id = object.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !object.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                        amount = object.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                                        guarantorId = object.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                                    }

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !object.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                        status = object.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                        created = object.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                        modified = object.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                                    }

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_GUARANTOR) && !object.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTOR)) {

                                        JSONObject innerJSON = object.getJSONObject(Keys.VehicleKeys.KEY_JSON_GUARANTOR);


                                        if (innerJSON.has(Keys.VehicleKeys.KEY_JSON_PHONE) && !innerJSON.isNull(Keys.VehicleKeys.KEY_JSON_PHONE)) {
                                            phone_number = innerJSON.getString(Keys.VehicleKeys.KEY_JSON_PHONE);
                                        }
                                        if (innerJSON.has(Keys.VehicleKeys.KEY_JSON_FNAME) && !innerJSON.isNull(Keys.VehicleKeys.KEY_JSON_FNAME)) {
                                            fname = innerJSON.getString(Keys.VehicleKeys.KEY_JSON_FNAME);
                                        }
                                        if (innerJSON.has(Keys.VehicleKeys.KEY_JSON_LNAME) && !innerJSON.isNull(Keys.VehicleKeys.KEY_JSON_LNAME)) {
                                            lname = innerJSON.getString(Keys.VehicleKeys.KEY_JSON_LNAME);
                                        }

                                    }





                                    phonebook.setUser_id(id);
                                    phonebook.setAmount(amount);
                                    phonebook.setStatus(status);
                                    phonebook.setUsername(CapsFL(fname)+" "+CapsFL(lname));
                                    phonebook.setPhone_number(phone_number);


                                    contacts.add(phonebook);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }


                            //Update Main View

                            adapter = new AdapterYourLoan(ExistingLoanActivity.this,contacts);
                            adapter.notifyDataSetChanged();

                            //adapter.setOnLoanItemClickListener(ExistingLoanActivity.this);

                            recyclerView.setAdapter(adapter);

                            recyclerView.setLayoutManager(new LinearLayoutManager(ExistingLoanActivity.this));

                            recyclerView.setItemAnimator(new DefaultItemAnimator());



                        } else {

                            textViewInfo.setText(getString(R.string.no_guarantor));
                        }



                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        textViewInfo.setText(getString(R.string.unable_to_update));


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    textViewInfo.setText(getString(R.string.unable_to_update));
                }
            }


        }
    }

    private String getLoanStatus(String status){
        String result;
        if(status.equals(Constant.ONE_AS_VALUE)){
            result = getString(R.string.approved);
        }else{
            result = getString(R.string.pending);
        }
        return result;
    }


    private String formatDate(String dt){

        DateTimeFormatter inputFormatter = null;
        DateTimeFormatter outputFormatter = null;
        LocalDate date = null;
        String formattedDate = dt;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            date = LocalDate.parse(dt, inputFormatter);
            formattedDate = outputFormatter.format(date);

        }else{
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date2 = null;
            try {
                date2 = inputFormat.parse(dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            formattedDate = outputFormat.format(date2);

        }

        return formattedDate;
    }

    String  CapsFL(String word){
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }


}
