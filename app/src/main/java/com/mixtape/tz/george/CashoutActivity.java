package com.mixtape.tz.george;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CashoutActivity extends AppCompatActivity implements View.OnClickListener {


    EditText editTextInputAmount;
    ImageView imageViewEditPhnNo;
    Button buttonSend;
    TextView textViewBalance;
    TextView textViewPhnNumber;
    ImageView imageViewInfo;

    TextView textViewAccountNo;
    TextView textViewGuarantorBalance;
    String TAG;
    Resources res;

    int x = 0;
    int y = 0;
    int z = 0;

    BroadcastReceiver mBroadcastReceiver;
    public static boolean isAppRunning;
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashout);

        isAppRunning = true;

        editTextInputAmount = (EditText)findViewById(R.id.input_amount);
        imageViewEditPhnNo = (ImageView)findViewById(R.id.imgv_editphn_no);
        buttonSend = (Button)findViewById(R.id.button_send);
        textViewBalance = (TextView)findViewById(R.id.txtv_ac_bal);
        textViewPhnNumber = (TextView)findViewById(R.id.txtview_phnnumber);
        textViewAccountNo = (TextView)findViewById(R.id.txtv_ac_no);
        textViewGuarantorBalance = (TextView)findViewById(R.id.txtv_g_bal);
        relativeLayout = (RelativeLayout) findViewById(R.id.layout1);

        imageViewInfo = (ImageView) findViewById(R.id.image_view_info);

        imageViewInfo.setOnClickListener(this);
        res = getResources();

        TAG = getIntent().getClass().getSimpleName();

        String accountNumberTxtHolder = String.format(res.getString(R.string.ac_no),Constant.ZERO_AS_VALUE);
        textViewAccountNo.setText(accountNumberTxtHolder);

        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),Constant.ZERO_AS_VALUE);
        textViewBalance.setText(accountBalanceTxtHolder);

        String guaranteeBalanceTxtHolder = String.format(res.getString(R.string.g_bal),Constant.ZERO_AS_VALUE);
        textViewGuarantorBalance.setText(guaranteeBalanceTxtHolder);

        buttonSend.setOnClickListener(this);
        imageViewEditPhnNo.setOnClickListener(this);

        editTextInputAmount.addTextChangedListener(new TextWatcher() {

            boolean isManualChange = false;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                if (isManualChange) {
                    isManualChange = false;
                    textViewBalance.setText(putCommer(Integer.toString(x)));
                    return;
                }

                try {
                    String value = s.toString().replace(",", "");
                    String reverseValue = new StringBuilder(value).reverse()
                            .toString();
                    StringBuilder finalValue = new StringBuilder();
                    for (int i = 1; i <= reverseValue.length(); i++) {
                        char val = reverseValue.charAt(i - 1);
                        finalValue.append(val);
                        if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                            finalValue.append(",");
                        }
                    }
                    isManualChange = true;
                    editTextInputAmount.setText(finalValue.reverse());
                    editTextInputAmount.setSelection(finalValue.length());

                    //deduct

                    int bal = x;

                    if (value.length() > 0) {
                        int number = Integer.parseInt(value.toString());
                        bal =x - number;
                    }

                    textViewBalance.setText(putCommer(Integer.toString(bal)));


                } catch (Exception e) {
                    // Do nothing since not a number
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub



            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                /*int number = Integer.parseInt(s.toString());
                x += number;
                textViewBalance.setText(Integer.toString(x));*/

            }
        });

        //BindData

        Cache cache = new Cache(CashoutActivity.this);

        textViewPhnNumber.setText(cache.getPhone_number());

        ConnectionDetector connectionDetector = new ConnectionDetector(CashoutActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetAccount(CashoutActivity.this).execute();

        } else {
            Toast.makeText(CashoutActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }

    }


    private int removeCommer(String amount){
        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.image_view_info:

                showDialog();
                break;

            case R.id.imgv_editphn_no:

                break;
            case R.id.button_send:

                if(getAmount(textViewBalance.getText().toString()) > 0){
                    if(editTextInputAmount.getText().toString().length() > 0){

                        //TODO add the OkHttp for server request for cash out

                        ConnectionDetector connectionDetector = new ConnectionDetector(CashoutActivity.this);
                        if (connectionDetector.isInternetConnected()) {

                            new Withdraw(CashoutActivity.this).execute();

                        } else {
                            Toast.makeText(CashoutActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                        }


                    }else{

                        Toast.makeText(this,getString(R.string.plz_enter_loan_amt),Toast.LENGTH_SHORT).show();


                    }
                }else{
                    Toast.makeText(this,getString(R.string.cash_out_no),Toast.LENGTH_SHORT).show();
                }

                break;


        }
    }


    private void showDialog(){

        String message;

        if(getAmount(textViewBalance.getText().toString()) > 0){

            message = getString(R.string.cash_out_yes);

        }else{
            message = getString(R.string.cash_out_no);

        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(CashoutActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.tz)+" "+textViewBalance.getText().toString());
        dialog.setMessage(message);
        dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".

            }
        })
                .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }



    private int getAmount(String amount){

        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }

    public class GetAccount extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetAccount(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = Constant.NIL;
            while (running) {

                try {


                    String root_username = Constant.ROOT_USER;
                    String root_password = Constant.ROOT_PASSWORD;
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();



                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(CashoutActivity.this);

                    try {
                        filterJson.put("where", new JSONObject().put("memberId",cache.getUserId()));
                        filterJson.put("order", "id DESC");
                        filterJson.put("limit",1);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_ACCOUNT+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS_ACCOUNT,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    JSONArray jsonArray = new JSONArray(result);

                    if (json instanceof JSONArray){

                        JSONObject innerJson = jsonArray.getJSONObject(0);

                        String accountNumber = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_NUMBER);
                        String accountBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_BALANCE);
                        String guaranteeBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_BALANCE);
                        String created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                        String modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                        String id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);


                        x = Integer.parseInt(accountBalance);

                        String accountNumberTxtHolder = String.format(res.getString(R.string.ac_no),accountNumber);
                        textViewAccountNo.setText(accountNumberTxtHolder);

                        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),putCommer(accountBalance));
                        textViewBalance.setText(accountBalanceTxtHolder);

                        String guaranteeBalanceTxtHolder = String.format(res.getString(R.string.g_bal),putCommer(guaranteeBalance));
                        textViewGuarantorBalance.setText(guaranteeBalanceTxtHolder);


                    } else if (json instanceof JSONObject){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(CashoutActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }else{

                        //Neither JsonObject Nor JsonArray
                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(CashoutActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {
                    //Return is []
                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(CashoutActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();

                }
            }


        }
    }


    public class Withdraw extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public Withdraw(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = Constant.NIL;
            while (running) {

                try {


                    String root_username = Constant.ROOT_USER;
                    String root_password = Constant.ROOT_PASSWORD;
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();



                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(CashoutActivity.this);

                    try {
                        filterJson.put("where", new JSONObject().put("memberId",cache.getUserId()));
                        filterJson.put("order", "id DESC");
                        filterJson.put("limit",1);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_ACCOUNT+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS_ACCOUNT,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();


        }
    }

    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }
        return amt;
    }


    int getZ(){
        return x-y;
    }


    @Override
    protected void onResume() {
        super.onResume();

        //register new push message receiver // by doing this, the activity will be notified each time a new message arrives

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)){
                    Snackbar snackbar = Snackbar
                            .make(relativeLayout, intent.getStringExtra(Constant.MESSAGE), Snackbar.LENGTH_INDEFINITE)
                            .setAction("View", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    String ACTIVITY = intent.getStringExtra(Constant.ACTIVITY_TYPE);

                                    Class<?> myClass = null;
                                    Activity myActivity = null;

                                    try {
                                        myClass = Class.forName(getPackageName()+"."+ACTIVITY);
                                        myActivity = (Activity) myClass.newInstance();
                                        Log.e("myActivity",myClass.toString());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    startActivity(new Intent(CashoutActivity.this, myClass));

                                }
                            });

                    snackbar.show();
                }

            }
        }, new IntentFilter(Constant.PUSH_NOTIFICATION));



    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        Log.e(TAG,"Unregister Broadcast");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }
}
