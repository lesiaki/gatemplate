package com.mixtape.tz.george;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.appinvite.FirebaseAppInvite;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Button buttonNext;
    private EditText inputPhonenumber;
    private String phonenumber;
    private TextView txtViewCountryZipCode;
    private TextView textViewCountryName;
    private String ZipCode;
    private String country_name;
    private String country_ISO;
    private String internationalFormat;
    private TextView textViewTerms;
    private LinearLayout LayoutCode;
    private String TAG;

    public static boolean isAppRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        TAG = getIntent().getClass().getName();
        isAppRunning = true;

        inputPhonenumber = (EditText)findViewById(R.id.input_textedit_phonenumber);
        buttonNext = (Button)findViewById(R.id.button_next);
        txtViewCountryZipCode = (TextView)findViewById(R.id.txt_view_phonenumber_country_code);
        textViewCountryName = (TextView)findViewById(R.id.edit_profile_country_name);
        textViewTerms = (TextView)findViewById(R.id.tv_terms);
        LayoutCode = (LinearLayout) findViewById(R.id.layout_code);



        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        country_ISO = manager.getNetworkCountryIso().toUpperCase();

        country_name = CountryName(country_ISO);

        textViewCountryName.setText(country_name);

        ZipCode = this.GetCountryZipCode(manager);

        txtViewCountryZipCode.setText("+"+ZipCode);


        buttonNext.setOnClickListener(this);
        textViewTerms.setOnClickListener(this);
        //LayoutCode.setOnClickListener(this);




        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData != null) {
                            //init analytics if you want to get analytics from your dynamic links
                            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(MainActivity.this);

                            Uri deepLink = pendingDynamicLinkData.getLink();
                            Log.e(TAG,"onSuccess called " + deepLink.toString());
                            Log.e(TAG,"Parameter Path " + deepLink.getPath());
                            Log.e(TAG,"Parameter invitedby " + deepLink.getQueryParameter("invitedby"));
                            Log.e(TAG,"Parameter Query " + deepLink.getQuery());
                            Log.e(TAG,"Parameter Authority " + deepLink.getAuthority());
                            Log.e(TAG,"Parameter Last Path Segment " + deepLink.getLastPathSegment());
                            Log.e(TAG,"Parameter Host " + deepLink.getHost());
                            Log.e(TAG,"Parameter User Info " + deepLink.getUserInfo());

                            Long time  = pendingDynamicLinkData.getClickTimestamp();

                            Toast.makeText(MainActivity.this,deepLink.getUserInfo(),Toast.LENGTH_LONG).show();

                            //logic here, redeem code or whatever

                            FirebaseAppInvite invite = FirebaseAppInvite.getInvitation(pendingDynamicLinkData);
                            if (invite != null) {

                                Log.e(TAG,"invitation-Id " + invite.toString());

                                String invitationId = invite.getInvitationId();
                                if (!TextUtils.isEmpty(invitationId)){
                                    Log.e(TAG,"invitation Id " + invitationId);
                                    Toast.makeText(MainActivity.this,"invitation Id "+invitationId,Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG,"\nonFailure");
                        Toast.makeText(MainActivity.this,"onFailure",Toast.LENGTH_SHORT).show();
                    }
                });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_next:

                if(this.IsValidationClean()){

                    if(this.PhoneNumberLibValidation()){

                        try {

                            ConnectionDetector connectionDetector = new ConnectionDetector(MainActivity.this);
                            if (connectionDetector.isInternetConnected()) {

                                new PostMembersPhoneExists(MainActivity.this).execute();

                            } else {
                                Toast.makeText(MainActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this,getString(R.string.try_leter),Toast.LENGTH_LONG).show();
                        }

                    }

                }else{

                    Toast.makeText(this,getString(R.string.invalid_phonenumber),Toast.LENGTH_SHORT).show();

                }


                break;

            case R.id.tv_terms:
                //startActivity(new Intent(MainActivity.this,LoginActivity.class));
                break;

            case R.id.layout_code:

                Intent i = new Intent(this, CountryCodeActivity.class);
                startActivityForResult(i, 1);

                break;

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }



    private boolean IsValidationClean(){
        boolean is_clear = false;
        phonenumber = inputPhonenumber.getText().toString();

        is_clear = !phonenumber.isEmpty() && (phonenumber.length() == 9 || phonenumber.length() == 10);
        return is_clear;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;

    }

    public String GetCountryZipCode(TelephonyManager manager){
        String CountryID="";
        String CountryZipCode="";
        String CountryName = "";

        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }


    private boolean PhoneNumberLibValidation() {

        boolean  validate_okay = false;
        // get the inputted phone number
        String phoneNumber = phonenumber;


        // On our country, people are used to typing 7 (landline) or 11 (cellphone) digit numbers
        // To make it 7 digit numbers valid, I have to prepend “02″
        if (phoneNumber.length() == 9) {

            /*ZipCode ="+255";
            country_ISO = "TZ";*/
            phoneNumber = ZipCode + phoneNumber;
        }

        // Use the library’s functions
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phNumberProto = null;

        try {

            // I set the default region to TZ (Tanzania)
            // You can find your country code here http://www.iso.org/iso/country_names_and_code_elements
            phNumberProto = phoneUtil.parse(phoneNumber, country_ISO);

        } catch (NumberParseException e) {
            // if there’s any error
        }

        // check if the number is valid
        boolean isValid = phoneUtil.isValidNumber(phNumberProto);

        if (isValid) {

            validate_okay = true;
            // get the valid number’s international format
            internationalFormat = phoneUtil.format(phNumberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);

            //Toast.makeText(this, "Phone number VALID: " + internationalFormat, Toast.LENGTH_SHORT).show();

        } else {

            validate_okay = false;
            // prompt the user when the number is invalid
            Toast.makeText(getBaseContext(),getString(R.string.invalid_phonenumber), Toast.LENGTH_SHORT).show();

        }

        return validate_okay;

    }



    public class PostMembersPhoneExists extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostMembersPhoneExists(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";

                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("POST_MEMBERS_PHN_EXISTS",Urls.POST_MEMBERS_PHONE_EXISTS);

                    JSONObject jsonObject = new JSONObject();




                    //Remove the + sign



                    String phone_num = internationalFormat.replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_PHONE_EXISTS+"?phone="+phone_num)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String exists = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_EXISTS);

                            if(Boolean.parseBoolean(exists)){

                                Intent intentPassword = new Intent(MainActivity.this,LoginActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constant.PHONE_NUMBER,internationalFormat);
                                intentPassword.putExtras(bundle);
                                startActivity(intentPassword);

                            }else{

                                Intent intentSignup = new Intent(MainActivity.this,SignUpActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constant.PHONE_NUMBER,internationalFormat);
                                intentSignup.putExtras(bundle);
                                startActivity(intentSignup);
                            }

                        }


                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }

        }
    }



    private String CountryName(String CountryCodeIso){

        String country_name= "";

        HashMap countryLookupMap = null;
        countryLookupMap = new HashMap();

        countryLookupMap.put("AD","Andorra");
        countryLookupMap.put("AE","United Arab Emirates");
        countryLookupMap.put("AF","Afghanistan");
        countryLookupMap.put("AG","Antigua and Barbuda");
        countryLookupMap.put("AI","Anguilla");
        countryLookupMap.put("AL","Albania");
        countryLookupMap.put("AM","Armenia");
        countryLookupMap.put("AN","Netherlands Antilles");
        countryLookupMap.put("AO","Angola");
        countryLookupMap.put("AQ","Antarctica");
        countryLookupMap.put("AR","Argentina");
        countryLookupMap.put("AS","American Samoa");
        countryLookupMap.put("AT","Austria");
        countryLookupMap.put("AU","Australia");
        countryLookupMap.put("AW","Aruba");
        countryLookupMap.put("AZ","Azerbaijan");
        countryLookupMap.put("BA","Bosnia and Herzegovina");
        countryLookupMap.put("BB","Barbados");
        countryLookupMap.put("BD","Bangladesh");
        countryLookupMap.put("BE","Belgium");
        countryLookupMap.put("BF","Burkina Faso");
        countryLookupMap.put("BG","Bulgaria");
        countryLookupMap.put("BH","Bahrain");
        countryLookupMap.put("BI","Burundi");
        countryLookupMap.put("BJ","Benin");
        countryLookupMap.put("BM","Bermuda");
        countryLookupMap.put("BN","Brunei");
        countryLookupMap.put("BO","Bolivia");
        countryLookupMap.put("BR","Brazil");
        countryLookupMap.put("BS","Bahamas");
        countryLookupMap.put("BT","Bhutan");
        countryLookupMap.put("BV","Bouvet Island");
        countryLookupMap.put("BW","Botswana");
        countryLookupMap.put("BY","Belarus");
        countryLookupMap.put("BZ","Belize");
        countryLookupMap.put("CA","Canada");
        countryLookupMap.put("CC","Cocos (Keeling) Islands");
        countryLookupMap.put("CD","Congo, The Democratic Republic of the");
        countryLookupMap.put("CF","Central African Republic");
        countryLookupMap.put("CG","Congo");
        countryLookupMap.put("CH","Switzerland");
        countryLookupMap.put("CI","Côte d?Ivoire");
        countryLookupMap.put("CK","Cook Islands");
        countryLookupMap.put("CL","Chile");
        countryLookupMap.put("CM","Cameroon");
        countryLookupMap.put("CN","China");
        countryLookupMap.put("CO","Colombia");
        countryLookupMap.put("CR","Costa Rica");
        countryLookupMap.put("CU","Cuba");
        countryLookupMap.put("CV","Cape Verde");
        countryLookupMap.put("CX","Christmas Island");
        countryLookupMap.put("CY","Cyprus");
        countryLookupMap.put("CZ","Czech Republic");
        countryLookupMap.put("DE","Germany");
        countryLookupMap.put("DJ","Djibouti");
        countryLookupMap.put("DK","Denmark");
        countryLookupMap.put("DM","Dominica");
        countryLookupMap.put("DO","Dominican Republic");
        countryLookupMap.put("DZ","Algeria");
        countryLookupMap.put("EC","Ecuador");
        countryLookupMap.put("EE","Estonia");
        countryLookupMap.put("EG","Egypt");
        countryLookupMap.put("EH","Western Sahara");
        countryLookupMap.put("ER","Eritrea");
        countryLookupMap.put("ES","Spain");
        countryLookupMap.put("ET","Ethiopia");
        countryLookupMap.put("FI","Finland");
        countryLookupMap.put("FJ","Fiji Islands");
        countryLookupMap.put("FK","Falkland Islands");
        countryLookupMap.put("FM","Micronesia, Federated States of");
        countryLookupMap.put("FO","Faroe Islands");
        countryLookupMap.put("FR","France");
        countryLookupMap.put("GA","Gabon");
        countryLookupMap.put("GB","United Kingdom");
        countryLookupMap.put("GD","Grenada");
        countryLookupMap.put("GE","Georgia");
        countryLookupMap.put("GF","French Guiana");
        countryLookupMap.put("GH","Ghana");
        countryLookupMap.put("GI","Gibraltar");
        countryLookupMap.put("GL","Greenland");
        countryLookupMap.put("GM","Gambia");
        countryLookupMap.put("GN","Guinea");
        countryLookupMap.put("GP","Guadeloupe");
        countryLookupMap.put("GQ","Equatorial Guinea");
        countryLookupMap.put("GR","Greece");
        countryLookupMap.put("GS","South Georgia and the South Sandwich Islands");
        countryLookupMap.put("GT","Guatemala");
        countryLookupMap.put("GU","Guam");
        countryLookupMap.put("GW","Guinea-Bissau");
        countryLookupMap.put("GY","Guyana");
        countryLookupMap.put("HK","Hong Kong");
        countryLookupMap.put("HM","Heard Island and McDonald Islands");
        countryLookupMap.put("HN","Honduras");
        countryLookupMap.put("HR","Croatia");
        countryLookupMap.put("HT","Haiti");
        countryLookupMap.put("HU","Hungary");
        countryLookupMap.put("ID","Indonesia");
        countryLookupMap.put("IE","Ireland");
        countryLookupMap.put("IL","Israel");
        countryLookupMap.put("IN","India");
        countryLookupMap.put("IO","British Indian Ocean Territory");
        countryLookupMap.put("IQ","Iraq");
        countryLookupMap.put("IR","Iran");
        countryLookupMap.put("IS","Iceland");
        countryLookupMap.put("IT","Italy");
        countryLookupMap.put("JM","Jamaica");
        countryLookupMap.put("JO","Jordan");
        countryLookupMap.put("JP","Japan");
        countryLookupMap.put("KE","Kenya");
        countryLookupMap.put("KG","Kyrgyzstan");
        countryLookupMap.put("KH","Cambodia");
        countryLookupMap.put("KI","Kiribati");
        countryLookupMap.put("KM","Comoros");
        countryLookupMap.put("KN","Saint Kitts and Nevis");
        countryLookupMap.put("KP","North Korea");
        countryLookupMap.put("KR","South Korea");
        countryLookupMap.put("KW","Kuwait");
        countryLookupMap.put("KY","Cayman Islands");
        countryLookupMap.put("KZ","Kazakstan");
        countryLookupMap.put("LA","Laos");
        countryLookupMap.put("LB","Lebanon");
        countryLookupMap.put("LC","Saint Lucia");
        countryLookupMap.put("LI","Liechtenstein");
        countryLookupMap.put("LK","Sri Lanka");
        countryLookupMap.put("LR","Liberia");
        countryLookupMap.put("LS","Lesotho");
        countryLookupMap.put("LT","Lithuania");
        countryLookupMap.put("LU","Luxembourg");
        countryLookupMap.put("LV","Latvia");
        countryLookupMap.put("LY","Libyan Arab Jamahiriya");
        countryLookupMap.put("MA","Morocco");
        countryLookupMap.put("MC","Monaco");
        countryLookupMap.put("MD","Moldova");
        countryLookupMap.put("MG","Madagascar");
        countryLookupMap.put("MH","Marshall Islands");
        countryLookupMap.put("MK","Macedonia");
        countryLookupMap.put("ML","Mali");
        countryLookupMap.put("MM","Myanmar");
        countryLookupMap.put("MN","Mongolia");
        countryLookupMap.put("MO","Macao");
        countryLookupMap.put("MP","Northern Mariana Islands");
        countryLookupMap.put("MQ","Martinique");
        countryLookupMap.put("MR","Mauritania");
        countryLookupMap.put("MS","Montserrat");
        countryLookupMap.put("MT","Malta");
        countryLookupMap.put("MU","Mauritius");
        countryLookupMap.put("MV","Maldives");
        countryLookupMap.put("MW","Malawi");
        countryLookupMap.put("MX","Mexico");
        countryLookupMap.put("MY","Malaysia");
        countryLookupMap.put("MZ","Mozambique");
        countryLookupMap.put("NA","Namibia");
        countryLookupMap.put("NC","New Caledonia");
        countryLookupMap.put("NE","Niger");
        countryLookupMap.put("NF","Norfolk Island");
        countryLookupMap.put("NG","Nigeria");
        countryLookupMap.put("NI","Nicaragua");
        countryLookupMap.put("NL","Netherlands");
        countryLookupMap.put("NO","Norway");
        countryLookupMap.put("NP","Nepal");
        countryLookupMap.put("NR","Nauru");
        countryLookupMap.put("NU","Niue");
        countryLookupMap.put("NZ","New Zealand");
        countryLookupMap.put("OM","Oman");
        countryLookupMap.put("PA","Panama");
        countryLookupMap.put("PE","Peru");
        countryLookupMap.put("PF","French Polynesia");
        countryLookupMap.put("PG","Papua New Guinea");
        countryLookupMap.put("PH","Philippines");
        countryLookupMap.put("PK","Pakistan");
        countryLookupMap.put("PL","Poland");
        countryLookupMap.put("PM","Saint Pierre and Miquelon");
        countryLookupMap.put("PN","Pitcairn");
        countryLookupMap.put("PR","Puerto Rico");
        countryLookupMap.put("PS","Palestine");
        countryLookupMap.put("PT","Portugal");
        countryLookupMap.put("PW","Palau");
        countryLookupMap.put("PY","Paraguay");
        countryLookupMap.put("QA","Qatar");
        countryLookupMap.put("RE","Réunion");
        countryLookupMap.put("RO","Romania");
        countryLookupMap.put("RU","Russian Federation");
        countryLookupMap.put("RW","Rwanda");
        countryLookupMap.put("SA","Saudi Arabia");
        countryLookupMap.put("SB","Solomon Islands");
        countryLookupMap.put("SC","Seychelles");
        countryLookupMap.put("SD","Sudan");
        countryLookupMap.put("SE","Sweden");
        countryLookupMap.put("SG","Singapore");
        countryLookupMap.put("SH","Saint Helena");
        countryLookupMap.put("SI","Slovenia");
        countryLookupMap.put("SJ","Svalbard and Jan Mayen");
        countryLookupMap.put("SK","Slovakia");
        countryLookupMap.put("SL","Sierra Leone");
        countryLookupMap.put("SM","San Marino");
        countryLookupMap.put("SN","Senegal");
        countryLookupMap.put("SO","Somalia");
        countryLookupMap.put("SR","Suriname");
        countryLookupMap.put("ST","Sao Tome and Principe");
        countryLookupMap.put("SV","El Salvador");
        countryLookupMap.put("SY","Syria");
        countryLookupMap.put("SZ","Swaziland");
        countryLookupMap.put("TC","Turks and Caicos Islands");
        countryLookupMap.put("TD","Chad");
        countryLookupMap.put("TF","French Southern territories");
        countryLookupMap.put("TG","Togo");
        countryLookupMap.put("TH","Thailand");
        countryLookupMap.put("TJ","Tajikistan");
        countryLookupMap.put("TK","Tokelau");
        countryLookupMap.put("TM","Turkmenistan");
        countryLookupMap.put("TN","Tunisia");
        countryLookupMap.put("TO","Tonga");
        countryLookupMap.put("TP","East Timor");
        countryLookupMap.put("TR","Turkey");
        countryLookupMap.put("TT","Trinidad and Tobago");
        countryLookupMap.put("TV","Tuvalu");
        countryLookupMap.put("TW","Taiwan");
        countryLookupMap.put("TZ","Tanzania");
        countryLookupMap.put("UA","Ukraine");
        countryLookupMap.put("UG","Uganda");
        countryLookupMap.put("UM","United States Minor Outlying Islands");
        countryLookupMap.put("US","United States");
        countryLookupMap.put("UY","Uruguay");
        countryLookupMap.put("UZ","Uzbekistan");
        countryLookupMap.put("VA","Holy See (Vatican City State)");
        countryLookupMap.put("VC","Saint Vincent and the Grenadines");
        countryLookupMap.put("VE","Venezuela");
        countryLookupMap.put("VG","Virgin Islands, British");
        countryLookupMap.put("VI","Virgin Islands, U.S.");
        countryLookupMap.put("VN","Vietnam");
        countryLookupMap.put("VU","Vanuatu");
        countryLookupMap.put("WF","Wallis and Futuna");
        countryLookupMap.put("WS","Samoa");
        countryLookupMap.put("YE","Yemen");
        countryLookupMap.put("YT","Mayotte");
        countryLookupMap.put("YU","Yugoslavia");
        countryLookupMap.put("ZA","South Africa");
        countryLookupMap.put("ZM","Zambia");
        countryLookupMap.put("ZW","Zimbabwe");

        country_name = countryLookupMap.get(CountryCodeIso).toString();

        return country_name;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }


}
