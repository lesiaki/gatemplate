package com.mixtape.tz.george;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by georgejohn on 21/08/2016.
 * https://www.androidbegin.com/tutorial/android-delete-multiple-selected-items-listview-tutorial/
 */

public class AdapterPay extends RecyclerView.Adapter<AdapterPay.GuranterViewHolder> {

    private LayoutInflater inflater;
    Context context;

    List<Loans> data = Collections.emptyList();

    ExistingLoanOnItemClickListener mItemClickListener;

    public AdapterPay(Context context, List<Loans> data) {

        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;

    }

    @Override
    public GuranterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.custome_row_pay, parent, false);

        GuranterViewHolder hodler = new GuranterViewHolder(view);

        return hodler;
    }

    @Override
    public void onBindViewHolder(final GuranterViewHolder holder, final int position) {

                /*String name = data.get(position).getAmount();


                String request = String.format(context.getString(R.string.req_desc),formatDate(data.get(position).getCreated()));
                holder.textViewRequest.setText(request);

                try {
                    holder.textViewAmount.setText(putCommer(data.get(position).getAmount()));
                    if(data.get(position).getGuaranteeAccepted() > 0){

                        Drawable btnborder = context.getResources().getDrawable(R.drawable.round_border_blue);
                        holder.textViewButton.setBackground(btnborder);
                        holder.textViewButton.setTextColor(context.getResources().getColor(R.color.bluel1));
                        holder.textViewButton.setText(context.getString(R.string.accepted));
                        holder.textViewButton.setClickable(false);


                    }else{
                        Drawable btnborder = context.getResources().getDrawable(R.drawable.round_solid_blue);
                        holder.textViewButton.setBackground(btnborder);
                        holder.textViewButton.setText(context.getString(R.string.accept));
                        holder.textViewButton.setTextColor(context.getResources().getColor(R.color.white));
                        holder.textViewButton.setClickable(true);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }*/

    }



    @Override
    public int getItemCount() {
            /*return data.size();*/

        return 10;
    }

    public void setOnLoanItemClickListener(ExistingLoanOnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }



    class GuranterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewAmount;
        TextView textViewRequest;
        TextView textViewButton;
        LinearLayout linearLayout;
        public RelativeLayout viewBackground, viewForeground;

        public GuranterViewHolder(View itemView) {
            super(itemView);

            /*textViewAmount = (TextView) itemView.findViewById(R.id.txtv_amount);
            textViewButton = (TextView) itemView.findViewById(R.id.button);
            textViewRequest = (TextView)itemView.findViewById(R.id.txtv_request);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.line1);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);

            linearLayout.setOnClickListener(this);
            textViewButton.setOnClickListener(this);*/

        }


        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {

                mItemClickListener.onItemClick(v, getPosition(),data);
            }
        }



    }

    private int removeCommer(String amount){
        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }


    private String putCommer(String amount){

        String amt = context.getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }



        return amt;
    }



    private String formatDate(String dt){

        DateTimeFormatter inputFormatter = null;
        DateTimeFormatter outputFormatter = null;
        LocalDate date = null;
        String formattedDate = dt;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            date = LocalDate.parse(dt, inputFormatter);
            formattedDate = outputFormatter.format(date);

        }else{
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date2 = null;
            try {
                date2 = inputFormat.parse(dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            formattedDate = outputFormat.format(date2);

        }

        return formattedDate;
    }


    public void removeItem(int position) {
        data.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(Loans item, int position) {
        data.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }




}


