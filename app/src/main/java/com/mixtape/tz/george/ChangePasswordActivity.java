package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.raycoarana.codeinputview.CodeInputView;

import com.raycoarana.codeinputview.CodeInputView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


//https://github.com/acani/CodeInputView

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {


    TextView textViewPhonenumber;
    EditText editTextPassword;
    TextView textViewCancel;
    TextView textViewResendOTP;
    EditText editTextReTypePassword;
    private String phone_number;
    private String password;
    String retype_password;
    Button buttonLogin;
    CodeInputView codeInputView;
    String code;
    String TAG;

    TextView txtview_strength;
    static int i = 1;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        TAG = getIntent().getClass().getSimpleName();

        textViewPhonenumber = (TextView) findViewById(R.id.tv_phone_number);
        editTextReTypePassword = (EditText)findViewById(R.id.retype_password);
        codeInputView = (CodeInputView)findViewById(R.id.code_input);
        textViewCancel = (TextView)findViewById(R.id.tv_cancel);
        editTextPassword = (EditText)findViewById(R.id.password);
        textViewResendOTP = (TextView)findViewById(R.id.resend_otp);

        buttonLogin = (Button)findViewById(R.id.button_login);

        txtview_strength = (TextView) findViewById(R.id.password_strength);
        pb = (ProgressBar)findViewById(R.id.progressBar);

        try {
            phone_number = getIntent().getStringExtra(Constant.PHONE_NUMBER);
            textViewPhonenumber.setText(phone_number);
        } catch (Exception e) {
            e.printStackTrace();
        }


        buttonLogin.setOnClickListener(this);
        textViewCancel.setOnClickListener(this);
        textViewResendOTP.setOnClickListener(this);

        editTextPassword.addTextChangedListener(new TextWatcher() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                if (editTextPassword.getText().toString().length() == 0) {
                    editTextPassword.setError("Enter your password..!");
                } else {
                    caculation();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

        });



        /*codeInputView.addOnCompleteListener(new OnCodeCompleteListener() {
            @Override
            public void onCompleted(String code) {

                if (validateInputs()){
                    try {

                        ConnectionDetector connectionDetector = new ConnectionDetector(ChangePasswordActivity.this);
                        if (connectionDetector.isInternetConnected()) {

                            new PostMembersChangePassword(ChangePasswordActivity.this).execute();

                        } else {
                            Toast.makeText(ChangePasswordActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {
                        Toast.makeText(ChangePasswordActivity.this,getString(R.string.try_leter),Toast.LENGTH_LONG).show();
                    }
                }

            }
        });*/

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_login:

                        if (validateInputs()){
                            try {

                                ConnectionDetector connectionDetector = new ConnectionDetector(ChangePasswordActivity.this);
                                if (connectionDetector.isInternetConnected()) {

                                    new PostMembersChangePassword(ChangePasswordActivity.this).execute();

                                } else {
                                    Toast.makeText(ChangePasswordActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                }


                            } catch (Exception e) {
                                Toast.makeText(ChangePasswordActivity.this,getString(R.string.try_leter),Toast.LENGTH_LONG).show();
                            }
                        }

                        break;

            case R.id.tv_cancel:

                Intent intentPassword = new Intent(ChangePasswordActivity.this,MainActivity.class);
                /*Bundle bundle = new Bundle();
                bundle.putString(Constant.PHONE_NUMBER,phone_number);
                intentPassword.putExtras(bundle);*/
                startActivity(intentPassword);

                break;

            case R.id.resend_otp:

                ConnectionDetector connectionDetector = new ConnectionDetector(ChangePasswordActivity.this);
                if (connectionDetector.isInternetConnected()) {

                    new SendOTP(ChangePasswordActivity.this).execute();

                } else {
                    Toast.makeText(ChangePasswordActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                }

                break;



                }

        }


    private boolean validateInputs(){


        //https://github.com/acani/CodeInputView

        password = editTextPassword.getText().toString();
        retype_password = editTextReTypePassword.getText().toString();
        code = codeInputView.getCode();


        if(password.isEmpty()){

            Toast.makeText(this,getString(R.string.require_pass),Toast.LENGTH_SHORT).show();
            return false;
        }

        if(password.length() < 8){

            Toast.makeText(this,getString(R.string.minmum_char_pass),Toast.LENGTH_SHORT).show();
            return false;
        }

        if(retype_password.isEmpty()){

            Toast.makeText(this,getString(R.string.require_retype_pass),Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!password.equals(retype_password)){

            Log.e("PassWord",password+":"+retype_password);

            Toast.makeText(this,getString(R.string.pass_not_match),Toast.LENGTH_SHORT).show();
            return false;
        }

        if(codeInputView.getCode().length() < 4){

            Toast.makeText(this,getString(R.string.require_code),Toast.LENGTH_SHORT).show();
            return false;

        }



        return true;


    }



    public class PostMembersChangePassword extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostMembersChangePassword(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    JSONObject jsonObject = new JSONObject();

                    //Remove the + sign

                    String phone_num = phone_number.replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    try {
                        jsonObject.put("phone", phone_num);
                        jsonObject.put("password", password);
                        jsonObject.put("otp",code);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("Parameter",jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_MEMBERS_RESET_PASSWORD_WITH_OTP)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            codeInputView.setCode("");
            codeInputView.setEditable(true);
            try {
                Log.e(TAG+":Reset Pass with OTP",result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(ChangePasswordActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            Toast.makeText(ChangePasswordActivity.this,getString(R.string.password_changed),Toast.LENGTH_SHORT).show();

                            Intent intentPassword = new Intent(ChangePasswordActivity.this,LoginActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(Constant.PHONE_NUMBER,phone_number);
                            intentPassword.putExtras(bundle);
                            startActivity(intentPassword);

                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }



    public class SendOTP extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public SendOTP(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = null;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";

                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("POST_MEMBERS_PHN_EXISTS",Urls.POST_MEMBERS_SEND_OTP);

                    JSONObject jsonObject = new JSONObject();

                    //Remove the + sign

                    String phone_num = phone_number.replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_SEND_OTP+"?phone="+phone_num)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        Log.e(TAG, "Data is J-OB ");

                        JSONObject jsonObject = new JSONObject(result);

                        String sent = jsonObject.getString("sent");

                        if(Boolean.parseBoolean(sent)){

                            Toast.makeText(ChangePasswordActivity.this,getString(R.string.otp_sent_noti),Toast.LENGTH_LONG).show();

                        }


                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void caculation() {
        // TODO Auto-generated method stub
        String temp = editTextPassword.getText().toString();
        System.out.println(i + " current password is : " + temp);
        i = i + 1;

        int length = 0, uppercase = 0, lowercase = 0, digits = 0, symbols = 0, bonus = 0, requirements = 0;

        int lettersonly = 0, numbersonly = 0, cuc = 0, clc = 0;

        length = temp.length();
        for (int i = 0; i < temp.length(); i++) {
            if (Character.isUpperCase(temp.charAt(i)))
                uppercase++;
            else if (Character.isLowerCase(temp.charAt(i)))
                lowercase++;
            else if (Character.isDigit(temp.charAt(i)))
                digits++;

            symbols = length - uppercase - lowercase - digits;

        }

        for (int j = 1; j < temp.length() - 1; j++) {

            if (Character.isDigit(temp.charAt(j)))
                bonus++;

        }

        for (int k = 0; k < temp.length(); k++) {

            if (Character.isUpperCase(temp.charAt(k))) {
                k++;

                if (k < temp.length()) {

                    if (Character.isUpperCase(temp.charAt(k))) {

                        cuc++;
                        k--;

                    }

                }

            }

        }

        for (int l = 0; l < temp.length(); l++) {

            if (Character.isLowerCase(temp.charAt(l))) {
                l++;

                if (l < temp.length()) {

                    if (Character.isLowerCase(temp.charAt(l))) {

                        clc++;
                        l--;

                    }

                }

            }

        }

        /*System.out.println("length" + length);
        System.out.println("uppercase" + uppercase);
        System.out.println("lowercase" + lowercase);
        System.out.println("digits" + digits);
        System.out.println("symbols" + symbols);
        System.out.println("bonus" + bonus);
        System.out.println("cuc" + cuc);
        System.out.println("clc" + clc);*/

        if (length > 7) {
            requirements++;
        }

        if (uppercase > 0) {
            requirements++;
        }

        if (lowercase > 0) {
            requirements++;
        }

        if (digits > 0) {
            requirements++;
        }

        if (symbols > 0) {
            requirements++;
        }

        if (bonus > 0) {
            requirements++;
        }

        if (digits == 0 && symbols == 0) {
            lettersonly = 1;
        }

        if (lowercase == 0 && uppercase == 0 && symbols == 0) {
            numbersonly = 1;
        }

        /*int Total = (length * 4) + ((length - uppercase) * 2)
                + ((length - lowercase) * 2) + (digits * 4) + (symbols * 6)
                + (bonus * 2) + (requirements * 2) - (lettersonly * length*2)
                - (numbersonly * length*3) - (cuc * 2) - (clc * 2);*/

        int Total = (length * 2) + ((length - uppercase) * 2)
                + ((length - lowercase) * 2) + (digits * 4) + (symbols * 6)
                + (bonus * 2) + (requirements * 2) - (lettersonly * length*2)
                - (numbersonly * 2) - (cuc * 2) - (clc * 2);



        System.out.println("Total" + Total);

        if(Total<50){
            pb.setProgress(Total-15);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.red), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.weak));
        }

        else if (Total>=50 && Total <70)
        {
            pb.setProgress(Total-20);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.amber_700), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.medium));
        }

        else if (Total>=70 && Total <100)
        {
            pb.setProgress(Total-25);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.bluel1), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.good));
        }

        else if (Total>=100)
        {
            pb.setProgress(Total-30);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.green), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.strong));
        }
        else{
            pb.setProgress(Total-20);
        }

    }


}
