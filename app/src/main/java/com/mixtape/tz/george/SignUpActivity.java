package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//https://github.com/raycoarana/material-code-input

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {



    TextView textViewLogin;
    Button buttonSignUP;
    EditText editTextFirstName;
    EditText editTextLastName;
    EditText editTextPassword;
    TextView textViewPhoneNumber;
    EditText editTextReTypePassword;


    String first_name;
    String last_name;
    String password;
    String retype_password;
    String TAG;
    String phone_number;
    String OTP = "0";


    TextView txtview_strength;
    static int i = 1;
    ProgressBar pb;
    Cache cache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        textViewLogin = (TextView)findViewById(R.id.text_view_login);
        buttonSignUP = (Button)findViewById(R.id.button_signup);
        editTextFirstName = (EditText)findViewById(R.id.firstname);
        editTextLastName = (EditText)findViewById(R.id.lastname);
        editTextReTypePassword = (EditText)findViewById(R.id.retype_password);

        editTextPassword = (EditText)findViewById(R.id.password);
        textViewPhoneNumber = (TextView)findViewById(R.id.tv_phone_number);

        txtview_strength = (TextView) findViewById(R.id.password_strength);
        pb = (ProgressBar)findViewById(R.id.progressBar);


        textViewLogin.setOnClickListener(this);
        buttonSignUP.setOnClickListener(this);

        TAG = getIntent().getClass().getSimpleName();

        try {
            phone_number = getIntent().getStringExtra(Constant.PHONE_NUMBER);
            textViewPhoneNumber.setText(phone_number);
        } catch (Exception e) {
            e.printStackTrace();
        }


        editTextPassword.addTextChangedListener(new TextWatcher() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                if (editTextPassword.getText().toString().length() == 0) {
                    editTextPassword.setError("Enter your password..!");
                } else {
                    caculation();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

        });


        cache = new Cache(this);

        if(cache.getFirebase_token().equals(Constant.DEFAULT_CACHE_FIREBASE_TOKEN)){
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SignUpActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    Log.e("newToken",newToken);
                    cache.setFirebase_token(newToken);

                }
            });
        }else{

            Log.e("Our Saved newToken",cache.getFirebase_token());

        }




    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.text_view_login:

                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));

                break;

            case R.id.button_signup:


                if(validateInputs()){

                    ConnectionDetector connectionDetector = new ConnectionDetector(SignUpActivity.this);
                    if (connectionDetector.isInternetConnected()) {

                        new PostMembers(SignUpActivity.this).execute();

                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                    }

                }


                break;

        }

    }


    private boolean validateInputs(){

        first_name = editTextFirstName.getText().toString();
        last_name = editTextLastName.getText().toString();
        password = editTextPassword.getText().toString();
        retype_password = editTextReTypePassword.getText().toString();


        if(first_name.isEmpty()){

            Toast.makeText(this,getString(R.string.require_fn),Toast.LENGTH_SHORT).show();
            return false;

        }


        if(last_name.isEmpty()){

            Toast.makeText(this,getString(R.string.require_ln),Toast.LENGTH_SHORT).show();
            return false;
        }


        if(password.isEmpty()){

            Toast.makeText(this,getString(R.string.require_pass),Toast.LENGTH_SHORT).show();
            return false;
        }


        if(password.length() < 8){

            Toast.makeText(this,getString(R.string.minmum_char_pass),Toast.LENGTH_SHORT).show();
            return false;
        }

        if(retype_password.isEmpty()){

            Toast.makeText(this,getString(R.string.require_retype_pass),Toast.LENGTH_SHORT).show();
            return false;
        }


        if(!password.equals(retype_password)){

            Toast.makeText(this,getString(R.string.pass_not_match),Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    public class PostMembers extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostMembers(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("Urls.POST_MEMBERS",Urls.POST_MEMBERS);

                    String phone_num = phone_number.replaceAll("[\\D]", "").trim();


                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("firstName", first_name);
                        jsonObject.put("lastName", last_name);
                        jsonObject.put("phone", phone_num);
                        jsonObject.put("username", phone_num);
                        jsonObject.put("otp", OTP);
                        jsonObject.put("created", "2018-07-30T07:52:59.340Z");
                        jsonObject.put("modified", "2018-07-30T07:52:59.340Z");
                        jsonObject.put("realm", "");
                        jsonObject.put("email", phone_num+"@amini.money");
                        //jsonObject.put("username", username);
                        jsonObject.put("password", password);
                        jsonObject.put("emailVerified", "false");
                        jsonObject.put("firebaseToken", cache.getFirebase_token());




                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_MEMBERS)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String id = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);

                            if(Integer.parseInt(id) > 0){

                                //NOTE the id and redirect to sign-up

                                //Toast.makeText(SignUpActivity.this,getString(R.string.otp_sent_noti),Toast.LENGTH_LONG).show();

                                Intent intentPassword = new Intent(SignUpActivity.this,OTPActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constant.PHONE_NUMBER,phone_number);
                                intentPassword.putExtras(bundle);
                                startActivity(intentPassword);
                                SignUpActivity.this.finish();

                            }else{

                            }

                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void caculation() {
        // TODO Auto-generated method stub
        String temp = editTextPassword.getText().toString();
        System.out.println(i + " current password is : " + temp);
        i = i + 1;

        int length = 0, uppercase = 0, lowercase = 0, digits = 0, symbols = 0, bonus = 0, requirements = 0;

        int lettersonly = 0, numbersonly = 0, cuc = 0, clc = 0;

        length = temp.length();
        for (int i = 0; i < temp.length(); i++) {
            if (Character.isUpperCase(temp.charAt(i)))
                uppercase++;
            else if (Character.isLowerCase(temp.charAt(i)))
                lowercase++;
            else if (Character.isDigit(temp.charAt(i)))
                digits++;

            symbols = length - uppercase - lowercase - digits;

        }

        for (int j = 1; j < temp.length() - 1; j++) {

            if (Character.isDigit(temp.charAt(j)))
                bonus++;

        }

        for (int k = 0; k < temp.length(); k++) {

            if (Character.isUpperCase(temp.charAt(k))) {
                k++;

                if (k < temp.length()) {

                    if (Character.isUpperCase(temp.charAt(k))) {

                        cuc++;
                        k--;

                    }

                }

            }

        }

        for (int l = 0; l < temp.length(); l++) {

            if (Character.isLowerCase(temp.charAt(l))) {
                l++;

                if (l < temp.length()) {

                    if (Character.isLowerCase(temp.charAt(l))) {

                        clc++;
                        l--;

                    }

                }

            }

        }

        System.out.println("length" + length);
        System.out.println("uppercase" + uppercase);
        System.out.println("lowercase" + lowercase);
        System.out.println("digits" + digits);
        System.out.println("symbols" + symbols);
        System.out.println("bonus" + bonus);
        System.out.println("cuc" + cuc);
        System.out.println("clc" + clc);

        if (length > 7) {
            requirements++;
        }

        if (uppercase > 0) {
            requirements++;
        }

        if (lowercase > 0) {
            requirements++;
        }

        if (digits > 0) {
            requirements++;
        }

        if (symbols > 0) {
            requirements++;
        }

        if (bonus > 0) {
            requirements++;
        }

        if (digits == 0 && symbols == 0) {
            lettersonly = 1;
        }

        if (lowercase == 0 && uppercase == 0 && symbols == 0) {
            numbersonly = 1;
        }

        /*int Total = (length * 4) + ((length - uppercase) * 2)
                + ((length - lowercase) * 2) + (digits * 4) + (symbols * 6)
                + (bonus * 2) + (requirements * 2) - (lettersonly * length*2)
                - (numbersonly * length*3) - (cuc * 2) - (clc * 2);*/

        int Total = (length * 2) + ((length - uppercase) * 2)
                + ((length - lowercase) * 2) + (digits * 4) + (symbols * 6)
                + (bonus * 2) + (requirements * 2) - (lettersonly * length*2)
                - (numbersonly * 2) - (cuc * 2) - (clc * 2);



        System.out.println("Total" + Total);

        if(Total<50){
            pb.setProgress(Total-15);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.red), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.weak));
        }

        else if (Total>=50 && Total <70)
        {
            pb.setProgress(Total-20);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.amber_700), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.medium));
        }

        else if (Total>=70 && Total <100)
        {
            pb.setProgress(Total-25);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.bluel1), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.good));
        }

        else if (Total>=100)
        {
            pb.setProgress(Total-30);
            pb.getProgressDrawable().setColorFilter(getColor(R.color.green), PorterDuff.Mode.SRC_IN);
            txtview_strength.setText(getString(R.string.strong));
        }
        else{
            pb.setProgress(Total-20);
        }

    }




}
