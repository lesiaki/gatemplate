package com.mixtape.tz.george;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by georgejohn on 14/11/2017.
 */

public class DatabasePhonebook {

    List<Phonebook> data = Collections.emptyList();
    List<Phonebook> read_data = Collections.emptyList();

    // Database handles
    private Context context; // Application's handle
    private SQLiteDatabase database; // Database connection handle
    private DatabaseHelperPhonebook dbHelper; // Database creation/upgrading helper

	/*------------------General Database Control Functions-------------------*/

    /**
     * Constructor
     *
     * @param context
     *            -> Application's context
     */
    public DatabasePhonebook(Context context) {
        // Initialize global variables
        this.context = context;
        database = null;
        dbHelper = null;
    }

    /**
     * Obtain database handle
     *
     * @return this class's handle
     * @throws SQLException
     */
    public DatabasePhonebook open() throws SQLException {
        dbHelper = new DatabaseHelperPhonebook(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    // Close the database connection
    public void close() {
        dbHelper.close();
    }

    /*
    * Save Data To Cache
    * */

    public boolean saveCache(List<Phonebook> data) {

        this.data = new ArrayList<>();
        this.data = data;
        long id = -1;
        ContentValues cv = new ContentValues();

        for(int i = 0; i < data.size(); i++){

            cv.put(Keys.VehicleKeys.KEY_JSON_USER_ID,this.data.get(i).getUser_id());
            cv.put(Keys.VehicleKeys.KEY_JSON_USERNAME,this.data.get(i).getUsername());
            cv.put(Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER,this.data.get(i).getPhone_number());
            cv.put(Keys.VehicleKeys.KEY_JSON_STATUS,this.data.get(i).getStatus());
            cv.put(Keys.VehicleKeys.KEY_JSON_DATE,this.data.get(i).getDate_in());

            id = database.insert(Keys.VehicleKeys.TABLE_PHONEBOOK, null, cv);

        }

        return true;

    }


    public boolean saveContact(String user_id,String username, String phone_number,String status,String datein) {

        long id = -1;
        ContentValues cv = new ContentValues();

        cv.put(Keys.VehicleKeys.KEY_JSON_USER_ID,user_id);
        cv.put(Keys.VehicleKeys.KEY_JSON_USERNAME,username);
        cv.put(Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER,phone_number);
        cv.put(Keys.VehicleKeys.KEY_JSON_STATUS,status);
        cv.put(Keys.VehicleKeys.KEY_JSON_DATE,datein);

        id = database.insert(Keys.VehicleKeys.TABLE_PHONEBOOK, null, cv);

        return true;

    }


    public int updateStatus(String phone_number){

        int result = 0;

        int status = 1;

        ContentValues cv = new ContentValues();

        cv.put(Keys.VehicleKeys.KEY_JSON_STATUS, status);

        result = database.update(Keys.VehicleKeys.TABLE_PHONEBOOK, cv, Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER + "= ?", new String[] {phone_number});

        return result;

    }


    public int updateUserID(String phone_number, String user_id){

        int result = 0;

        ContentValues cv = new ContentValues();

        cv.put(Keys.VehicleKeys.KEY_JSON_USER_ID, user_id);

        result = database.update(Keys.VehicleKeys.TABLE_PHONEBOOK, cv, Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER + "= ? ", new String[] {phone_number});

        return result;

    }




    public List<Phonebook> getPhoneBook(){

        List<Phonebook> list = new ArrayList<>();


        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER+", " +
                Keys.VehicleKeys.KEY_JSON_USERNAME+"," +
                Keys.VehicleKeys.KEY_JSON_DATE+"," +
                Keys.VehicleKeys.KEY_JSON_USER_ID+"," +
                Keys.VehicleKeys.KEY_JSON_STATUS+
                " FROM " + Keys.VehicleKeys.TABLE_PHONEBOOK;

        Cursor c = database.rawQuery(query,null);

        if (c.moveToFirst()) {

            do {
                Phonebook phonebook = new Phonebook();

                phonebook.setUsername(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USERNAME)));
                phonebook.setPhone_number(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER)));
                phonebook.setStatus(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STATUS)));
                phonebook.setDate_in(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DATE)));
                phonebook.setUser_id(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USER_ID)));

                // Add book to books
                list.add(phonebook);
            } while (c.moveToNext());

        }


        return list;

    }


    public List<Phonebook> getPhoneBookByStatus(){

        List<Phonebook> list = new ArrayList<>();
        String status = Constant.ZERO_AS_VALUE;


        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER+", " +
                Keys.VehicleKeys.KEY_JSON_USERNAME+"," +
                Keys.VehicleKeys.KEY_JSON_DATE+"," +
                Keys.VehicleKeys.KEY_JSON_USER_ID+"," +
                Keys.VehicleKeys.KEY_JSON_STATUS+
                " FROM " + Keys.VehicleKeys.TABLE_PHONEBOOK+" WHERE "+ Keys.VehicleKeys.KEY_JSON_STATUS +" = ? LIMIT "+Constant.LIMIT;

        Cursor c = database.rawQuery(query,new String[]{status});

        if (c.moveToFirst()) {

            do {
                Phonebook phonebook = new Phonebook();

                phonebook.setUsername(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USERNAME)));
                phonebook.setPhone_number(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER)));
                phonebook.setStatus(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STATUS)));
                phonebook.setDate_in(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DATE)));
                phonebook.setUser_id(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USER_ID)));

                // Add book to books
                list.add(phonebook);
            } while (c.moveToNext());

        }


        return list;

    }


    public List<Phonebook> getFriends(){

        List<Phonebook> list = new ArrayList<>();

        String status = Constant.ZERO_AS_VALUE;


        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER+", " +
                Keys.VehicleKeys.KEY_JSON_USERNAME+"," +
                Keys.VehicleKeys.KEY_JSON_DATE+"," +
                Keys.VehicleKeys.KEY_JSON_USER_ID+"," +
                Keys.VehicleKeys.KEY_JSON_STATUS+
                " FROM " + Keys.VehicleKeys.TABLE_PHONEBOOK+" WHERE "+ Keys.VehicleKeys.KEY_JSON_USER_ID +" > ? ";

        Cursor c = database.rawQuery(query,new String[]{ status });

        if (c.moveToFirst()) {

            do {
                Phonebook phonebook = new Phonebook();

                phonebook.setUsername(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USERNAME)));
                phonebook.setPhone_number(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER)));
                phonebook.setStatus(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STATUS)));
                phonebook.setDate_in(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DATE)));
                phonebook.setUser_id(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USER_ID)));

                // Add book to books
                list.add(phonebook);
            } while (c.moveToNext());

        }


        return list;

    }


    public List<Phonebook> getInvitees(){

        List<Phonebook> list = new ArrayList<>();

        String status = Constant.ZERO_AS_VALUE;

        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER+", " +
                Keys.VehicleKeys.KEY_JSON_USERNAME+"," +
                Keys.VehicleKeys.KEY_JSON_DATE+"," +
                Keys.VehicleKeys.KEY_JSON_USER_ID+"," +
                Keys.VehicleKeys.KEY_JSON_STATUS+
                " FROM " + Keys.VehicleKeys.TABLE_PHONEBOOK +" WHERE "+ Keys.VehicleKeys.KEY_JSON_USER_ID +" = ? ";

        Cursor c = database.rawQuery(query,new String[]{status});

        if (c.moveToFirst()) {

            do {
                Phonebook phonebook = new Phonebook();

                phonebook.setUsername(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USERNAME)));
                phonebook.setPhone_number(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER)));
                phonebook.setStatus(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STATUS)));
                phonebook.setDate_in(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DATE)));
                phonebook.setUser_id(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USER_ID)));

                // Add book to books
                list.add(phonebook);
            } while (c.moveToNext());

        }


        return list;

    }




    /*



    public int getStreetID(String street_name){

        int result = 0;
        Cursor c = database.rawQuery("SELECT "+Keys.VehicleKeys.KEY_JSON_STREET_ID+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_STREET_NAME +" = '"+street_name+"'",null);

        if (c.moveToFirst()) {
            result = c.getInt(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STREET_ID));
        }
        return result;

    }


    public List<Phonebook> getLocation(String street_id){

        List<Phonebook> list = Collections.emptyList();
        Phonebook phonebook = new Phonebook();

        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_STREET_NAME+", " +
                                    Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME+"," +
                                        Keys.VehicleKeys.KEY_JSON_REGION_NAME+
                                            " FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_STREET_ID +" = ? ";

        Cursor c = database.rawQuery(query,new String[] { street_id });
        if (c.moveToFirst()) {

            phonebook.setStreet_name(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STREET_NAME)));
            phonebook.setRegion_name(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_REGION_NAME)));

        }

        list.add(phonebook);
        return list;

    }


    public int getDistrictID(String district_name){

        int result = 0;
        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_DISTRICT_ID+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME +" = ? ";

        Log.e("Query: ",query);

        Cursor c = database.rawQuery(query,new String[] { district_name });
        if (c.moveToFirst()) {
            result = c.getInt(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DISTRICT_ID));
        }
        return result;

    }

    public int getDistrictIDFromDistrictTable(String district_name){

        int result = 0;
        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_DISTRICT_ID+" FROM " + Keys.VehicleKeys.DISTRICT_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME +" = ? ";

        Cursor c = database.rawQuery(query,new String[] { district_name });
        if (c.moveToFirst()) {
            result = c.getInt(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DISTRICT_ID));
        }
        return result;

    }

    public int getRegionID(String region_name){

        int result = 2;

        region_name = region_name.replaceAll("'","\'");

        Cursor c = database.rawQuery("SELECT "+Keys.VehicleKeys.KEY_JSON_REGION_ID+" FROM " + Keys.VehicleKeys.DISTRICT_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_REGION_NAME +" = '"+region_name+"'",null);

        if (c.moveToFirst()) {
            result = c.getInt(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_REGION_ID));
        }
        return result;

    }



    public String getNumberOfHouses(String street_name){

        street_name = street_name.replaceAll("'","\'");
        String result = "0";
        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_NO_OF_HOUSE+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_STREET_NAME +" = ?";

        Cursor c = database.rawQuery(query,new String[] { street_name });
        if (c.moveToFirst()) {
            result = c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_NO_OF_HOUSE));
        }
        return result;

    }


    public String getNumberOfDalalis(String street_name){

        String result = "0";
        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_NO_OF_DALALI+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_STREET_NAME +" = ? ";

        Cursor c = database.rawQuery(query,new String[] { street_name });
        if (c.moveToFirst()) {
            result = c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_NO_OF_DALALI));
        }
        return result;

    }

    public String getNumberOfPlots(String street_name){

        String result = "0";
        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_NO_OF_PLOTS+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+ Keys.VehicleKeys.KEY_JSON_STREET_NAME +" = ? ";

        Cursor c = database.rawQuery(query,new String[] { street_name });
        if (c.moveToFirst()) {
            result = c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_NO_OF_PLOTS));
        }
        return result;

    }

    public List<Phonebook> getStreets(String region) {
        read_data = new ArrayList<>();

        // 1. build the query

        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_STREET_NAME+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+Keys.VehicleKeys.KEY_JSON_REGION_NAME+ " = ?";



        // 2. get reference to writable DB
        Cursor c = database.rawQuery(query, new String[] { region });
        //Cursor c = database.query(Keys.VehicleKeys.STREET_ADDRESS_TABLE, new String[]{Keys.VehicleKeys.KEY_JSON_STREET_NAME,Keys.VehicleKeys.KEY_JSON_NO_OF_HOUSE}, null, null, null, null, Keys.VehicleKeys.KEY_JSON_NO_OF_HOUSE+" DESC");

        // 3. go over each row, build book and add it to list
        Phonebook current = null;
        if (c.moveToFirst()) {
            do {
                current = new Phonebook();

                current.setStreet_name(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STREET_NAME)));

                // Add book to books
                read_data.add(current);
            } while (c.moveToNext());
        }


        // return books
        return read_data;
    }

    public List<Phonebook> getStreetsOrderByDalalis(String region) {
        read_data = new ArrayList<>();

        // 1. build the query

        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_STREET_NAME+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+Keys.VehicleKeys.KEY_JSON_REGION_NAME+ " = ? ORDER BY "+Keys.VehicleKeys.KEY_JSON_NO_OF_DALALI+" DESC";


        // 2. get reference to writable DB
        Cursor c = database.rawQuery(query, new String[] { region });

        // 3. go over each row, build book and add it to list
        Phonebook current = null;
        if (c.moveToFirst()) {
            do {
                current = new Phonebook();

                current.setStreet_name(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STREET_NAME)));

                // Add book to books
                read_data.add(current);
            } while (c.moveToNext());
        }


        // return books
        return read_data;
    }


    public List<Phonebook> getStreetsOrderByPlots(String region) {
        read_data = new ArrayList<>();

        // 1. build the query

        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_STREET_NAME+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+Keys.VehicleKeys.KEY_JSON_REGION_NAME+ " = ? ORDER BY "+Keys.VehicleKeys.KEY_JSON_NO_OF_PLOTS+" DESC";


        // 2. get reference to writable DB
        Cursor c = database.rawQuery(query, new String[] { region });

        // 3. go over each row, build book and add it to list
        Phonebook current = null;
        if (c.moveToFirst()) {
            do {
                current = new Phonebook();

                current.setStreet_name(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_STREET_NAME)));

                // Add book to books
                read_data.add(current);
            } while (c.moveToNext());
        }


        // return books
        return read_data;
    }


    public List<Phonebook> getDistrictsX(String region) {
        read_data = new ArrayList<>();

        // 1. build the query

        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME+" FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE +" WHERE "+Keys.VehicleKeys.KEY_JSON_REGION_NAME+ " = ? GROUP BY "+Keys.VehicleKeys.KEY_JSON_DISTRICT_ID;


        // 2. get reference to writable DB
        Cursor c = database.rawQuery(query, new String[] { region });

        // 3. go over each row, build book and add it to list
        Phonebook current = null;
        if (c.moveToFirst()) {
            do {
                current = new Phonebook();

                current.setDistrict_name(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME)));

                // Add book to books
                read_data.add(current);
            } while (c.moveToNext());
        }


        // return books
        return read_data;
    }


    public List<Phonebook> getDistricts(String region) {
        read_data = new ArrayList<>();

        // 1. build the query

        String query = "SELECT "+Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME+" FROM " + Keys.VehicleKeys.DISTRICT_ADDRESS_TABLE +" WHERE "+Keys.VehicleKeys.KEY_JSON_REGION_NAME+ " = ? GROUP BY "+Keys.VehicleKeys.KEY_JSON_DISTRICT_ID;


        // 2. get reference to writable DB
        Cursor c = database.rawQuery(query, new String[] { region });

        // 3. go over each row, build book and add it to list
        Phonebook current = null;
        if (c.moveToFirst()) {
            do {
                current = new Phonebook();

                current.setDistrict_name(c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME)));

                // Add book to books
                read_data.add(current);
            } while (c.moveToNext());
        }


        // return books
        return read_data;
    }





    public boolean deleteCacheDistrict() {
        try {
            database.delete(Keys.VehicleKeys.DISTRICT_ADDRESS_TABLE, null, null);
        } catch (SQLiteException e) {
            return false;
        } catch (Exception e) {
            return false;
        }

        return true;
    }




    public int getNumberOfRows(){

        Cursor c = database.rawQuery("SELECT * FROM " + Keys.VehicleKeys.STREET_ADDRESS_TABLE, null);
        int cnt = c.getCount();
        c.close();
        return cnt;
    }

    public int getNumberOfRowsDistrict(){

        Cursor c = database.rawQuery("SELECT * FROM " + Keys.VehicleKeys.DISTRICT_ADDRESS_TABLE, null);
        int cnt = c.getCount();
        c.close();
        return cnt;
    }



    public int addStreet(Phonebook phonebook){


        long id = -1;
        ContentValues cv = new ContentValues();

        cv.put(Keys.VehicleKeys.KEY_JSON_STREET_ID, phonebook.getStreet_id());
        cv.put(Keys.VehicleKeys.KEY_JSON_STREET_NAME, phonebook.getStreet_name());
        cv.put(Keys.VehicleKeys.KEY_JSON_SUB_STREET_NAME, phonebook.getSub_street_name());
        cv.put(Keys.VehicleKeys.KEY_JSON_DISTRICT_ID, phonebook.getDistrict_id());
        cv.put(Keys.VehicleKeys.KEY_JSON_DISTRICT_NAME, phonebook.getDistrict_name());
        cv.put(Keys.VehicleKeys.KEY_JSON_REGION_ID, phonebook.getRegion_id());
        cv.put(Keys.VehicleKeys.KEY_JSON_REGION_NAME, phonebook.getRegion_name());
        cv.put(Keys.VehicleKeys.KEY_JSON_NO_OF_HOUSE, phonebook.getNo_of_house());
        cv.put(Keys.VehicleKeys.KEY_JSON_NO_OF_DALALI, phonebook.getNo_of_dalalis());
        cv.put(Keys.VehicleKeys.KEY_JSON_NO_OF_PLOTS, phonebook.getNo_of_plots());

        try {
            id = database.insert(Keys.VehicleKeys.STREET_ADDRESS_TABLE, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (int)id;

    }



    public int updateNoOfHouseInStreet(String street_id,String street_name,int no_of_houses){

        int result = 0;

        int total_house  = no_of_houses + 1;

        ContentValues cv = new ContentValues();

        cv.put(Keys.VehicleKeys.KEY_JSON_NO_OF_HOUSE, Integer.toString(total_house));

        result = database.update(Keys.VehicleKeys.STREET_ADDRESS_TABLE, cv, Keys.VehicleKeys.KEY_JSON_STREET_ID + "= ?", new String[] {street_id});

        return result;

    }

    public int updateNoOfDalaliInStreet(String street_id,String street_name,int no_of_dalali){

        int result = 0;


        int total_house  = no_of_dalali + 1;

        ContentValues cv = new ContentValues();

        cv.put(Keys.VehicleKeys.KEY_JSON_NO_OF_DALALI, Integer.toString(total_house));

        result = database.update(Keys.VehicleKeys.STREET_ADDRESS_TABLE, cv, Keys.VehicleKeys.KEY_JSON_STREET_ID + "= ?", new String[] {street_id});

        return result;

    }

    public int updateNoOfPlotsInStreet(String street_id,String street_name,int no_of_plots){

        int result = 0;


        int total_house  = no_of_plots + 1;

        ContentValues cv = new ContentValues();

        cv.put(Keys.VehicleKeys.KEY_JSON_NO_OF_PLOTS, Integer.toString(total_house));

        result = database.update(Keys.VehicleKeys.STREET_ADDRESS_TABLE, cv, Keys.VehicleKeys.KEY_JSON_STREET_ID + "= ?", new String[] {street_id});

        return result;

    }
*/

    public boolean deleteCache() {
        try {
            database.delete(Keys.VehicleKeys.TABLE_PHONEBOOK, null, null);
        } catch (SQLiteException e) {
            return false;
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public String getTableAsString( String tableName) {

        Log.d("DatabasePhonebook", "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows  = database.rawQuery("SELECT * FROM " + tableName, null);
        if (allRows.moveToFirst() ){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }

        return tableString;
    }

    public String getName(String phoneNumber){

        String name = "UnKnown";

        String query = "SELECT "+ Keys.VehicleKeys.KEY_JSON_USERNAME+ " FROM " + Keys.VehicleKeys.TABLE_PHONEBOOK+" WHERE "+ Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER +" == ?";

        Cursor c = database.rawQuery(query,new String[]{phoneNumber});

        if (c.moveToFirst()) {

            name = c.getString(c.getColumnIndex(Keys.VehicleKeys.KEY_JSON_USERNAME));

        }

        c.close();
        return name;
    }

}
