package com.mixtape.tz.george;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SavingActivity extends AppCompatActivity implements View.OnClickListener {



    RecyclerView recyclerView;
    AdapterSaving adapterSaving;
    TextView textViewBalance;
    TextView textViewPhoneNumber;
    EditText inputAmount;
    LinearLayout linearLayoutBarTitle;
    private List<Payments> data = Collections.emptyList();
    String TAG;
    Button button;
    CardView cardView;
    int amount_deposited = 0;
    String phone_to_deposit = Constant.ERROR_100;
    String merchantId = "1100";
    ImageView imageViewEdit;


    String internationalFormat;
    private String ZipCode = "+255";
    TelephonyManager manager;
    String country_ISO;
    String country_CODE;
    Resources res;

    TextView textViewAccountNo;
    TextView textViewGuarantorBalance;

    BroadcastReceiver mBroadcastReceiver;
    public static boolean isAppRunning;
    LinearLayout relativeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saving);
        isAppRunning = true;

        TAG = getIntent().getClass().getSimpleName();

        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);
        textViewBalance = (TextView)findViewById(R.id.txtv_ac_bal);
        button = (Button) findViewById(R.id.button);
        linearLayoutBarTitle = (LinearLayout)findViewById(R.id.bar_title);
        cardView = (CardView)findViewById(R.id.card_view);
        textViewPhoneNumber = (TextView)findViewById(R.id.txtv_phone_number);
        inputAmount = (EditText)findViewById(R.id.amount);
        imageViewEdit = (ImageView)findViewById(R.id.edit_phn);

        textViewAccountNo = (TextView)findViewById(R.id.txtv_ac_no);
        textViewGuarantorBalance = (TextView)findViewById(R.id.txtv_g_bal);
        relativeLayout = (LinearLayout)findViewById(R.id.layout1);

        data = new ArrayList<>();
        res = getResources();

        adapterSaving = new AdapterSaving(data,this);
        recyclerView.setAdapter(adapterSaving);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        country_ISO = manager.getNetworkCountryIso().toUpperCase();
        country_CODE = GetCountryZipCode(manager);


        //Data update

        textViewPhoneNumber.setText(new Cache(this).getPhone_number());

        String accountNumberTxtHolder = String.format(res.getString(R.string.ac_no),Constant.ZERO_AS_VALUE);
        textViewAccountNo.setText(accountNumberTxtHolder);

        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),Constant.ZERO_AS_VALUE);
        textViewBalance.setText(accountBalanceTxtHolder);

        String guaranteeBalanceTxtHolder = String.format(res.getString(R.string.g_bal),Constant.ZERO_AS_VALUE);
        textViewGuarantorBalance.setText(guaranteeBalanceTxtHolder);

        button.setOnClickListener(this);
        imageViewEdit.setOnClickListener(this);


        ConnectionDetector connectionDetector = new ConnectionDetector(SavingActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetAccount(SavingActivity.this).execute();

        } else {
            Toast.makeText(SavingActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }

        inputAmount.addTextChangedListener(new TextWatcher() {

            boolean isManualChange = false;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (isManualChange) {
                    isManualChange = false;
                    return;
                }

                try {
                    String value = s.toString().replace(",", "");
                    String reverseValue = new StringBuilder(value).reverse()
                            .toString();
                    StringBuilder finalValue = new StringBuilder();
                    for (int i = 1; i <= reverseValue.length(); i++) {
                        char val = reverseValue.charAt(i - 1);
                        finalValue.append(val);
                        if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                            finalValue.append(",");
                        }
                    }
                    isManualChange = true;
                    inputAmount.setText(finalValue.reverse());
                    inputAmount.setSelection(finalValue.length());
                } catch (Exception e) {
                    // Do nothing since not a number
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.button:

                String amount = inputAmount.getText().toString();
                phone_to_deposit = textViewPhoneNumber.getText().toString().replaceAll("[\\D]", "");

                if(amount.isEmpty() || phone_to_deposit.isEmpty()|| phone_to_deposit.equals(Constant.ERROR_100)){

                    Toast.makeText(SavingActivity.this, R.string.enter_saving, Toast.LENGTH_SHORT).show();

                }else{

                    amount_deposited = removeCommer(amount);

                    ConnectionDetector connectionDetector = new ConnectionDetector(SavingActivity.this);
                    if (connectionDetector.isInternetConnected()) {

                        new DepositAmount(SavingActivity.this).execute();

                    } else {
                        Toast.makeText(SavingActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                    }


                }

                break;

            case R.id.image_view_info:

                showDialog();
                break;

            case R.id.edit_phn:
                ChangePhnDialog(textViewPhoneNumber.getText().toString());
                break;
        }
    }


    public class GetAccount extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetAccount(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = Constant.ROOT_USER;
                    String root_password = Constant.ROOT_PASSWORD;
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();



                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(SavingActivity.this);

                    try {
                        filterJson.put("where", new JSONObject().put("memberId",cache.getUserId()));
                        filterJson.put("order", "id DESC");
                        filterJson.put("limit",1);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_ACCOUNT+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS_ACCOUNT,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    JSONArray jsonArray = new JSONArray(result);

                    if (json instanceof JSONArray){

                        JSONObject innerJson = jsonArray.getJSONObject(0);

                        String accountNumber = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_NUMBER);
                        String accountBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_BALANCE);
                        String guaranteeBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_BALANCE);
                        String created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                        String modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                        String id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);

                        String accountNumberTxtHolder = String.format(res.getString(R.string.ac_no),accountNumber);
                        textViewAccountNo.setText(accountNumberTxtHolder);

                        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),putCommer(accountBalance));
                        textViewBalance.setText(accountBalanceTxtHolder);

                        String guaranteeBalanceTxtHolder = String.format(res.getString(R.string.g_bal),putCommer(guaranteeBalance));
                        textViewGuarantorBalance.setText(guaranteeBalanceTxtHolder);


                    } else if (json instanceof JSONObject){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(SavingActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }else{

                        //Neither JsonObject Nor JsonArray
                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(SavingActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {
                    //Return is []
                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(SavingActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();

                }
            }


        }
    }


    public class DepositAmount extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public DepositAmount(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = Constant.ROOT_USER;
                    String root_password = Constant.ROOT_PASSWORD;
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject jsonObject = new JSONObject();

                    try {

                        jsonObject.put("phone", phone_to_deposit);
                        jsonObject.put("amount", amount_deposited);
                        jsonObject.put("merchantId", merchantId);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());


                    Log.e("Parameter",jsonObject.toString());

                    //http://app.lipawallet.co.tz:3000/api/Payments/customerCashIn

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_CREDIT_WALLET)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            inputAmount.setText("");


            try {

                Object json = new JSONTokener(result).nextValue();

                if (json instanceof JSONObject){
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                        JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                        String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                        String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                        String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                        String status = innerJson.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                        String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                        Log.e(TAG,"Mechant Fail:Message:"+statusCode+":"+message);

                        Toast.makeText(SavingActivity.this,message,Toast.LENGTH_SHORT).show();


                    }else{

                        JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.ACK);

                        JSONObject statusJson = innerJson.getJSONObject(Keys.VehicleKeys.KEY_JSON_STATUS);
                        String code = statusJson.getString(Keys.VehicleKeys.KEY_JSON_CODE);
                        String message = statusJson.getString(Keys.VehicleKeys.KEY_JSON_MESSAGE);

                        String responseCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_RESPONSE_CODE);

                        Log.e(TAG,"Mechant Pass:Message:"+code+":"+message);

                        Toast.makeText(SavingActivity.this,"Please complete with USSD Menu",Toast.LENGTH_SHORT).show();

                    }

                }

            } catch (Exception e) {

                Log.e(TAG, "JsonFail "+e.toString());
            }

        }
    }

    private void showDialog(){

        String message;

        if(removeCommer(textViewBalance.getText().toString()) > 0){

            message = getString(R.string.saving_yes);

        }else{
            message = getString(R.string.saving_no);

        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(SavingActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.tz)+" "+textViewBalance.getText().toString());
        dialog.setMessage(message);
        dialog.setPositiveButton("Borrow", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                startActivity(new Intent(SavingActivity.this,RequestLoanActivity.class));
            }
        })
                .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }






    private int removeCommer(String amount){

        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }



    private void ChangePhnDialog(final String phone_number) {

        final Dialog dialog = new Dialog(SavingActivity.this);

        LayoutInflater inflater = this.getLayoutInflater();

        View view = inflater.inflate(R.layout.layout_change_phn, null);



        TextView textViewCancel = (TextView) view.findViewById(R.id.txt_cancel);
        TextView textViewChange = (TextView) view.findViewById(R.id.txt_change);
        final EditText editTextPhn = (EditText)view.findViewById(R.id.input_phn);


        editTextPhn.setText(phone_number);
        editTextPhn.requestFocus();
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editTextPhn, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
        }


        textViewChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(editTextPhn.getText().toString().length() > 0){
                    if(IsValidationClean(editTextPhn.getText().toString())){

                        if(PhoneNumberLibValidation(editTextPhn.getText().toString())){

                            textViewPhoneNumber.setText(internationalFormat);
                            dialog.dismiss();
                        }else{

                            Toast.makeText(SavingActivity.this,getResources().getString(R.string.invalid_phonenumber),Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(SavingActivity.this,getResources().getString(R.string.invalid_phonenumber),Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(SavingActivity.this,getResources().getString(R.string.enter_phn_no),Toast.LENGTH_SHORT).show();
                }

            }
        });


        textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               dialog.dismiss();

            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(view);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();

    }

    public String GetCountryZipCode(TelephonyManager manager){
        String CountryID="";
        String CountryZipCode="";
        String CountryName = "";

        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl= getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    private boolean IsValidationClean(String phn){
        boolean is_clear = false; //9,10,12,13
        //is_clear = phn.length() == 9 || phn.length() == 10 || phn.length() == 13;
        is_clear = phn.trim().length() > 8;
        return is_clear;
    }

    private boolean PhoneNumberLibValidation(String phn) {

        boolean  validate_okay = false;
        // get the inputted phone number
        String phoneNumber = phn;


        // On our country, people are used to typing 7 (landline) or 11 (cellphone) digit numbers
        // To make it 7 digit numbers valid, I have to prepend “02″
        if (phoneNumber.length() == 9) {

            /*ZipCode ="+255";
            country_ISO = "TZ";*/
            phoneNumber = ZipCode + phoneNumber;
        }

        // Use the library’s functions
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phNumberProto = null;

        try {

            // I set the default region to TZ (Tanzania)
            // You can find your country code here http://www.iso.org/iso/country_names_and_code_elements
            phNumberProto = phoneUtil.parse(phoneNumber, country_ISO);

        } catch (NumberParseException e) {
            // if there’s any error
        }

        // check if the number is valid
        boolean isValid = phoneUtil.isValidNumber(phNumberProto);

        if (isValid) {

            validate_okay = true;
            // get the valid number’s international format
            internationalFormat = phoneUtil.format(phNumberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);

            //Toast.makeText(this, "Phone number VALID: " + internationalFormat, Toast.LENGTH_SHORT).show();

        } else {

            validate_okay = false;
            // prompt the user when the number is invalid
            Log.e(TAG,getString(R.string.invalid_phonenumber));

        }

        return validate_okay;

    }

    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }

        return amt;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //register new push message receiver // by doing this, the activity will be notified each time a new message arrives

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)){
                    Snackbar snackbar = Snackbar
                            .make(relativeLayout, intent.getStringExtra(Constant.MESSAGE), Snackbar.LENGTH_INDEFINITE)
                            .setAction("View", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    String ACTIVITY = intent.getStringExtra(Constant.ACTIVITY_TYPE);

                                    Class<?> myClass = null;
                                    Activity myActivity = null;

                                    try {
                                        myClass = Class.forName(getPackageName()+"."+ACTIVITY);
                                        myActivity = (Activity) myClass.newInstance();
                                        Log.e("myActivity",myClass.toString());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    startActivity(new Intent(SavingActivity.this, myClass));

                                }
                            });

                    snackbar.show();
                }

            }
        }, new IntentFilter(Constant.PUSH_NOTIFICATION));



    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        Log.e(TAG,"Unregister Broadcast");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }
}
