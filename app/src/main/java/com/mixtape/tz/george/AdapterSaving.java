package com.mixtape.tz.george;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;


public class AdapterSaving extends RecyclerView.Adapter<AdapterSaving.MyHolderView> {

    private LayoutInflater inflater;
    Context context;
    Saving saving;
    private List<Payments> data = Collections.emptyList();


    public AdapterSaving(List<Payments> data, Context context){

        //this.data = new ArrayList<>();
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyHolderView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.saving_custom_row,parent,false);

        MyHolderView hodler = new MyHolderView(view);

        return hodler;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolderView holder, int position) {

        holder.textViewAmount.setText(data.get(position).getAmount());
        holder.textViewFrom.setText(data.get(position).getChannelId());
        holder.textViewDate.setText(data.get(position).getCreated());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolderView extends RecyclerView.ViewHolder {


        TextView textViewAmount;
        TextView textViewFrom;
        TextView textViewDate;

        public MyHolderView(View itemView) {
            super(itemView);

            textViewAmount = (TextView)itemView.findViewById(R.id.text_amount);
            textViewFrom = (TextView)itemView.findViewById(R.id.text_from);
            textViewDate = (TextView)itemView.findViewById(R.id.text_date);

        }
    }

}
