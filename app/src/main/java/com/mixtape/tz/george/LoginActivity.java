package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    TextView textViewPhonenumber;
    EditText editTextPassword;
    TextView textViewChangePassword;
    private String phone_number;
    private String password;
    Button buttonLogin;
    String TAG;
    Cache cache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TAG = getIntent().getClass().getSimpleName();
        cache = new Cache(LoginActivity.this);

        textViewPhonenumber = (TextView) findViewById(R.id.tv_phone_number);
        textViewChangePassword = (TextView)findViewById(R.id.tv_change_password);
        editTextPassword = (EditText)findViewById(R.id.input_password);

        buttonLogin = (Button)findViewById(R.id.button_login);

        try {
            phone_number = getIntent().getStringExtra(Constant.PHONE_NUMBER);
            textViewPhonenumber.setText(phone_number);
        } catch (Exception e) {
            e.printStackTrace();
        }


        buttonLogin.setOnClickListener(this);
        textViewChangePassword.setOnClickListener(this);






    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_login:

                password = editTextPassword.getText().toString();

                        if(password.isEmpty()){

                            Toast.makeText(LoginActivity.this,getString(R.string.prompt_password),Toast.LENGTH_LONG).show();

                        }else{
                            try {

                                ConnectionDetector connectionDetector = new ConnectionDetector(LoginActivity.this);
                                if (connectionDetector.isInternetConnected()) {

                                    new PostMembersLogin(LoginActivity.this).execute();

                                } else {
                                    Toast.makeText(LoginActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                }


                            } catch (Exception e) {
                                Toast.makeText(LoginActivity.this,getString(R.string.try_leter),Toast.LENGTH_LONG).show();
                            }
                        }


                        break;


            case R.id.tv_change_password:

                ConnectionDetector connectionDetector = new ConnectionDetector(LoginActivity.this);
                if (connectionDetector.isInternetConnected()) {

                    new SendOTP(LoginActivity.this).execute();

                } else {
                    Toast.makeText(LoginActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                }

                break;



                }

        }



    public class PostMembersLogin extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostMembersLogin(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("Urls.POST_MEMBERS_LOGIN",Urls.POST_MEMBERS_LOGIN);

                    JSONObject jsonObject = new JSONObject();


                    String phone_num = phone_number.replaceAll("[\\D]", "").trim();

                    Log.e("YourPhnNumberPar",phone_num);


                    try {

                        jsonObject.put("username", phone_num);
                        jsonObject.put("phone", phone_num);
                        jsonObject.put("password", password);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_MEMBERS_LOGIN)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String id = Constant.ERROR_100;
                            String userId = Constant.ERROR_100;
                            String ttl = Constant.ERROR_100;
                            String created = Constant.ERROR_100;
                            String otpVerified = Constant.ERROR_100;
                            String fname = Constant.ERROR_100;
                            String lname = Constant.ERROR_100;
                            String firebaseToken = Constant.ERROR_100;
                            String email = Constant.ERROR_100;
                            String plain_phone_number = Constant.ERROR_100;


                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ID) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                id = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_USER_ID) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_USER_ID)) {
                                userId = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_USER_ID);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_TTL) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_TTL)) {
                                ttl = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_TTL);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                created = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED)) {
                                otpVerified = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_FNAME) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_FNAME)) {
                                fname = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_FNAME);
                            }

                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_LNAME) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_LNAME)) {
                                lname = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_LNAME);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_EMAIL) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_EMAIL)) {
                                email = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_EMAIL);
                            }

                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN)) {
                                firebaseToken = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN);
                            }

                            if (jsonObject.has(Keys.VehicleKeys.KEY_PLN_PHN) && !jsonObject.isNull(Keys.VehicleKeys.KEY_PLN_PHN)) {
                                plain_phone_number = jsonObject.getString(Keys.VehicleKeys.KEY_PLN_PHN);
                            }





                            if(Integer.parseInt(userId) > 0){

                                //NOTE the id and redirect to sign-up


                                cache.setId(id);
                                cache.setUserId(userId);
                                cache.setTtl(ttl);
                                cache.setCreated(created);
                                cache.setPhone_number(phone_number);
                                cache.setPlain_phone_number(plain_phone_number);
                                cache.setFname(fname);
                                cache.setLname(lname);
                                cache.setEmail(plain_phone_number+"@amini.money");
                                cache.setPassword(password);


                                if(!firebaseToken.equals(cache.getFirebase_token())){

                                    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( LoginActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                                            @Override
                                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                                String newToken = instanceIdResult.getToken();
                                                Log.e("newToken",newToken);

                                                //Update Cache file in App
                                                cache.setFirebase_token(newToken);

                                                //Update token in Server also

                                                ConnectionDetector connectionDetector = new ConnectionDetector(LoginActivity.this);
                                                if (connectionDetector.isInternetConnected()) {

                                                    new UpdateMember(LoginActivity.this).execute();

                                                } else {
                                                    Toast.makeText(LoginActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                                }


                                            }
                                        });

                                }

                                if(Boolean.parseBoolean(otpVerified)){


                                    try {
                                        DatabasePhonebook db = new DatabasePhonebook(LoginActivity.this);
                                        db.open();

                                        if(db.getPhoneBook().size() > 0){

                                            startActivity(new Intent(LoginActivity.this,DashboardMainActivity.class));
                                            LoginActivity.this.finish();

                                        }else{

                                            startActivity(new Intent(LoginActivity.this,ImportingContactsActivity.class));
                                            LoginActivity.this.finish();
                                        }
                                        db.close();
                                    }catch (Exception e) {
                                        Log.e(TAG,e.toString());
                                    }




                                }else{

                                    Intent intentPassword = new Intent(LoginActivity.this,OTPActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(Constant.PHONE_NUMBER,phone_number);
                                    intentPassword.putExtras(bundle);
                                    startActivity(intentPassword);
                                    LoginActivity.this.finish();

                                }



                            }else{

                                //
                                Toast.makeText(LoginActivity.this,getString(R.string.login_fail),Toast.LENGTH_SHORT).show();

                            }

                        }


                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }


    public class SendOTP extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public SendOTP(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";

                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("POST_MEMBERS_PHN_EXISTS",Urls.POST_MEMBERS_SEND_OTP);

                    JSONObject jsonObject = new JSONObject();

                    //Remove the + sign

                    String phone_num = phone_number.replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_SEND_OTP+"?phone="+phone_num)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String sent = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_SENT);

                            if(Boolean.parseBoolean(sent)){

                                Toast.makeText(LoginActivity.this,getString(R.string.otp_sent_noti),Toast.LENGTH_LONG).show();

                                Intent intentPassword = new Intent(LoginActivity.this,ChangePasswordActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constant.PHONE_NUMBER,phone_number);
                                intentPassword.putExtras(bundle);
                                startActivity(intentPassword);
                                LoginActivity.this.finish();

                            }
                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }


    public class UpdateMember extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public UpdateMember(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("Urls.POST_MEMBERS",Urls.POST_MEMBERS);

                    String phone_num = phone_number.replaceAll("[\\D]", "").trim();


                    JSONObject jsonObject = new JSONObject();
                    try {

                        /*jsonObject.put("firstName", cache.getFname());
                        jsonObject.put("lastName", cache.getLname());
                        jsonObject.put("phone", cache.getPlain_phone_number());
                        jsonObject.put("username", cache.getPlain_phone_number());
                        *//*jsonObject.put("otp", cache.getOtp());*//*
                        jsonObject.put("created", "2018-07-30T07:52:59.340Z");
                        jsonObject.put("modified", "2018-07-30T07:52:59.340Z");
                        jsonObject.put("realm", "");
                        jsonObject.put("email", cache.getEmail());
                        jsonObject.put("password", cache.getPassword());
                        jsonObject.put("emailVerified", "true");
                        jsonObject.put("otpVerified", "true");*/
                        jsonObject.put("firebaseToken", cache.getFirebase_token());




                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("Parameter_update",jsonObject.toString());

                    Request request = new Request.Builder()
                            .header("Authorization", cache.getId())
                            .patch(postData)
                            .url(Urls.POST_MEMBERS+"/"+cache.getUserId())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG + ":" + Urls.POST_MEMBERS, result);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }





}
