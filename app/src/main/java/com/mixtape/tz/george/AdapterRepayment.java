package com.mixtape.tz.george;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by georgejohn on 21/08/2016.
 * https://www.androidbegin.com/tutorial/android-delete-multiple-selected-items-listview-tutorial/
 */

public class AdapterRepayment extends RecyclerView.Adapter<AdapterRepayment.GuranterViewHolder> {

    private LayoutInflater inflater;
    Context context;

    List<Repayments> data = Collections.emptyList();

    ExistingLoanOnItemClickListener mItemClickListener;

    public AdapterRepayment(Context context, List<Repayments> data) {

        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;

    }

    @Override
    public GuranterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.custome_row_repayment, parent, false);

        GuranterViewHolder hodler = new GuranterViewHolder(view);

        return hodler;
    }

    @Override
    public void onBindViewHolder(final GuranterViewHolder holder, final int position) {

                try {
                    holder.textViewAmountPaid.setText(data.get(position).getAmountPaid());
                    holder.textViewDateDue.setText(data.get(position).getDateDue());
                    holder.textViewAmountDue.setText(data.get(position).getAmountDue());

                } catch (Exception e) {
                    e.printStackTrace();
                }

    }



    @Override
    public int getItemCount() {
            return data.size();
    }

    public void ExistingLoanOnItemClickListener(ExistingLoanOnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }



    class GuranterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewDateDue;
        TextView textViewAmountPaid;
        TextView textViewAmountDue;
        LinearLayout linearLayout;

        public GuranterViewHolder(View itemView) {
            super(itemView);

            textViewAmountPaid = (TextView) itemView.findViewById(R.id.txtvamountpaid);
            textViewDateDue = (TextView) itemView.findViewById(R.id.txtvdatedue);
            textViewAmountDue = (TextView)itemView.findViewById(R.id.txtvamountdue);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.line1);

        }


        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {

                //mItemClickListener.onItemClick(v, getPosition(),data);
            }
        }



    }




}


