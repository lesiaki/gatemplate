package com.mixtape.tz.george;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RequestLoanActivity extends AppCompatActivity implements View.OnClickListener {



    EditText inputAmount;
    Button buttonRequest;
    TextView textViewBalance;
    TextView textViewPhnNumber;
    ImageView imageViewInfo;
    String amount;
    String TAG;
    TextView textViewInterestRate;
    TextView textViewAccountNo;
    TextView textViewGuarantorBalance;
    Resources res;


    BroadcastReceiver mBroadcastReceiver;
    public static boolean isAppRunning;
    RelativeLayout relativeLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_loan_layout);

        isAppRunning = true;

        TAG = getIntent().getClass().getSimpleName();
        inputAmount = (EditText)findViewById(R.id.input_amount);
        textViewBalance = (TextView)findViewById(R.id.txtview_balance);
        textViewPhnNumber = (TextView)findViewById(R.id.txtview_phnnumber);
        imageViewInfo = (ImageView) findViewById(R.id.image_view_info);
        textViewAccountNo = (TextView)findViewById(R.id.txtv_ac_no);
        textViewGuarantorBalance = (TextView)findViewById(R.id.txtv_g_bal);
        buttonRequest = (Button)findViewById(R.id.button_request);
        relativeLayout = (RelativeLayout) findViewById(R.id.layout1);
        textViewInterestRate = (TextView)findViewById(R.id.txtv_interest_rate);

        res = getResources();


        String interest = String.format(res.getString(R.string.interest),Constant.ZERO_AS_VALUE);
        textViewInterestRate.setText(interest);

        String accountNumberTxtHolder = String.format(res.getString(R.string.ac_no),Constant.ZERO_AS_VALUE);
        textViewAccountNo.setText(accountNumberTxtHolder);

        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),Constant.ZERO_AS_VALUE);
        textViewBalance.setText(accountBalanceTxtHolder);

        String guaranteeBalanceTxtHolder = String.format(res.getString(R.string.g_bal),Constant.ZERO_AS_VALUE);
        textViewGuarantorBalance.setText(guaranteeBalanceTxtHolder);


        buttonRequest.setOnClickListener(this);
        imageViewInfo.setOnClickListener(this);



        inputAmount.addTextChangedListener(new TextWatcher() {

            boolean isManualChange = false;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (isManualChange) {
                    isManualChange = false;
                    return;
                }

                try {
                    String value = s.toString().replace(",", "");
                    String reverseValue = new StringBuilder(value).reverse()
                            .toString();
                    StringBuilder finalValue = new StringBuilder();
                    for (int i = 1; i <= reverseValue.length(); i++) {
                        char val = reverseValue.charAt(i - 1);
                        finalValue.append(val);
                        if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                            finalValue.append(",");
                        }
                    }
                    isManualChange = true;
                    inputAmount.setText(finalValue.reverse());
                    inputAmount.setSelection(finalValue.length());
                } catch (Exception e) {
                    // Do nothing since not a number
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        //BindData

        Cache cache = new Cache(RequestLoanActivity.this);

        textViewPhnNumber.setText(cache.getPhone_number());

        ConnectionDetector connectionDetector = new ConnectionDetector(RequestLoanActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetAccount(RequestLoanActivity.this).execute();

        } else {
            Toast.makeText(RequestLoanActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.image_view_info:

                showDialog();
                break;

            case R.id.circle_saving:

                startActivity(new Intent(RequestLoanActivity.this,SavingActivity.class));
                break;

            case R.id.circle_garantee:

                startActivity(new Intent(RequestLoanActivity.this,ListofGuranterActivity.class));
                break;

            case R.id.button_request:
                //startActivity(new Intent(RequestLoanActivity.this,ReadContactsActivity.class));

                if(removeCommer(textViewBalance.getText().toString()) > 0){
                    if(inputAmount.getText().toString().length() > 0){


                        amount = inputAmount.getText().toString().replace(",", "").trim();

                    /*Intent intentPassword = new Intent(RequestLoanActivity.this,ListofGuranterActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.LOAN_AMOUNT,loan_amount);
                    intentPassword.putExtras(bundle);
                    startActivity(intentPassword);*/



                        ConnectionDetector connectionDetector = new ConnectionDetector(RequestLoanActivity.this);
                        if (connectionDetector.isInternetConnected()) {

                            new PostLoanRequest(RequestLoanActivity.this).execute();

                        } else {
                            Toast.makeText(RequestLoanActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                        }



                    }else{

                        Toast.makeText(this,getString(R.string.plz_enter_loan_amt),Toast.LENGTH_SHORT).show();


                    }
                }else{

                    showDialog();
                    //Toast.makeText(this,getString(R.string.loan_request_no),Toast.LENGTH_SHORT).show();

                }

                break;

        }


    }

    public class GetAccount extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetAccount(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = Constant.ROOT_USER;
                    String root_password = Constant.ROOT_PASSWORD;
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();



                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(RequestLoanActivity.this);
                    //{"where":{"memberId":"102"},"order":"id DESC","limit":1}

                    try {
                        filterJson.put("where", new JSONObject().put("memberId",cache.getUserId()));
                        filterJson.put("order", "id DESC");
                        filterJson.put("limit",1);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_MEMBERS_ACCOUNT+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS_ACCOUNT,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    JSONArray jsonArray = new JSONArray(result);

                    if (json instanceof JSONArray){

                        JSONObject innerJson = jsonArray.getJSONObject(0);

                        String accountNumber = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_NUMBER);
                        String accountBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ACCOUNT_BALANCE);
                        String guaranteeBalance = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_BALANCE);
                        String created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                        String modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                        String id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);

                        String accountNumberTxtHolder = String.format(res.getString(R.string.ac_no),accountNumber);
                        textViewAccountNo.setText(accountNumberTxtHolder);

                        String accountBalanceTxtHolder = String.format(res.getString(R.string.ac_bal),putCommer(accountBalance));
                        textViewBalance.setText(accountBalanceTxtHolder);

                        String guaranteeBalanceTxtHolder = String.format(res.getString(R.string.g_bal),putCommer(guaranteeBalance));
                        textViewGuarantorBalance.setText(guaranteeBalanceTxtHolder);


                    } else if (json instanceof JSONObject){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(RequestLoanActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }else{

                        //Neither JsonObject Nor JsonArray
                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(RequestLoanActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {
                    //Return is []
                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(RequestLoanActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();

                }
            }


        }
    }

    public class PostLoanRequest extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostLoanRequest(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("LOAN_REQUEST_URL",Urls.POST_LOAN_REQUEST);



                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("amount", amount);
                        jsonObject.put("memberId", new Cache(RequestLoanActivity.this).getUserId());
                        jsonObject.put("interestRate", 0);
                        jsonObject.put("gracePeriod", 0);
                        jsonObject.put("dailyPenalty", 0);
                        jsonObject.put("status", 0);
                        jsonObject.put("created", "2018-10-17T19:03:50.058Z");
                        jsonObject.put("modified","2018-10-17T19:03:50.058Z");
                        /*jsonObject.put("id", 3);*/

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_LOAN_REQUEST)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_REQUEST,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(RequestLoanActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String id = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);
                            String amount = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                            String interestRate = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_INTEREST_RATE);
                            String gracePeriod = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_GRACE_PERIOD);
                            String dailyPenalty = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_DAILY_PENALTY);
                            String status = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                            String created = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                            String modified = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);

                            if(Integer.parseInt(id) > 0){

                                //NOTE the id and redirect to sign-up

                                Intent intentPassword = new Intent(RequestLoanActivity.this,YourLoansActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constant.ID,id);
                                bundle.putString(Constant.LOAN_AMOUNT,amount);
                                bundle.putString(Constant.INTEREST_RATE,interestRate);
                                bundle.putString(Constant.GRACE_PERIOD,gracePeriod);
                                bundle.putString(Constant.DAILY_PENALTY,dailyPenalty);
                                bundle.putString(Constant.STATUS,status);
                                bundle.putString(Constant.CREATED,created);
                                bundle.putString(Constant.MODIFIED,modified);
                                intentPassword.putExtras(bundle);
                                startActivity(intentPassword);
                                RequestLoanActivity.this.finish();


                            }else{

                                Toast.makeText(RequestLoanActivity.this,getString(R.string.unable_to_plc_ln),Toast.LENGTH_SHORT).show();

                            }

                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(RequestLoanActivity.this,getString(R.string.unable_to_plc_ln),Toast.LENGTH_SHORT).show();

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(RequestLoanActivity.this,getString(R.string.unable_to_plc_ln),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(RequestLoanActivity.this,getString(R.string.unable_to_plc_ln),Toast.LENGTH_SHORT).show();
                }
            }


        }
    }



    private void showDialog(){

        String message;

        if(removeCommer(textViewBalance.getText().toString()) > 0){

            message = getString(R.string.loan_request_yes);

        }else{
            message = getString(R.string.loan_request_no);

        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(RequestLoanActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.tz)+" "+textViewBalance.getText().toString());
        dialog.setMessage(message);
        dialog.setPositiveButton("Credit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                startActivity(new Intent(RequestLoanActivity.this,SavingActivity.class));
            }
        })
                .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }


    private int removeCommer(String amount){
        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }

    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }



        return amt;
    }


    @Override
    protected void onResume() {
        super.onResume();

        //register new push message receiver // by doing this, the activity will be notified each time a new message arrives

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)){
                    Snackbar snackbar = Snackbar
                            .make(relativeLayout, intent.getStringExtra(Constant.MESSAGE), Snackbar.LENGTH_INDEFINITE)
                            .setAction("View", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    String ACTIVITY = intent.getStringExtra(Constant.ACTIVITY_TYPE);

                                    Class<?> myClass = null;
                                    Activity myActivity = null;

                                    try {
                                        myClass = Class.forName(getPackageName()+"."+ACTIVITY);
                                        myActivity = (Activity) myClass.newInstance();
                                        Log.e("myActivity",myClass.toString());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    startActivity(new Intent(RequestLoanActivity.this, myClass));

                                }
                            });

                    snackbar.show();
                }

            }
        }, new IntentFilter(Constant.PUSH_NOTIFICATION));



    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        Log.e(TAG,"Unregister Broadcast");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }
}
