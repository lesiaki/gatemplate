package com.mixtape.tz.george;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.Collections;
import java.util.List;

/**
 * Created by georgejohn on 21/08/2016.
 * https://www.androidbegin.com/tutorial/android-delete-multiple-selected-items-listview-tutorial/
 */

public class AdapterContacts extends RecyclerView.Adapter<AdapterContacts.GuranterViewHolder> {

    private LayoutInflater inflater;
    Context context;

    List<Phonebook> data = Collections.emptyList();

    ExistingLoanOnItemClickListener mItemClickListener;

    public AdapterContacts(Context context, List<Phonebook> data) {

        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;

    }

    @Override
    public GuranterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.custome_row_user, parent, false);

        GuranterViewHolder hodler = new GuranterViewHolder(view);

        return hodler;
    }

    @Override
    public void onBindViewHolder(final GuranterViewHolder holder, final int position) {

                String name = data.get(position).getUsername();

                try {
                    holder.txtViewUsername.setText(name);
                    holder.textViewPhoneNumber.setText(data.get(position).getPhone_number());

                    if(Integer.parseInt(data.get(position).getStatus()) > 0){

                        holder.textViewStatus.setText(Constant.ADD);

                    }else{

                        holder.textViewStatus.setText(Constant.INVITE);
                    }



                } catch (Exception e) {
                    e.printStackTrace();
                }

    }



    @Override
    public int getItemCount() {
            return data.size();
    }

    public void CustomGuranterOnItemClickListener(ExistingLoanOnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }



    class GuranterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtViewUsername;
        TextView textViewPhoneNumber;
        TextView textViewStatus;
        ImageView imageCall;
        ImageView profileImage;
        LinearLayout linearLayout;

        public GuranterViewHolder(View itemView) {
            super(itemView);

            txtViewUsername = (TextView) itemView.findViewById(R.id.username_txtview);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_icon_dalali);
            textViewPhoneNumber = (TextView)itemView.findViewById(R.id.phone_number_txtview);
            textViewStatus = (TextView)itemView.findViewById(R.id.txt_v_status);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.line1);

        }


        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {

               // mItemClickListener.onItemClick(v, getPosition(),data);
            }
        }



    }






}


