package com.mixtape.tz.george;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.pavlospt.CircleView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

public class DashboardMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {


    CircleView circleViewSaving;
    CircleView circleViewGurantee;
    CircleView circleViewExistingLoans;
    CircleView circleWithDraw;
    Button buttonRequest;

    TextView navUsername;
    TextView navSubtitle;
    String TAG;

    BroadcastReceiver mBroadcastReceiver;
    public static boolean isAppRunning;
    RelativeLayout relativeLayout;

    //https://firebase.google.com/docs/invites/android

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        isAppRunning = true;


        try {
            TAG = getIntent().getClass().getName();
        } catch (Exception e) {
            e.printStackTrace();
        }

        circleViewSaving = (CircleView) findViewById(R.id.circle_saving);
        circleViewGurantee = (CircleView) findViewById(R.id.circle_garantee);
        circleViewExistingLoans = (CircleView) findViewById(R.id.circle_outstanding);
        circleWithDraw = (CircleView) findViewById(R.id.circle_withdraw);
        relativeLayout = (RelativeLayout)findViewById(R.id.layout1);
        buttonRequest = (Button) findViewById(R.id.button_request);


        circleViewSaving.setOnClickListener(this);
        circleViewGurantee.setOnClickListener(this);
        circleViewExistingLoans.setOnClickListener(this);
        circleWithDraw.setOnClickListener(this);
        buttonRequest.setOnClickListener(this);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        View headerView = navigationView.getHeaderView(0);
        navUsername = (TextView) headerView.findViewById(R.id.txtvusername);
        navSubtitle = (TextView) headerView.findViewById(R.id.txtvsubtitle);


        //BindData
        Cache cache = new Cache(DashboardMainActivity.this);
        navUsername.setText( getString(R.string.holder_username,CapsFL(cache.getFname()),CapsFL(cache.getLname())));
        navSubtitle.setText(cache.getPhone_number());


        //Knowing my Phone number

        TelephonyManager tMgr = (TelephonyManager) getApplication().getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String mPhoneNumber = tMgr.getLine1Number();






    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_import) {
            // Handle the camera action
            startActivity(new Intent(DashboardMainActivity.this,ImportingContactsActivity.class));
        } else if (id == R.id.nav_invite) {
            //startActivity(new Intent(DashboardMainActivity.this,TempInviteesActivity.class));
            //shareLongDynamicLink();
            shareShortDynamicLink();

        } else if (id == R.id.nav_notification) {

            startActivity(new Intent(DashboardMainActivity.this,NotificationsActivity.class));

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.circle_saving:

                startActivity(new Intent(DashboardMainActivity.this,SavingActivity.class));
                break;

            case R.id.circle_garantee:

                startActivity(new Intent(DashboardMainActivity.this,ListOfFriendsIGuaranteeActivity.class));
                break;
            case R.id.circle_outstanding:

                startActivity(new Intent(DashboardMainActivity.this,YourLoansActivity.class));
                break;

            case R.id.button_request:
                startActivity(new Intent(DashboardMainActivity.this,RequestLoanActivity.class));
                break;

            case R.id.circle_withdraw:
                startActivity(new Intent(DashboardMainActivity.this,CashoutActivity.class));
                break;

        }
    }


    private void showDialog(){

        String message = getString(R.string.alert_import);

        AlertDialog.Builder dialog = new AlertDialog.Builder(DashboardMainActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.alert_import_title));
        dialog.setMessage(message);
        dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                startActivity(new Intent(DashboardMainActivity.this,ImportingContactsActivity.class));
            }
        })
                .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       //DashboardMainActivity.this.finish();
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }


   /* @Override
    protected void onResume() {
        super.onResume();
        try {
            DatabasePhonebook db = new DatabasePhonebook(DashboardMainActivity.this);
            db.open();
            if(db.getPhoneBook().size() > 0){

               //Okay

            }else{

                showDialog();
            }
            db.close();
        }catch (Exception e) {
            Log.e(TAG,e.toString());
        }
    }*/

    String  CapsFL(String word){
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }


    public void shareLongDynamicLink() {
        Intent intent = new Intent();
        String msg = getString(R.string.invitation_amini_description)+": " + buildDynamicLink();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        intent.setType("text/plain");
        startActivity(intent);
    }


    private String buildDynamicLink() {
        //more info at https://firebase.google.com/docs/dynamic-links/create-manually
        //https://firebase.google.com/docs/invites/android


       /* *****************  WARNING WARNING WARNING  *****************

        Keystore type: JKS
        Keystore provider: SUN

        Your keystore contains 1 entry

        Alias name: amini_key
        Creation date: Aug 19, 2018
        Entry type: PrivateKeyEntry
        Certificate chain length: 1
        Certificate[1]:
        Owner: CN=Amini
        Issuer: CN=Amini
        Serial number: 19953bf9
        Valid from: Sun Aug 19 02:05:07 EAT 2018 until: Thu Aug 13 02:05:07 EAT 2043
        Certificate fingerprints:
        MD5:  5F:51:A3:51:74:9A:46:BF:B7:BB:A2:06:AC:1B:38:68
        SHA1: 2A:89:AC:AF:EF:4C:DF:96:44:C5:4B:31:2D:AC:94:8D:81:CC:DF:AC
        SHA256: 91:20:13:88:E7:1B:84:8D:A0:7E:02:40:66:4D:D8:E5:D1:48:B0:DC:A5:6A:83:4E:3A:40:F2:82:39:E4:AA:AA
        Signature algorithm name: SHA256withRSA
        Version: 3

        Extensions:

#1: ObjectId: 2.5.29.14 Criticality=false
        SubjectKeyIdentifier [
                KeyIdentifier [
                        0000: C8 73 D9 59 66 3D 97 58   3A 55 50 23 69 2B 14 DD  .s.Yf=.X:UP#i+..
        0010: 0F 15 24 7D                                        ..$.
]
]



*******************************************
********************************************/


        //https://aminiapp.page.link/?link=https://play.google.com/store/apps/
        // details?id%3Dcom.mixtape.tz.aminibank
        // &apn=com.mixtape.tz.aminibank
        // &st=Share+this+App
        // &sd=Looking+for+money+to+borrow+this+is+the+place+no+collateral

        String uid = new Cache(this).getUserId();

        String link  = getString(R.string.invitation_link)+
                "?link=" +getString(R.string.invitation_amini_link)+
                "&apn=" + getPackageName()+
                "&st=" +getString(R.string.invitation_amini_title)+
                "&utm_source=" +getString(R.string.invitation_amini_source);

        Log.e("My-Link",link);

        return link;
    }




    public void shareShortDynamicLink() {
        Task<ShortDynamicLink> createLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(buildDynamicLink()))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink(); //flowchart link is a debugging URL

                            Log.d(TAG, shortLink.toString());
                            Log.d(TAG, flowchartLink.toString());
                            Intent intent = new Intent();
                            String msg = getString(R.string.invitation_message) + shortLink.toString();
                            intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, msg);
                            intent.setType("text/plain");
                            startActivity(intent);

                        } else {
                            // Error
                            Log.e(TAG,"\nError building short link");
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //register new push message receiver // by doing this, the activity will be notified each time a new message arrives

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)){
                    Snackbar snackbar = Snackbar
                            .make(relativeLayout, intent.getStringExtra(Constant.MESSAGE), Snackbar.LENGTH_INDEFINITE)
                            .setAction("View", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    String ACTIVITY = intent.getStringExtra(Constant.ACTIVITY_TYPE);

                                    Class<?> myClass = null;
                                    Activity myActivity = null;

                                    try {
                                        myClass = Class.forName(getPackageName()+"."+ACTIVITY);
                                        myActivity = (Activity) myClass.newInstance();
                                        Log.e("myActivity",myClass.toString());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    startActivity(new Intent(DashboardMainActivity.this, myClass));

                                }
                            });

                    snackbar.show();
                }

            }
        }, new IntentFilter(Constant.PUSH_NOTIFICATION));



    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        Log.e(TAG,"Unregister Broadcast");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }
}
