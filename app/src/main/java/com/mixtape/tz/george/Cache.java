package com.mixtape.tz.george;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by george on 5/28/15.
 */
public class Cache {


    private Context context;
    private SharedPreferences sharedPreferences;
    private String id;
    private String ttl;
    private String created;
    private String userId;
    private String phone_number;
    private String firebase_token;
    private String fname;
    private String lname;
    private String otp;
    private String password;
    private String email;
    private String plain_phone_number;

    public String getPlain_phone_number() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        plain_phone_number = sharedPreferences.getString(Keys.VehicleKeys.KEY_PLN_PHN, Constant.DEFAULT_CACHE_PHONE_NUMBER);
        return plain_phone_number;
    }

    public void setPlain_phone_number(String plain_phone_number) {
        this.plain_phone_number = plain_phone_number;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_PLN_PHN, this.plain_phone_number).apply();
    }

    public String getEmail() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        email = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_EMAIL, Constant.DEFAULT_EMAIL);
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_EMAIL, this.email).apply();
    }

    public String getPassword() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        password = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_PASSWORD, Constant.DEFAULT_PASSWORD);
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_PASSWORD, this.password).apply();
    }

    public String getOtp() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        otp = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_OTP, Constant.DEFAULT_OTP);
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_OTP, this.otp).apply();
    }

    public String getFname() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        fname = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_FNAME, Constant.DEFAULT_CACHE_NAME);
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_FNAME, this.fname).apply();
    }

    public String getLname() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        lname = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_LNAME, Constant.DEFAULT_CACHE_NAME);
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_LNAME, this.lname).apply();
    }

    public Cache(Context context) {

        this.context = context;
    }


    public String getPhone_number() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        phone_number = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_PHONE_NUMBER, Constant.DEFAULT_CACHE_PHONE_NUMBER);
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_PHONE_NUMBER, this.phone_number).apply();
    }

    public String getFirebase_token() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        firebase_token = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_FIREBASE_TOKEN, Constant.DEFAULT_CACHE_FIREBASE_TOKEN);
        return firebase_token;
    }

    public void setFirebase_token(String firebase_token) {
        this.firebase_token = firebase_token;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_FIREBASE_TOKEN, this.firebase_token).apply();
    }




    public String getId() {

        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        id = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_ID, Constant.DEFAULT_CACHE_ID);
        return id;
    }

    public void setId(String id) {
        this.id = id;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_ID, this.id).apply();
    }

    public String getTtl() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        ttl = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_TTL, Constant.DEFAULT_CACHE_TTL);
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_TTL, this.ttl).apply();
    }

    public String getCreated() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);

        created = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_CREATED, Constant.DEFAULT_CACHE_CREATED);
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_CREATED, this.created).apply();
    }

    public String getUserId() {
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        userId = sharedPreferences.getString(Keys.VehicleKeys.KEY_CACHED_USERID, Constant.DEFAULT_CACHE_USERID);
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        sharedPreferences = context.getSharedPreferences(Constant.CACHED, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Keys.VehicleKeys.KEY_CACHED_USERID, this.userId).apply();
    }







}
