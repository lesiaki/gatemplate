package com.mixtape.tz.george;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class YourLoansActivity extends AppCompatActivity implements ClickListernerToGuarantee {


    List<Phonebook> contacts = Collections.emptyList();
    List<Phonebook> tempData = Collections.emptyList();
    AdapterYourLoan adapter;
    RecyclerView recyclerView;
    TextView txtVTotalBalToBePaidByG;
    TextView textViewReportingDate;
    TextView textViewInterestRate;
    static String TAG;

    LinearLayout lyFooter;
    Resources res;
    String country_code;
    int mSelectedItem;
    TextView textViewInfo;
    TextView textViewStatus;
    TextView textViewInterestAmount;
    TextView textViewNoOfInstallments;
    TextView textViewExpireDate;
    TextView textViewGuaranteeBalance;
    TextView textViewPersonalGuarantee;
    TextView textViewLoanAmount;

    LinearLayout lFooter;



    String gLoanID;
    String guarantorId;
    String status;
    String created;
    String modified;
    String gAmount;
    String gResourceID;
    ImageView imageViewInfo;
    Loans loans;


    /*
    * Varibale for firebase
    * */

    String to;
    String message;
    String gName;
    String gFirebaseToken;

    Cache cache;


    int x = 0;
    int y = 0;
    int z = 0;

    BroadcastReceiver mBroadcastReceiver;
    public static boolean isAppRunning;
    RelativeLayout relativeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_true_friends);

        isAppRunning = true;

        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);
        textViewInterestRate = (TextView)findViewById(R.id.txtv_interest_rate);
        textViewReportingDate = (TextView)findViewById(R.id.txtv_report_date);
        imageViewInfo = (ImageView)findViewById(R.id.image_view_info);
        textViewInfo = (TextView)findViewById(R.id.txt_pw);
        textViewInterestAmount = (TextView)findViewById(R.id.txtv_interest_amt);
        textViewNoOfInstallments = (TextView)findViewById(R.id.txtv_no_installments);
        textViewExpireDate = (TextView)findViewById(R.id.txtv_expire_date);
        textViewStatus = (TextView)findViewById(R.id.txtv_status);
        textViewGuaranteeBalance = (TextView)findViewById(R.id.txtv_gneeded);
        textViewLoanAmount = (TextView)findViewById(R.id.txtv_loan_amount);
        txtVTotalBalToBePaidByG = (TextView)findViewById(R.id.txtview_friendsg);
        textViewPersonalGuarantee= (TextView)findViewById(R.id.txtv_personalg);

        relativeLayout = (RelativeLayout)findViewById(R.id.layout1);


        lFooter = (LinearLayout)findViewById(R.id.footer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        cache = new Cache(YourLoansActivity.this);

        // on some click or some loading we need to wait for...

        textViewInfo.setVisibility(View.GONE);


        lyFooter = (LinearLayout)findViewById(R.id.footer);

        TAG = getIntent().getClass().getSimpleName();

        res = getResources();
        country_code = getString(R.string.tz);

        /*String gm = String.format(res.getString(R.string.tgm),putCommer(Integer.toString(y)));
        txtVTotalBalToBePaidByG.setText(gm);*/

        String total_loan = String.format(res.getString(R.string.total_loan_amt),putCommer(Integer.toString(x)));
        textViewLoanAmount.setText(total_loan);

        String gm = String.format(res.getString(R.string.amtg),putCommer(Integer.toString(y)));
        txtVTotalBalToBePaidByG.setText(gm);

        String personal_guarantee = String.format(res.getString(R.string.personal_guarantoee),putCommer(Constant.ZERO_AS_VALUE));
        textViewPersonalGuarantee.setText(personal_guarantee);

        String message_balance = String.format(res.getString(R.string.guarantee_needed),putCommer(Constant.ZERO_AS_VALUE));
        textViewGuaranteeBalance.setText(message_balance);

        String interest = String.format(res.getString(R.string.interest),Constant.ZERO_AS_VALUE);
        textViewInterestRate.setText(interest);

        String reporting_date = String.format(res.getString(R.string.reporting_date),Constant.ZERO_AS_VALUE);
        textViewReportingDate.setText(reporting_date);

        String interest_amount = String.format(res.getString(R.string.interest_amount),Constant.ZERO_AS_VALUE);
        textViewInterestAmount.setText(interest_amount);

        String no_of_installments = String.format(res.getString(R.string.no_of_installments),Constant.ZERO_AS_VALUE);
        textViewNoOfInstallments.setText(no_of_installments);

        String expire_date = String.format(res.getString(R.string.expire_date),Constant.ZERO_AS_VALUE);
        textViewExpireDate.setText(expire_date);


        final String loan_status = String.format(res.getString(R.string.loan_status),getLoanStatus(Constant.ZERO_AS_VALUE));
        textViewStatus.setText(loan_status);


        contacts = new ArrayList<>();




        ConnectionDetector connectionDetector = new ConnectionDetector(YourLoansActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetLoans(YourLoansActivity.this).execute();

        } else {
            Toast.makeText(YourLoansActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }



        imageViewInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(YourLoansActivity.this,ExistingLoanActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtras(bundle);
                startActivity(intent);*/
            }
        });

        lFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentPassword = new Intent(YourLoansActivity.this,TempInviteesActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constant.ID,loans.getId());
                bundle.putString(Constant.LOAN_AMOUNT,loans.getAmount());
                bundle.putString(Constant.INTEREST_RATE,loans.getInterestRate());
                bundle.putString(Constant.GRACE_PERIOD,loans.gracePeriod);
                bundle.putString(Constant.DAILY_PENALTY,loans.getDailyPenalty());
                bundle.putString(Constant.STATUS,loans.getStatus());
                bundle.putString(Constant.CREATED,loans.getCreated());
                bundle.putString(Constant.MODIFIED,loans.getModified());
                intentPassword.putExtras(bundle);
                startActivity(intentPassword);
                YourLoansActivity.this.finish();
            }
        });

    }


    @Override
    public void onItemClick(View view, int position, List<Phonebook> data) {

        switch (view.getId()){

            case R.id.button_request:

                mSelectedItem = position;
                tempData = data;

                gName = data.get(position).getUsername();
                gFirebaseToken = data.get(position).getFirebaseToken();
                guarantorId = data.get(position).getUser_id();
                status = data.get(position).getStatus();
                gResourceID = data.get(position).getId();

                gAmount = Integer.toString(removeCommer(data.get(position).getAmount()));

                //Check for status & resourece_id if they null this means there are registered but verified their token

                if(status == null || gResourceID == null){

                    status = Constant.ZERO;
                    gResourceID = Constant.ZERO;
                }

                if(data.get(position).getGselected() > 0 && Integer.parseInt(status)==0){

                    //Request sent but not yet accepted, so ask for a prompt when user want to cancel the request
                    open(view);

                }else if(Integer.parseInt(status)== 1){
                    //The Request if accepted, so take your to pay page when click the button

                    Intent intent = new Intent(YourLoansActivity.this,PayActivity.class);
                    Bundle bundle = new Bundle();

                    bundle.putString(Constant.GUARANTER_ID,guarantorId);
                    bundle.putString(Constant.LOAN_AMOUNT,gAmount);
                    bundle.putString(Constant.LOAN_ID,gLoanID);
                    bundle.putString(Constant.FULLNAME,gName);
                    bundle.putString(Constant.ID,gResourceID);
                    bundle.putString(Constant.PHONE_NUMBER,"+"+data.get(position).getPhone_number());

                    intent.putExtras(bundle);

                    startActivity(intent);

                } else{
                    /*if(getZ() < 1){

                        String alert_msg = String.format(res.getString(R.string.max_amt_reach));
                        Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();


                    }else */if(data.get(position).getAmount().equals("")){

                        EditText editText = (EditText)recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.input_amount);
                        editText.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                        Toast.makeText(this, getString(R.string.prompt_guarantee_amount), Toast.LENGTH_SHORT).show();

                    } else if(removeCommer(data.get(position).getAmount()) < 1){

                        Toast.makeText(this, getString(R.string.prompt_guarantee_amount), Toast.LENGTH_SHORT).show();

                    }else{



                        /*if(removeCommer(data.get(position).getAmount()) > x){

                            //Toast.makeText(this, getString(R.string.u_reach_ur_amt), Toast.LENGTH_SHORT).show();
                            String alert_msg = String.format(res.getString(R.string.max_blc_for_g), country_code, putCommer(Integer.toString(getZ())));
                            Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();

                        }
                        else if(y > x){

                            String alert_msg = String.format(res.getString(R.string.max_blc_for_g), country_code, putCommer(Integer.toString(getZ())));
                            Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();

                        }else{*/


                            if(removeCommer(data.get(position).getAmount()) > x){

                                String alert_msg = String.format(res.getString(R.string.max_per_g), country_code, putCommer(Integer.toString(getZ())));
                                Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();

                           }else{
                                ConnectionDetector connectionDetector = new ConnectionDetector(YourLoansActivity.this);
                                if (connectionDetector.isInternetConnected()) {

                                    new PostMembers(YourLoansActivity.this).execute();

                                } else {
                                    Toast.makeText(YourLoansActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                }
                            }

                        //}
                    }
                }




            break;
        }


    }

    @Override
    public void onItemLongClick(View view, int position, List<Phonebook> data) {

    }








    private int removeCommer(String amount){

        if(amount.equals(""))amount = Constant.ZERO_AS_VALUE;

        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }


    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }



        return amt;
    }





    public class GetLoans extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetLoans(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject filterJson = new JSONObject();

                    /*Cache cache = new Cache(YourLoansActivity.this);*/

                    try {
                        filterJson.put("where", new JSONObject().put("memberId",cache.getUserId()));
                        filterJson.put("order", "id DESC");
                        filterJson.put("limit", "1");
                        //filter[order] =

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }



                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_LOAN_REQUEST+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_REQUEST,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();

            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();


                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(YourLoansActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                        JSONArray jsonArray = new JSONArray(result);

                        if (jsonArray.length() > 0) {

                            //Default value for input
                            String id = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String interestRate = Constant.ERROR_100;
                            String gracePeriod = Constant.ERROR_100;
                            String dailyPenalty = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    loans = new Loans();

                                    //Check for json data if has KEY and KEY_VALUE is not Null

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                        id = object.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !object.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                        amount = object.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_INTEREST_RATE) && !object.isNull(Keys.VehicleKeys.KEY_JSON_INTEREST_RATE)) {
                                        interestRate = object.getString(Keys.VehicleKeys.KEY_JSON_INTEREST_RATE);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_GRACE_PERIOD) && !object.isNull(Keys.VehicleKeys.KEY_JSON_GRACE_PERIOD)) {
                                        gracePeriod = object.getString(Keys.VehicleKeys.KEY_JSON_GRACE_PERIOD);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_DAILY_PENALTY) && !object.isNull(Keys.VehicleKeys.KEY_JSON_DAILY_PENALTY)) {
                                        dailyPenalty = object.getString(Keys.VehicleKeys.KEY_JSON_DAILY_PENALTY);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !object.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                        status = object.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                        created = object.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                        modified = object.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                                    }


                                    gLoanID = id;

                                    loans.setId(id);
                                    loans.setAmount(amount);
                                    loans.setDailyPenalty(dailyPenalty);
                                    loans.setGracePeriod(gracePeriod);
                                    loans.setInterestRate(interestRate);
                                    loans.setStatus(status);
                                    loans.setCreated(created);
                                    loans.setModified(modified);
                                    loans.setLoanId(id);


                                    //data.add(loans);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }


                            //Update Main View




                            x = Integer.parseInt(amount);

                            /*textViewLoanAmount.setText(putCommer(Integer.toString(x)));*/

                            String total_loan = String.format(res.getString(R.string.total_loan_amt),putCommer(Integer.toString(x)));
                            textViewLoanAmount.setText(total_loan);

                            String interest = String.format(res.getString(R.string.interest),interestRate);
                            textViewInterestRate.setText(interest);

                            String reporting_date = String.format(res.getString(R.string.reporting_date),formatDate(created));
                            textViewReportingDate.setText(reporting_date);

                            String interest_amount = String.format(res.getString(R.string.interest_amount),Constant.ZERO_AS_VALUE);
                            textViewInterestAmount.setText(interest_amount);

                            String no_of_installments = String.format(res.getString(R.string.no_of_installments),Constant.ZERO_AS_VALUE);
                            textViewNoOfInstallments.setText(no_of_installments);

                            String expire_date = String.format(res.getString(R.string.expire_date),Constant.ZERO_AS_VALUE);
                            textViewExpireDate.setText(expire_date);


                            String loan_status = String.format(res.getString(R.string.loan_status),getLoanStatus(status));
                            textViewStatus.setText(loan_status);


                            textViewInfo.setVisibility(View.VISIBLE);

                            ConnectionDetector connectionDetector = new ConnectionDetector(YourLoansActivity.this);
                            if (connectionDetector.isInternetConnected()) {

                                new GetGuarantees(YourLoansActivity.this).execute();

                            } else {
                                Toast.makeText(YourLoansActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                            }



                        } else {
                            Toast.makeText(YourLoansActivity.this,getString(R.string.no_loan),Toast.LENGTH_SHORT).show();
                        }



                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }


    }


    public class GetGuarantees extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetGuarantees(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();



                    JSONObject filterJson = new JSONObject();

                    /*Cache cache = new Cache(YourLoansActivity.this);*/

                    //Remove the + sign

                    String phone_num = cache.getPhone_number().replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    try {

                        filterJson.put("phone", phone_num);


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                           // .header("Authorization", cache.getId())
                            .get()
                            /*.url(Urls.POST_MEMBERS_FRIENDS+"?filter="+filterJson.toString())*/
                            //.url(Urls.POST_MEMBERS_FRIENDS+"=/"+phone_num+"/friends")
                            .url(Urls.POST_MEMBERS_FRIENDS+"/"+phone_num+"/"+"friends?access_token="+cache.getId())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try { Log.e(TAG+":"+Urls.POST_MEMBERS_FRIENDS,result); } catch (Exception e) { Log.e(TAG,"result crush"); }
            textViewInfo.setVisibility(View.GONE);

            if(result != null){

                try {
                    Object json = new JSONTokener(result).nextValue();

                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(YourLoansActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else if(jsonObject.has(Keys.VehicleKeys.KEY_JSON_RESPONSE) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_RESPONSE)){


                            JSONArray jsonArray = jsonObject.getJSONArray(Keys.VehicleKeys.KEY_JSON_RESPONSE);

                            if (jsonArray.length() > 0) {

                                //Default value for input
                                String id = Constant.ERROR_100;

                                String created = Constant.ERROR_100;
                                String modified = Constant.ERROR_100;
                                String fname = Constant.ERROR_100;
                                String lname = Constant.ERROR_100;
                                String phone_number = Constant.ERROR_100;
                                String firebase_token = Constant.ERROR_100;

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    try {
                                        JSONObject object = jsonArray.getJSONObject(i);

                                        Phonebook phonebook = new Phonebook();

                                        //Check for json data if has KEY and KEY_VALUE is not Null

                                        if (object.has(Keys.VehicleKeys.KEY_JSON_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                            id = object.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                        }
                                        if (object.has(Keys.VehicleKeys.KEY_JSON_PHONE) && !object.isNull(Keys.VehicleKeys.KEY_JSON_PHONE)) {
                                            phone_number = object.getString(Keys.VehicleKeys.KEY_JSON_PHONE);
                                        }
                                        if (object.has(Keys.VehicleKeys.KEY_JSON_FNAME) && !object.isNull(Keys.VehicleKeys.KEY_JSON_FNAME)) {
                                            fname = object.getString(Keys.VehicleKeys.KEY_JSON_FNAME);
                                        }
                                        if (object.has(Keys.VehicleKeys.KEY_JSON_LNAME) && !object.isNull(Keys.VehicleKeys.KEY_JSON_LNAME)) {
                                            lname = object.getString(Keys.VehicleKeys.KEY_JSON_LNAME);
                                        }
                                        if (object.has(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN) && !object.isNull(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN)) {
                                            firebase_token = object.getString(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN);
                                        }

                                        if (object.has(Keys.VehicleKeys.KEY_JSON_LOAN_GUARANTEE) && !object.isNull(Keys.VehicleKeys.KEY_JSON_LOAN_GUARANTEE)) {

                                            JSONArray jsonArrayL2 = object.getJSONArray(Keys.VehicleKeys.KEY_JSON_LOAN_GUARANTEE);

                                            Log.e("ArrayL2",jsonArrayL2.toString());

                                            if (jsonArrayL2.length() > 0) {

                                                //Default value for input
                                                String loanId = Constant.ERROR_100;
                                                String guarantorId = Constant.ERROR_100;
                                                String amount = Constant.ERROR_100;
                                                String status = Constant.ERROR_100;
                                                String resource_id = Constant.ERROR_100;



                                                for (int k = 0; k < jsonArrayL2.length(); k++) {

                                                    JSONObject innerObject = jsonArrayL2.getJSONObject(k);

                                                    if (innerObject.has(Keys.VehicleKeys.KEY_JSON_LOAN_ID) && !innerObject.isNull(Keys.VehicleKeys.KEY_JSON_LOAN_ID)) {

                                                        loanId = innerObject.getString(Keys.VehicleKeys.KEY_JSON_LOAN_ID);

                                                        if (innerObject.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !innerObject.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                                            status = innerObject.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                                                        }

                                                        Log.e(TAG,fname+":"+loanId+"=="+gLoanID+" Status:"+status);

                                                        if(loanId.equals(gLoanID) && Integer.parseInt(status)!=3){//what about 2 rejected one

                                                            //Put 2 for skip the decline one but here we should put 3 so as to know that we decline our self
                                                            //to decline use our own userid is not working so i have to use the gurantee id Bad and Wierd

                                                            if (innerObject.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !innerObject.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                                                                guarantorId = innerObject.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                                                            }
                                                            if (innerObject.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !innerObject.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                                                amount = innerObject.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                                                            }
                                                            if (innerObject.has(Keys.VehicleKeys.KEY_JSON_ID) && !innerObject.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                                                resource_id = innerObject.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                                            }

                                                            phonebook.setAmount(amount);
                                                            phonebook.setGuarantorId(guarantorId);
                                                            phonebook.setLoadId(loanId);
                                                            phonebook.setStatus(status);
                                                            phonebook.setId(resource_id);
                                                            phonebook.setgSelected(1);

                                                            //calculate

                                                            //sumGAmount = sumGAmount +Integer.parseInt(amount);

                                                            y = y+Integer.parseInt(amount);

                                                        }else{

                                                            //phonebook.setAmount(Constant.ZERO_AS_VALUE);
                                                            phonebook.setGuarantorId(Constant.ZERO_AS_VALUE);
                                                            phonebook.setLoadId(Constant.ZERO_AS_VALUE);
                                                            phonebook.setStatus(Constant.ZERO_AS_VALUE);
                                                            phonebook.setId(Constant.ZERO_AS_VALUE);

                                                        }
                                                    }

                                                }

                                                //Update View


                                                String gm = String.format(res.getString(R.string.amtg),putCommer(Integer.toString(y)));
                                                txtVTotalBalToBePaidByG.setText(gm);

                                                String message_balance = String.format(res.getString(R.string.guarantee_needed), putCommer(Integer.toString(getZ())));
                                                textViewGuaranteeBalance.setText(message_balance);
                                            }



                                        }



                                        phonebook.setUser_id(id);
                                        phonebook.setUsername(CapsFL(fname)+" "+CapsFL(lname));
                                        phonebook.setPhone_number(phone_number);
                                        phonebook.setFirebaseToken(firebase_token);

                                        contacts.add(phonebook);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }


                                //Update Main View

                                adapter = new AdapterYourLoan(YourLoansActivity.this,contacts);
                                adapter.notifyDataSetChanged();
                                adapter.setCustomOnItemClickListener(YourLoansActivity.this);
                                recyclerView.setAdapter(adapter);

                                recyclerView.setLayoutManager(new LinearLayoutManager(YourLoansActivity.this));

                                recyclerView.setItemAnimator(new DefaultItemAnimator());



                            } else {

                                textViewInfo.setText(getString(R.string.no_guarantor));
                            }











                        }else{
                            Log.e(TAG, "Data is NoIdea ");
                        }

                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    textViewInfo.setText(getString(R.string.unable_to_update));
                }
            }


        }
    }

    private String getLoanStatus(String status){
        String result;
        if(status.equals(Constant.ONE_AS_VALUE)){
            result = getString(R.string.approved);
        }else{
            result = getString(R.string.pending);
        }
        return result;
    }


    private String formatDate(String dt){

        DateTimeFormatter inputFormatter = null;
        DateTimeFormatter outputFormatter = null;
        LocalDate date = null;
        String formattedDate = dt;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            date = LocalDate.parse(dt, inputFormatter);
            formattedDate = outputFormatter.format(date);

        }else{
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date2 = null;
            try {
                date2 = inputFormat.parse(dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            formattedDate = outputFormat.format(date2);

        }

        return formattedDate;
    }

    String  CapsFL(String word){
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }


    public class PostMembers extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostMembers(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.sending_req));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("loanId", gLoanID); //Not Yet
                        jsonObject.put("guarantorId", guarantorId); //Not Yet
                        jsonObject.put("amount",gAmount ); //Done
                        jsonObject.put("status", status); //Done
                        jsonObject.put("created", "2018-07-30T07:52:59.340Z");
                        jsonObject.put("modified", "2018-07-30T07:52:59.340Z");


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }



                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("Paramemter", jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_LOAN_GUARANTEES)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_GUARANTEES,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(YourLoansActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String id = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);

                            if(Integer.parseInt(id) > 0){

                                //NOTE the id and redirect to sign-up

                                y = y+Integer.parseInt(gAmount);

                                /*String gm = String.format(res.getString(R.string.tgm),putCommer(Integer.toString(y)));
                                txtVTotalBalToBePaidByG.setText(gm);*/

                                String gm = String.format(res.getString(R.string.amtg),putCommer(Integer.toString(y)));
                                txtVTotalBalToBePaidByG.setText(gm);

                                String message_balance = String.format(res.getString(R.string.guarantee_needed),putCommer(Integer.toString(getZ())));
                                textViewGuaranteeBalance.setText(message_balance);
                                tempData.get(mSelectedItem).setgSelected(1);
                                adapter.notifyDataSetChanged();

                                /*Send firebase notification to guaranter*/

                                ConnectionDetector connectionDetector = new ConnectionDetector(YourLoansActivity.this);
                                if (connectionDetector.isInternetConnected()) {

                                    new PostNotification(YourLoansActivity.this).execute();

                                } else {
                                    Toast.makeText(YourLoansActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                }



                            }else{
                                Toast.makeText(YourLoansActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();

                            }

                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(YourLoansActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(YourLoansActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(YourLoansActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();
                }
            }


        }
    }


    public class PostNotification extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostNotification(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String server_key = "key=AAAA1Ck1sJg:APA91bFxFZ1G8fRo-aw012WXmd3w1GKFHIoTJbrCqBllYpCzib1_ieWEV0fbSolSq9g5bRr8NiktPiAZgIJ7xmYBXyg7saO0XQGSpPFpbRO1biUBb9yTXbJEICrYY59Yfxqkne_ni0wt";

                    String img_url = Constant.IMG_URL_SAMPLE;

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject jsonObject = new JSONObject();

                    String fullname = CapsFL(cache.getFname())+" "+CapsFL(cache.getLname());

                    message = fullname+": Naomba unikopeshe "+getString(R.string.tz)+ putCommer(gAmount);

                    try {
                        jsonObject.put("to", gFirebaseToken);
                        jsonObject.put("data",new JSONObject()
                                .put("title","Maombi ya Mkopo")
                                .put("message",message)
                                .put("image-url",img_url)
                                .put(Constant.LOAN_AMOUNT,gAmount)
                                .put(Constant.LOAN_ID,gLoanID)
                                .put(Constant.MEMBER_ID,cache.getUserId())
                                .put(Constant.ACTIVITY_TYPE,ListOfFriendsIGuaranteeActivity.class.getSimpleName())


                        );

                    }catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());


                    Log.e("Parameter",jsonObject.toString());

                    Request request = new Request.Builder()
                            .header("Authorization", server_key).addHeader("Content-Type","application/json")
                            .post(postData)
                            .url(Urls.POST_FCM_FIREBASE)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e("Result"+":"+Urls.POST_MEMBERS,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();


        }
    }


    public void open(View view){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.prompt_cancel_request));
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                ConnectionDetector connectionDetector = new ConnectionDetector(YourLoansActivity.this);
                                if (connectionDetector.isInternetConnected()) {

                                    new CancelRequest(YourLoansActivity.this).execute();

                                } else {
                                    Toast.makeText(YourLoansActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



    public class CancelRequest extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public CancelRequest(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.sending_req));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("status", 3); //3 Mean user cancel its reqested loan

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("ParamemterX", jsonObject.toString());
                    Log.e("Rerource ID for GLoan", gResourceID);

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .patch(postData)
                            .url(Urls.POST_LOAN_GUARANTEES+"/"+gResourceID)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_GUARANTEES,result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();

            String id = Constant.ERROR_100;
            String created = Constant.ERROR_100;
            String modified = Constant.ERROR_100;
            String loan_id = Constant.ERROR_100;
            String guarantor_id = Constant.ERROR_100;
            String status = Constant.ERROR_100;
            String amount = Constant.ERROR_100;
            String memberId = Constant.ERROR_100;

            try {

                JSONObject innerJson = new JSONObject(result);

                if (innerJson.has(Keys.VehicleKeys.KEY_JSON_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                    id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);
                }
                if (innerJson.has(Keys.VehicleKeys.KEY_JSON_LOAN_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_LOAN_ID)) {
                    loan_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_LOAN_ID);
                }
                if (innerJson.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                    amount = innerJson.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                }
                if (innerJson.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                    guarantor_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                }
                if (innerJson.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                    status = innerJson.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                }
                if (innerJson.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                    created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                }
                if (innerJson.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                    modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                }

                Log.e("Deleted Status is",status);

                if(Integer.parseInt(status) == 3){

                    tempData.get(mSelectedItem).setgSelected(0);
                    tempData.get(mSelectedItem).setStatus(Constant.ZERO);
                    tempData.get(mSelectedItem).setAmount("");
                    adapter.notifyDataSetChanged();

                    int y1 = Integer.parseInt(amount);

                    y = y-y1;

                    Log.e("MyZBal",Integer.toString(getZ()));

                    String gm = String.format(res.getString(R.string.amtg),putCommer(Integer.toString(y)));
                    txtVTotalBalToBePaidByG.setText(gm);

                    try {
                        String message_balance = String.format(res.getString(R.string.guarantee_needed),putCommer(Integer.toString(getZ())));
                        textViewGuaranteeBalance.setText(message_balance);
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG,e.toString());
                Toast.makeText(YourLoansActivity.this,getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();
            }

        }
    }


    int getX(){
        return x;
    }

    int getY(){

        return y;
    }

    int getZ(){
        return x-y;
    }


    @Override
    protected void onResume() {
        super.onResume();

        //register new push message receiver // by doing this, the activity will be notified each time a new message arrives

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)){
                    Snackbar snackbar = Snackbar
                            .make(relativeLayout, intent.getStringExtra(Constant.MESSAGE), Snackbar.LENGTH_INDEFINITE)
                            .setAction("View", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    String ACTIVITY = intent.getStringExtra(Constant.ACTIVITY_TYPE);

                                    Class<?> myClass = null;
                                    Activity myActivity = null;

                                    try {
                                        myClass = Class.forName(getPackageName()+"."+ACTIVITY);
                                        myActivity = (Activity) myClass.newInstance();
                                        Log.e("myActivity",myClass.toString());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    startActivity(new Intent(YourLoansActivity.this, myClass));

                                }
                            });

                    snackbar.show();
                }

            }
        }, new IntentFilter(Constant.PUSH_NOTIFICATION));



    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        Log.e(TAG,"Unregister Broadcast");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }



}
