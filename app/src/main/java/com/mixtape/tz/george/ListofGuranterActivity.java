package com.mixtape.tz.george;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ListofGuranterActivity extends AppCompatActivity implements ExistingLoanOnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    AdapterContacts adapter;
    RecyclerView recyclerView;

    String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
    String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    String TAG;


    List<Phonebook> contacts = Collections.emptyList();
    TelephonyManager manager;
    String country_ISO;
    String country_CODE;
    LinearLayout lyFooter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listof_guranter);
        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);
        lyFooter = (LinearLayout)findViewById(R.id.footer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TAG = getIntent().getClass().getSimpleName();


        try {
            DatabasePhonebook db = new DatabasePhonebook(ListofGuranterActivity.this);
            db.open();
            Log.e(TAG+":-Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
            Log.e(TAG+":-Length",Integer.toString(db.getPhoneBook().size()));

            if(db.getPhoneBook().size() > 0){
                //contacts = db.getPhoneBook();
            }else{
                Toast.makeText(this,getString(R.string.no_contacts),Toast.LENGTH_SHORT).show();
            }


            db.close();
        }catch (Exception e) {
            Log.e(TAG,e.toString());
        }




        manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        country_ISO = manager.getNetworkCountryIso().toUpperCase();
        country_CODE = GetCountryZipCode(manager);

        // Read and show the contacts
        showContacts();


        lyFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ListofGuranterActivity.this,TempInviteesActivity.class);
                startActivity(intent);

            }
        });








    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onItemClick(View view, int position, List<Loans> data) {
        switch (view.getId()){
            case R.id.line1:
                Intent intent = new Intent(ListofGuranterActivity.this,MainActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onItemLongClick(View view, int position, List<Loans> data) {

    }


    /**
     * Show the contacts in the ListView.
     */
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {

            getSupportLoaderManager().initLoader(1, null, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }




    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {

        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(this, CONTENT_URI, null,null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {

        StringBuilder sb = new StringBuilder();
        cursor.moveToFirst();

        contacts = new ArrayList<>();

        while (!cursor.isAfterLast()) {

            Phonebook phonebook = new Phonebook();

            phonebook.setUser_id(Constant.ZERO);
            phonebook.setUsername(cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)));
            phonebook.setPhone_number(cursor.getString(cursor.getColumnIndex(NUMBER)));
            phonebook.setStatus(Constant.ONE_AS_VALUE);
            phonebook.setDate_in("date");


            if(isPhoneNumberValid(cursor.getString(cursor.getColumnIndex(NUMBER)),"+255")){
                contacts.add(phonebook);
            }

            cursor.moveToNext();
        }

        try {
            DatabasePhonebook db = new DatabasePhonebook(ListofGuranterActivity.this);
            db.open();
            db.deleteCache();
            db.saveCache(contacts);
            db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK);
            db.close();
        }catch (Exception e) {
            Log.e("TAG",e.toString());
        }


       /* ConnectionDetector connectionDetector = new ConnectionDetector(ListofGuranterActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new AddPhoneBook(ListofGuranterActivity.this).execute();

        } else {
            Toast.makeText(ListofGuranterActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }*/



        adapter = new AdapterContacts(ListofGuranterActivity.this, contacts);
        adapter.CustomGuranterOnItemClickListener(ListofGuranterActivity.this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListofGuranterActivity.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());



        /**/

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    public class AddPhoneBook extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public AddPhoneBook(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    JSONObject jsonObject = new JSONObject();



                    Cache cache = new Cache(ListofGuranterActivity.this);
                    String full_name = cache.getFname()+" "+cache.getLname();

                    //Remove the + sign

                    String phone_num = cache.getPhone_number().replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);


                    String[] mybook = listofphn(contacts);

                    Log.e("PHN_SEND_TO_GRAPH", Arrays.toString(mybook));


                    try {
                        jsonObject.put("name", full_name);
                        jsonObject.put("phone", phone_num);
                        jsonObject.put("phoneBook",mybook);
                        jsonObject.put("friendsPhones","");
                        jsonObject.put("inviteesPhone","");

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            /*.header("Authorization", basic)*/
                            .header("Authorization", cache.getId())
                            .post(postData)
                            .url(Urls.POST_MEMBERS_ADDPHNBOOK)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            /*result = "{\n" +
                    "\"response\":[\n" +
                    "\"255764026070\",\n" +
                    "\"255764026071\",\n" +
                    "\"255764026072\",\n" +
                    "\"255654759275\",\n" +
                    "\"255659533022\"\n" +
                    "]\n" +
                    "}";*/


            try {
                Log.e(TAG+":ReturnInvitees",result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_RESPONSE) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_RESPONSE)) {


                            if(jsonObject.getString(Keys.VehicleKeys.KEY_JSON_RESPONSE).equals("Member has social graph")){

                                Toast.makeText(ListofGuranterActivity.this,"Exist",Toast.LENGTH_SHORT).show();

                            }else{

                                JSONArray phnNoArray = jsonObject.getJSONArray(Keys.VehicleKeys.KEY_JSON_RESPONSE);



                                try {
                                    DatabasePhonebook db = new DatabasePhonebook(ListofGuranterActivity.this);
                                    db.open();

                                    //ArrayList<String> listPhnNo = new ArrayList<>();

                                    for (int i = 0; i < phnNoArray.length(); i++ ){

                                        //listPhnNo.add(phnNoArray.getString(i));
                                        Log.e("Check :"+Integer.toHexString(i),phnNoArray.getString(i));

                                        /*if(db.updateStatus(Constant.PLUS+phnNoArray.getString(i)) > 0){

                                            Log.e("Phn_Status_Updated 1 :",phnNoArray.getString(i));

                                        }*/
                                    }


                                    contacts = db.getPhoneBook();
                                    Log.e(TAG+":-Table Update",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
                                    Log.e(TAG+":-Length",Integer.toString(db.getPhoneBook().size()));

                                    db.close();

                                    adapter = new AdapterContacts(ListofGuranterActivity.this, contacts);
                                    adapter.CustomGuranterOnItemClickListener(ListofGuranterActivity.this);
                                    recyclerView.setAdapter(adapter);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(ListofGuranterActivity.this));
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());


                                }catch (Exception e) {
                                    Log.e(TAG,e.toString());
                                }


                                adapter.notifyDataSetChanged();
                            }




                        }else{



                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }



        }
    }

   /* public class AddPhoneBookBackUp extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public AddPhoneBook(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = null;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    JSONObject jsonObject = new JSONObject();



                    Cache cache = new Cache(ListofGuranterActivity.this);
                    String full_name = cache.getFname()+" "+cache.getLname();

                    //Remove the + sign

                    String phone_num = cache.getPhone_number().replaceAll("[\\D]", "");
                    Log.e(TAG+":- Removed Phn + signed",phone_num);

                    String[] mybook = listofphn(data);

                    Log.e("LISTOFPHN", Arrays.toString(mybook));


                    try {
                        jsonObject.put("name", full_name);
                        jsonObject.put("phone", phone_num);
                        jsonObject.put("Phonebook",Arrays.toString(mybook));
                        jsonObject.put("Friendsphone","");
                        jsonObject.put("inviteesPhone","");

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Request request = new Request.Builder()
                            *//*.header("Authorization", basic)*//*
                            .header("Authorization", cache.getId())
                            .post(postData)
                            .url(Urls.POST_MEMBERS_ADDPHNBOOK)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":Add PhoneBook",result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {



                        }else{



                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


            //For Test



            Phonebook phonebook;


            for (int i =1; i < 200; i++ ){

                phonebook = new Phonebook();

                phonebook.setUser_id(Integer.toString(i));
                phonebook.setUsername("Alexa"+Integer.toString(i));
                phonebook.setPhone_number("+255 71637105"+Integer.toString(i));
                phonebook.setStatus("Invite"+Integer.toString(i));

                contacts.add(phonebook);

            }


            try {
                DatabasePhonebook db = new DatabasePhonebook(ListofGuranterActivity.this);
                db.open();
                db.deleteCache();
                db.saveCache(contacts);
                db.close();
            }catch (Exception e) {
                Log.e("TAG",e.toString());
            }


            adapter.notifyDataSetChanged();


        }
    }*/


    public String[] list_of_phns(List<Guranter> list){
        return list.toArray(new String[list.size()]);
    }


    public String[] listofphn(List<Phonebook> contacts){

        String[] phn = new String[contacts.size()];

        for(int i = 0; i < contacts.size(); i++) {

            String single  = contacts.get(i).phone_number.replace("+","").trim();
            phn[i] =  " \""+single+"\" ";
        }

        return phn;
    }


    private boolean PhoneNumberLibValidation(String phonePara) {

        boolean  validate_okay = false;



        String phoneNumber = phonePara.replaceAll("\"^[+]\\\\d*\"", "");



        if (phoneNumber.length() > 7) {

        Log.e("NumberPassed",phoneNumber);

        // On our country, people are used to typing 7 (landline) or 11 (cellphone) digit numbers
        // To make it 7 digit numbers valid, I have to prepend “02″
        if (phoneNumber.length() == 9) {

            phoneNumber = country_CODE+ phoneNumber;
        }

        // Use the library’s functions
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phNumberProto = null;

        try {

            // I set the default region to TZ (Tanzania)
            // You can find your country code here http://www.iso.org/iso/country_names_and_code_elements
            phNumberProto = phoneUtil.parse(phoneNumber, country_ISO);

        } catch (NumberParseException e) {
            // if there’s any error
        }

        // check if the number is valid
        boolean isValid = phoneUtil.isValidNumber(phNumberProto);

        if (isValid) {

            validate_okay = true;
            // get the valid number’s international format
            //String internationalFormat = phoneUtil.format(phNumberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);

            //Toast.makeText(this, "Phone number VALID: " + internationalFormat, Toast.LENGTH_SHORT).show();

        } else {

            validate_okay = false;
            // prompt the user when the number is invalid
            Toast.makeText(getBaseContext(),getString(R.string.invalid_phonenumber), Toast.LENGTH_SHORT).show();

            }
        }

        return validate_okay;

    }


    public String GetCountryZipCode(TelephonyManager manager){
        String CountryID="";
        String CountryZipCode="";
        String CountryName = "";

        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl= getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }


    public boolean isPhoneNumberValid(String phoneNumber, String countryCode)
    {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try
        {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
            return phoneUtil.isValidNumber(numberProto);
        }
        catch (NumberParseException e)
        {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }

        return false;
    }


}
