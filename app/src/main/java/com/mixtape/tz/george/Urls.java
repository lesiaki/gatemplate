package com.mixtape.tz.george;

/**
 * Created by george on 5/22/15.
 */
public class Urls {


    //remember if ur using phone debug use  the ip address of a phone by wireless hospot dont use 127.0.0.1 or usd tether
    //http://api.amini.money:3000/Explorer/
    private static String Server = "http://api.amini.money:3000/api/";

    public static final String POST_CREDIT_WALLET1  = "http://app.lipawallet.co.tz:3000/api/Payments/customerCashIn";

    public static final String POST_MEMBERS = Server+"Members";
    public static final String POST_MEMBERS_LOGIN = Server+"Members/login";
    public static final String POST_MEMBERS_PHONE_EXISTS = Server+"Members/phone/exists";
    public static final String POST_MEMBERS_RESET_PASSWORD = Server+"Members/reset-password";
    public static final String POST_MEMBERS_SEND_OTP = Server+"Members/oneTimePasscode";
    public static final String POST_MEMBERS_RESET_PASSWORD_WITH_OTP = Server+"Members/reset-password-with-OTP";
    public static final String POST_MEMBERS_VERIFY_PHONE = Server+"Members/verifyPhone";
    public static final String POST_LOAN_REQUEST = Server+"Loans";
    public static final String POST_MEMBERS_ADDPHNBOOK = Server+"Members/socialGraph";
    public static final String POST_MEMBERS_PAYMENTS = Server+"Payments";
    public static final String POST_LOAN_REPAYMENTS_SCHEDULE = Server+"LoanPayments";
    public static final String POST_LOAN_GUARANTEES = Server+"LoanGuarantees";
    public static final String POST_MEMBERS_FRIENDS = Server+ "Members/";
    public static final String POST_MEMBERS_ACCOUNT =Server+ "Accounts/" ;
    public static final String GET_LOAN_GUARANTEES =Server+ "LoanGuarantees/" ;
    public static final String GET_ACCOUNT_BALANCE =Server+ "Accounts/" ;
    public static final String POST_LOAN_GUARANTEES_ACCEPT = Server+ "LoanGuarantees/accept/" ;
    public static final String POST_LOAN_GUARANTEES_DECLINE = Server+ "LoanGuarantees/decline" ;
    public static final String POST_MEMBERS_INVITEES = Server+ "Members/";
    public static final String GET_MEMBERS = Server+"Members/";


    public static final String POST_FCM_FIREBASE = "https://fcm.googleapis.com/fcm/send";

    public static final String POST_CREDIT_WALLET  = "https://app.lipawallet.co.tz/explorer/#!/TZMpesaUSSDPushService/TZMpesaUSSDPushService_customerCashIn";

}
