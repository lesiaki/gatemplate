package com.mixtape.tz.george;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListofFriendsActivity extends AppCompatActivity {


    List<Phonebook> contacts = Collections.emptyList();
    AdapterContacts adapter;
    RecyclerView recyclerView;
    String TAG;
    LinearLayout lyFooter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);
        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lyFooter = (LinearLayout)findViewById(R.id.footer);

        TAG = getIntent().getClass().getSimpleName();

        contacts = new ArrayList<>();


        try {
            DatabasePhonebook db = new DatabasePhonebook(ListofFriendsActivity.this);
            db.open();
            Log.e(TAG+":-Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
            Log.e(TAG+":-Length",Integer.toString(db.getFriends().size()));

            if(db.getFriends().size() > 0){
                contacts = db.getFriends();
            }else{
                Toast.makeText(this,getString(R.string.no_contacts),Toast.LENGTH_SHORT).show();
            }

            db.close();


            adapter = new AdapterContacts(this, contacts);
            //adapter.CustomGuranterOnItemClickListener(this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());


        }catch (Exception e) {
            Log.e(TAG,e.toString());
        }


        lyFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ListofFriendsActivity.this,ListofInviteesActivity.class);
                startActivity(intent);

            }
        });


    }


}
