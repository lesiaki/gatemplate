package com.mixtape.tz.george;

import android.Manifest;
import android.arch.lifecycle.LifecycleOwner;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ImportingContactsActivityTemp2 extends AppCompatActivity implements ExistingLoanOnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    AdapterContacts adapter;
    RecyclerView recyclerView;

    String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
    String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    String TAG;


    List<Phonebook> contacts = Collections.emptyList();
    TelephonyManager manager;
    String country_ISO;
    String country_CODE;
    LinearLayout lySkip;
    Button buttonInvite;
    TextView textViewResult;
    TextView textViewResult2;


    String internationalFormat;
    private String ZipCode;
    JSONArray phoneBook = null;
    //ProgressBar pb;


    int callRequest = 0;
    private static final String TAG_ANDROID_CONTACTS = "ANDROID_CONTACTS";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.importing_contacts_layout);
        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);
        lySkip = (LinearLayout)findViewById(R.id.skip);
        buttonInvite = (Button)findViewById(R.id.button_invite);
        textViewResult = (TextView)findViewById(R.id.txt_pw);
        textViewResult2 = (TextView)findViewById(R.id.info_1);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            TAG = getIntent().getClass().getSimpleName();
            //ZipCode = getIntent().getStringExtra(Constant.ZIPCODE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        country_ISO = manager.getNetworkCountryIso().toUpperCase();
        country_CODE = GetCountryZipCode(manager);

        ZipCode = this.GetCountryZipCode(manager);

        lySkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ImportingContactsActivityTemp2.this,DashboardMainActivity.class);
                startActivity(intent);
                finish();

            }
        });


        buttonInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Initilize the conctact list
                contacts = new ArrayList<>();

                if(!hasPhoneContactsPermission(Manifest.permission.READ_CONTACTS))
                {
                    requestPermission(Manifest.permission.READ_CONTACTS);
                }else {



                    List<Phonebook> tempAllContacts = new ArrayList<>();

                    tempAllContacts = getAllContacts();

                    Log.e("SizeOfPhoneBook",Integer.toString(tempAllContacts.size()));

                    for(int i=0; i < tempAllContacts.size();i++){

                        String phoneNumber = tempAllContacts.get(i).getPhone_number();
                        String usernmae = tempAllContacts.get(i).getUsername();
                        String formattedDate = getString(R.string.undefine);

                        Log.e("PassToLocalDB",usernmae+":"+phoneNumber);

                        if(IsValidationClean(phoneNumber)){

                            if(PhoneNumberLibValidation(phoneNumber)){

                                Phonebook phonebook = new Phonebook();

                                phonebook.setUser_id(Constant.ZERO);
                                phonebook.setUsername(usernmae);
                                phonebook.setPhone_number(internationalFormat.replaceAll("\\s+",""));
                                phonebook.setStatus(Constant.ZERO_AS_VALUE);
                                phonebook.setDate_in(formattedDate);

                               contacts.add(phonebook);

                            }
                        }

                    }


                    //Send data to the server


                    ConnectionDetector connectionDetector = new ConnectionDetector(ImportingContactsActivityTemp2.this);
                    if (connectionDetector.isInternetConnected()) {

                        new AddPhoneBook(ImportingContactsActivityTemp2.this).execute();

                    } else {
                        Log.e(TAG,getString(R.string.no_network));
                    }

                    //Toast.makeText(ImportingContactsActivity.this, "Contact data has been printed in the android monitor log..", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onItemClick(View view, int position, List<Loans> data) {
        switch (view.getId()){
            case R.id.line1:
                Intent intent = new Intent(ImportingContactsActivityTemp2.this,MainActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onItemLongClick(View view, int position, List<Loans> data) {

    }


    /**
     * Show the contacts in the ListView.
     */
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {

            getSupportLoaderManager().initLoader(1, null, this);
            startActivity(new Intent(ImportingContactsActivityTemp2.this,DashboardMainActivity.class));
            finish();
        }
    }


   /* @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();

            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }*/


    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {

        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Log.e("LoaderStart:",CONTENT_URI.toString());
        return new CursorLoader(this, CONTENT_URI, null,null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {


        Log.e("LoaderFinished:",loader.toString());

        String formattedDate = getString(R.string.undefine);
        contacts = new ArrayList<>();
        Log.e(TAG,"Contact Size :"+cursor.getCount());
        cursor.moveToFirst();

        try {
            DatabasePhonebook db = new DatabasePhonebook(ImportingContactsActivityTemp2.this);
            db.open();
            db.deleteCache();

        while (!cursor.isAfterLast()) {


            if(IsValidationClean(cursor.getString(cursor.getColumnIndex(NUMBER)))){

                if(this.PhoneNumberLibValidation(cursor.getString(cursor.getColumnIndex(NUMBER)))){

                    /*Phonebook phonebook = new Phonebook();

                    phonebook.setUser_id(Constant.ZERO);
                    phonebook.setUsername(cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)));
                    phonebook.setPhone_number(internationalFormat.replaceAll("\\s+",""));
                    phonebook.setStatus(Constant.ZERO_AS_VALUE);
                    phonebook.setDate_in(formattedDate);

                    contacts.add(phonebook);*/


                    String user_id = Constant.ZERO;
                    String username = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                    String phn = internationalFormat.replaceAll("\\s+","");
                    String status = Constant.ZERO_AS_VALUE;
                    String dateIn = formattedDate;

                    db.saveContact(user_id,username,phn,status,dateIn);
                    //Log.e(TAG+":-Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
                    db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK);

                    }
            }


            cursor.moveToNext();
        }


        db.close();
        }catch (Exception e) {
            Log.e("TAG",e.toString());
        }

        /*try {
            DatabasePhonebook db = new DatabasePhonebook(ImportingContactsActivity.this);
            db.open();
            db.deleteCache();
            db.saveCache(contacts);
            Log.e(TAG+":-Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
            db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK);
            db.close();
        }catch (Exception e) {
            Log.e("TAG",e.toString());
        }*/


        ConnectionDetector connectionDetector = new ConnectionDetector(ImportingContactsActivityTemp2.this);
        if (connectionDetector.isInternetConnected()) {

            new AddPhoneBook(ImportingContactsActivityTemp2.this).execute();

        } else {
            Log.e(TAG,getString(R.string.no_network));
        }


        //setupContactsRefreshPeriodicTask();

    }


    private void setupContactsRefreshPeriodicTask(){

        SharedPreferences preferences = PreferenceManager.
                getDefaultSharedPreferences(this);

        //schedule recurring task only once
        if(!preferences.getBoolean("refreshTask", false)){

            Log.e(TAG,"Start SharedPreferences");

            //start one time task using work manager
            RefreshScheduler.refreshCouponOneTimeWork((LifecycleOwner)this);
            //RefreshScheduler.refreshCouponPeriodicWork();

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("refreshTask", true);
            editor.apply();
        }else{
            Log.e(TAG,"Dont Start SharedPreferences");
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }


    public JSONArray listofphn(List<Phonebook> contacts){

        JSONArray jsonArray = new JSONArray();

        String[] phn = new String[contacts.size()];

        for(int i = 0; i < contacts.size(); i++) {

            String single  = contacts.get(i).phone_number.replace("+","").trim();
            //phn[i] =  single;

            jsonArray.put(single);
        }

        return jsonArray;
    }





    public String GetCountryZipCode(TelephonyManager manager){
        String CountryID="";
        String CountryZipCode="";
        String CountryName = "";

        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl= getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }


    private boolean IsValidationClean(String phn){
        boolean is_clear = false; //9,10,12,13
        //is_clear = phn.length() == 9 || phn.length() == 10 || phn.length() == 13;
        is_clear = phn.trim().length() > 8;
        return is_clear;
    }

    private boolean PhoneNumberLibValidation(String phn) {

        boolean  validate_okay = false;
        // get the inputted phone number
        String phoneNumber = phn;


        // On our country, people are used to typing 7 (landline) or 11 (cellphone) digit numbers
        // To make it 7 digit numbers valid, I have to prepend “02″
        if (phoneNumber.length() == 9) {

            phoneNumber = ZipCode + phoneNumber;
        }

        // Use the library’s functions
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phNumberProto = null;

        try {

            // I set the default region to TZ (Tanzania)
            // You can find your country code here http://www.iso.org/iso/country_names_and_code_elements
            phNumberProto = phoneUtil.parse(phoneNumber, country_ISO);

        } catch (NumberParseException e) {
            // if there’s any error
        }

        // check if the number is valid
        boolean isValid = false;
        try {
            isValid = phoneUtil.isValidNumber(phNumberProto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isValid) {

            validate_okay = true;
            // get the valid number’s international format
            internationalFormat = phoneUtil.format(phNumberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            Log.e(TAG,getString(R.string.valid)+":"+internationalFormat);

            //Toast.makeText(this, "Phone number VALID: " + internationalFormat, Toast.LENGTH_SHORT).show();

        } else {

            validate_okay = false;
            // prompt the user when the number is invalid
            Log.e(TAG,getString(R.string.invalid)+":"+phn);

        }

        return validate_okay;

    }



    public class AddPhoneBook extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        public AddPhoneBook(Context context) { }

        @Override
        protected void onPreExecute() { }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            callRequest++;

            Log.e(TAG,"ServerUploadCall:"+Integer.toString(callRequest));
            while (running) {

                try {

                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject jsonObject = new JSONObject();

                    Cache cache = new Cache(ImportingContactsActivityTemp2.this);
                    String full_name = cache.getFname()+" "+cache.getLname();

                    //Remove the + sign
                    String phone_num = cache.getPhone_number().replaceAll("[\\D]", "");

                    try {
                        DatabasePhonebook db = new DatabasePhonebook(ImportingContactsActivityTemp2.this);
                        db.open();
                        phoneBook = listofphn(db.getPhoneBookByStatus());
                        Log.e(TAG,"TotalPhnNo:"+Integer.toString(phoneBook.length()));
                        db.close();

                    }catch (Exception e) {
                        Log.e("TAG",e.toString());
                    }


                    try {
                        jsonObject.put("name", full_name);
                        jsonObject.put("phone",phone_num);
                        jsonObject.put("phoneBook", phoneBook);
                        jsonObject.put("friendsPhones","");
                        jsonObject.put("inviteesPhone","");

                    } catch (JSONException e) {
                        Log.e(ImportingContactsActivityTemp2.this.getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());


                    Log.e("Paramemter", jsonObject.toString());

                    Request request = new Request.Builder()
                            /*.header("Authorization", basic)*/
                            .header("Authorization", cache.getId())
                            .post(postData)
                            .url(Urls.POST_MEMBERS_ADDPHNBOOK)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                Log.e(TAG+":Graph Result",result);

            } catch (Exception e) {
                e.printStackTrace();
            }

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_RESPONSE) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_RESPONSE)) {


                            if(jsonObject.getString(Keys.VehicleKeys.KEY_JSON_RESPONSE).equals("0")){

                                Log.e(TAG,getApplicationContext().getString(R.string.al_member));

                            }else {

                                JSONArray jsonArray = jsonObject.getJSONArray(Keys.VehicleKeys.KEY_JSON_RESPONSE);


                                if (jsonArray.length() > 0) {

                                    //Initilize temp variable

                                    List<Phonebook> tempContacts = new ArrayList<>();

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject innerJsonObject = jsonArray.getJSONObject(i);
                                        Phonebook phonebook = new Phonebook();


                                        String id = Constant.ERROR_100;
                                        String phone = Constant.ERROR_100;
                                        String userId = Constant.ERROR_100;
                                        String ttl = Constant.ERROR_100;
                                        String created = Constant.ERROR_100;
                                        String otpVerified = Constant.ERROR_100;
                                        String fname = Constant.ERROR_100;
                                        String lname = Constant.ERROR_100;


                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_ID) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                            id = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                        }
                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_USER_ID) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_USER_ID)) {
                                            userId = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_USER_ID);
                                        }
                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_TTL) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_TTL)) {
                                            ttl = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_TTL);
                                        }
                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                            created = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                        }
                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED)) {
                                            otpVerified = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED);
                                        }
                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_FNAME) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_FNAME)) {
                                            fname = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_FNAME);
                                        }

                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_LNAME) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_LNAME)) {
                                            lname = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_LNAME);
                                        }
                                        if (innerJsonObject.has(Keys.VehicleKeys.KEY_JSON_PHONE) && !innerJsonObject.isNull(Keys.VehicleKeys.KEY_JSON_PHONE)) {
                                            phone = innerJsonObject.getString(Keys.VehicleKeys.KEY_JSON_PHONE);
                                        }

                                        phonebook.setUser_id(id);
                                        phonebook.setPhone_number(phone);
                                        phonebook.setUsername(fname);

                                        tempContacts.add(phonebook);

                                    }

                                    //Update Member Local db
                                    updateUserIdLocalDB(tempContacts);
                                    //Update User selected local db
                                    updateSelectedPhoneNoLocalDB(phoneBook);
                                    //Check if we have more phone number left to upload
                                    //Check if we have more phone number to upload and make a call to a server (update GraphDatabase)

                                    if (getRemainPhoneBook().length() > 0) {

                                        ConnectionDetector connectionDetector = new ConnectionDetector(ImportingContactsActivityTemp2.this);
                                        if (connectionDetector.isInternetConnected()) {

                                            new AddPhoneBook(ImportingContactsActivityTemp2.this).execute();

                                        } else {
                                            textViewResult.setText(getString(R.string.no_network));
                                        }

                                    } else {
                                        Log.e(TAG, "PhoneBook" + ": No more phone_number to upload");
                                    }

                                } else {
                                    //Result is empty array
                                    Log.e(TAG,"Result is Empty Array:"+jsonArray);
                                }
                            }
                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");
                        Log.e(TAG,getApplicationContext().getString(R.string.unable_to_in));

                    }else{

                        //Neither JsonObject Nor JsonArray
                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Log.e(TAG,getApplicationContext().getString(R.string.unable_to_in));

                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    Log.e(TAG,getApplicationContext().getString(R.string.unable_to_in));
                }
            }
    }

    private void updateSelectedPhoneNoLocalDB(JSONArray JAPhoneBook){

        Log.e("Starts Update Status:",JAPhoneBook.toString());

        try {
            DatabasePhonebook db = new DatabasePhonebook(getApplicationContext());
            db.open();

            for(int i = 0; i < JAPhoneBook.length(); i++){

                if(db.updateStatus(Constant.PLUS+JAPhoneBook.getString(i))  > 0){

                    Log.e("Ustatus_Updated:",JAPhoneBook.getString(i));
                }
            }
            Log.e(TAG+":-Table",db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
            db.close();


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void updateUserIdLocalDB(List<Phonebook> tempContacts){
        try {
            DatabasePhonebook db = new DatabasePhonebook(getApplicationContext());
            db.open();

            for (int i = 0; i < tempContacts.size(); i++) {

                if (db.updateUserID(Constant.PLUS + tempContacts.get(i).getPhone_number(), tempContacts.get(i).getUser_id()) > 0) {

                    Log.e("UserID_Updated:", tempContacts.get(i).getPhone_number());
                }
            }

            Log.e(TAG + ":-MemberUpdate:Table", db.getTableAsString(Keys.VehicleKeys.TABLE_PHONEBOOK));
            db.close();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    private JSONArray getRemainPhoneBook(){
        JSONArray InnerPhoneBook = null;

        try {
            DatabasePhonebook db = new DatabasePhonebook(ImportingContactsActivityTemp2.this);
            db.open();
            InnerPhoneBook = listofphn(db.getPhoneBookByStatus());
            db.close();

        } catch (Exception e) {
            Log.e("TAG", e.toString());
        }
        return  InnerPhoneBook;
    }


    /*
    * Retrieve Contacts
    *
    *
    * */



    /* Return all contacts and show each contact data in android monitor console as debug info. */
    private List<Phonebook> getAllContacts()
    {
        List<Phonebook> ret = new ArrayList<Phonebook>();

        // Get all raw contacts id list.
        List<Integer> rawContactsIdList = getRawContactsIdList();

        int contactListSize = rawContactsIdList.size();

        ContentResolver contentResolver = getContentResolver();

        // Loop in the raw contacts list.
        for(int i=0;i<contactListSize;i++)
        {
            // Get the raw contact id.
            Integer rawContactId = rawContactsIdList.get(i);

            //Log.d(TAG_ANDROID_CONTACTS, "raw contact id : " + rawContactId.intValue());

            // Data content uri (access data table. )
            Uri dataContentUri = ContactsContract.Data.CONTENT_URI;

            // Build query columns name array.
            List<String> queryColumnList = new ArrayList<String>();

            // ContactsContract.Data.CONTACT_ID = "contact_id";
            queryColumnList.add(ContactsContract.Data.CONTACT_ID);

            // ContactsContract.Data.MIMETYPE = "mimetype";
            queryColumnList.add(ContactsContract.Data.MIMETYPE);

            queryColumnList.add(ContactsContract.Data.DATA1);
            queryColumnList.add(ContactsContract.Data.DATA2);
            queryColumnList.add(ContactsContract.Data.DATA3);
            queryColumnList.add(ContactsContract.Data.DATA4);
            queryColumnList.add(ContactsContract.Data.DATA5);
            queryColumnList.add(ContactsContract.Data.DATA6);
            queryColumnList.add(ContactsContract.Data.DATA7);
            queryColumnList.add(ContactsContract.Data.DATA8);
            queryColumnList.add(ContactsContract.Data.DATA9);
            queryColumnList.add(ContactsContract.Data.DATA10);
            queryColumnList.add(ContactsContract.Data.DATA11);
            queryColumnList.add(ContactsContract.Data.DATA12);
            queryColumnList.add(ContactsContract.Data.DATA13);
            queryColumnList.add(ContactsContract.Data.DATA14);
            queryColumnList.add(ContactsContract.Data.DATA15);

            // Translate column name list to array.
            String queryColumnArr[] = queryColumnList.toArray(new String[queryColumnList.size()]);

            // Build query condition string. Query rows by contact id.
            StringBuffer whereClauseBuf = new StringBuffer();
            whereClauseBuf.append(ContactsContract.Data.RAW_CONTACT_ID);
            whereClauseBuf.append("=");
            whereClauseBuf.append(rawContactId);

            // Query data table and return related contact data.
            Cursor cursor = contentResolver.query(dataContentUri, queryColumnArr, whereClauseBuf.toString(), null, null);

            /* If this cursor return database table row data.
               If do not check cursor.getCount() then it will throw error
               android.database.CursorIndexOutOfBoundsException: Index 0 requested, with a size of 0.
               */
            if(cursor!=null && cursor.getCount() > 0)
            {
                StringBuffer lineBuf = new StringBuffer();
                cursor.moveToFirst();

                //lineBuf.append("Raw Contact Id : ");
                //lineBuf.append(rawContactId);

                long contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
                //lineBuf.append(" , Contact Id : ");
                //lineBuf.append(contactId);


                Phonebook phonebook = new Phonebook();

                String phoneNumber = getString(R.string.undefine);
                String displayName = getString(R.string.undefine);

                do{
                    // First get mimetype column value.
                    String mimeType = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));
                    //lineBuf.append(" \r\n , MimeType : ");
                    //lineBuf.append(mimeType);

                    if(mimeType.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)){
                        phoneNumber =   getColumnValueByMimetypeBackup(cursor, mimeType);

                    }

                    if(mimeType.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)){
                        displayName =   getColumnValueByMimetypeBackup(cursor, mimeType);

                    }


                }while(cursor.moveToNext());


                if(!phoneNumber.equals(getString(R.string.undefine))){

                    phonebook.setUsername(displayName);
                    phonebook.setPhone_number(phoneNumber);

                    ret.add(phonebook);

                }

                Log.d(TAG_ANDROID_CONTACTS, lineBuf.toString());
            }

        }

        return ret;
    }

    /*
     *  Get email type related string format value.
     * */
    private String getEmailTypeString(int dataType)
    {
        String ret = "";

        if(ContactsContract.CommonDataKinds.Email.TYPE_HOME == dataType)
        {
            ret = "Home";
        }else if(ContactsContract.CommonDataKinds.Email.TYPE_WORK==dataType)
        {
            ret = "Work";
        }
        return ret;
    }

    /*
     *  Get phone type related string format value.
     * */
    private String getPhoneTypeString(int dataType)
    {
        String ret = "";

        if(ContactsContract.CommonDataKinds.Phone.TYPE_HOME == dataType)
        {
            ret = "Home";
        }else if(ContactsContract.CommonDataKinds.Phone.TYPE_WORK==dataType)
        {
            ret = "Work";
        }else if(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE==dataType)
        {
            ret = "Mobile";
        }
        return ret;
    }

    /*
     *  Return data column value by mimetype column value.
     *  Because for each mimetype there has not only one related value,
     *  such as Organization.CONTENT_ITEM_TYPE need return company, department, title, job description etc.
     *  So the return is a list string, each string for one column value.
     * */
    private String getColumnValueByMimetypeBackup(Cursor cursor, String mimeType)
    {
        String ret = getString(R.string.undefine);


        switch (mimeType)
        {
            // Get email data.
            /*case ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE :
                // Email.ADDRESS == data1
                String emailAddress = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                // Email.TYPE == data2
                int emailType = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                String emailTypeStr = getEmailTypeString(emailType);

                ret.add("Email Address : " + emailAddress);
                ret.add("Email Int Type : " + emailType);
                ret.add("Email String Type : " + emailTypeStr);
                break;*/

            // Get im data.
            /*case ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE:
                // Im.PROTOCOL == data5
                String imProtocol = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Im.PROTOCOL));
                // Im.DATA == data1
                String imId = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA));

                ret.add("IM Protocol : " + imProtocol);
                ret.add("IM ID : " + imId);
                break;*/

            // Get nickname
            /*case ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE:
                // Nickname.NAME == data1
                String nickName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Nickname.NAME));
                ret.add("Nick name : " + nickName);
                break;*/

            // Get organization data.
            /*case ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE:
                // Organization.COMPANY == data1
                String company = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
                // Organization.DEPARTMENT == data5
                String department = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DEPARTMENT));
                // Organization.TITLE == data4
                String title = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
                // Organization.JOB_DESCRIPTION == data6
                String jobDescription = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION));
                // Organization.OFFICE_LOCATION == data9
                String officeLocation = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION));

                ret.add("Company : " + company);
                ret.add("department : " + department);
                ret.add("Title : " + title);
                ret.add("Job Description : " + jobDescription);
                ret.add("Office Location : " + officeLocation);
                break;*/

            // Get phone number.
            case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
                // Phone.NUMBER == data1
                String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                // Phone.TYPE == data2
                int phoneTypeInt = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                String phoneTypeStr = getPhoneTypeString(phoneTypeInt);

                //ret.add(Constant.PHONE_NUMBER+":"+ phoneNumber);
                //ret.add("Phone Type Integer : " + phoneTypeInt);
                //ret.add("Phone Type String : " + phoneTypeStr);

                ret = phoneNumber;


                break;

            // Get sip address.
            /*case ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE:
                // SipAddress.SIP_ADDRESS == data1
                String address = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS));
                // SipAddress.TYPE == data2
                int addressTypeInt = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.TYPE));
                String addressTypeStr = getEmailTypeString(addressTypeInt);

                ret.add("Address : " + address);
                ret.add("Address Type Integer : " + addressTypeInt);
                ret.add("Address Type String : " + addressTypeStr);
                break;*/

            // Get display name.
            case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
                // StructuredName.DISPLAY_NAME == data1
                String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
                // StructuredName.GIVEN_NAME == data2
                String givenName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                // StructuredName.FAMILY_NAME == data3
                String familyName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));

                //ret.add(Constant.DISPLAY_NAME +":"+ displayName);
                //ret.add("Given Name : " + givenName);
                //ret.add("Family Name : " + familyName);

                ret = displayName;

                break;

            // Get postal address.
            /*case ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE:
                // StructuredPostal.COUNTRY == data10
                String country = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                // StructuredPostal.CITY == data7
                String city = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                // StructuredPostal.REGION == data8
                String region = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
                // StructuredPostal.STREET == data4
                String street = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                // StructuredPostal.POSTCODE == data9
                String postcode = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                // StructuredPostal.TYPE == data2
                int postType = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));
                String postTypeStr = getEmailTypeString(postType);

                ret.add("Country : " + country);
                ret.add("City : " + city);
                ret.add("Region : " + region);
                ret.add("Street : " + street);
                ret.add("Postcode : " + postcode);
                ret.add("Post Type Integer : " + postType);
                ret.add("Post Type String : " + postTypeStr);
                break;*/

            // Get identity.
            /*case ContactsContract.CommonDataKinds.Identity.CONTENT_ITEM_TYPE:
                // Identity.IDENTITY == data1
                String identity = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Identity.IDENTITY));
                // Identity.NAMESPACE == data2
                String namespace = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Identity.NAMESPACE));

                ret.add("Identity : " + identity);
                ret.add("Identity Namespace : " + namespace);
                break;*/

            // Get photo.
            /*case ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE:
                // Photo.PHOTO == data15
                String photo = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO));
                // Photo.PHOTO_FILE_ID == data14
                String photoFileId = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_FILE_ID));

                ret.add("Photo : " + photo);
                ret.add("Photo File Id: " + photoFileId);
                break;*/

            // Get group membership.
            /*case ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE:
                // GroupMembership.GROUP_ROW_ID == data1
                int groupId = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID));
                ret.add("Group ID : " + groupId);
                break;*/

            // Get website.
            /*case ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE:
                // Website.URL == data1
                String websiteUrl = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Website.URL));
                // Website.TYPE == data2
                int websiteTypeInt = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Website.TYPE));
                String websiteTypeStr = getEmailTypeString(websiteTypeInt);

                ret.add("Website Url : " + websiteUrl);
                ret.add("Website Type Integer : " + websiteTypeInt);
                ret.add("Website Type String : " + websiteTypeStr);
                break;*/

            // Get note.
            /*case ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE:
                // Note.NOTE == data1
                String note = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                ret.add("Note : " + note);
                break;*/

        }

        return ret;
    }


    /*
     *  Return data column value by mimetype column value.
     *  Because for each mimetype there has not only one related value,
     *  such as Organization.CONTENT_ITEM_TYPE need return company, department, title, job description etc.
     *  So the return is a list string, each string for one column value.
     * */
    private ContactDTO getColumnValueByMimetype(Cursor cursor, String mimeType)
    {
        List<String> ret = new ArrayList<String>();

        ContactDTO contactDTO = new ContactDTO();


        switch (mimeType)
        {
            // Get email data.
            case ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE :
                // Email.ADDRESS == data1
                String emailAddress = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                // Email.TYPE == data2
                int emailType = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                String emailTypeStr = getEmailTypeString(emailType);

                ret.add("Email Address : " + emailAddress);
                ret.add("Email Int Type : " + emailType);
                ret.add("Email String Type : " + emailTypeStr);
                break;

            // Get im data.
            case ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE:
                // Im.PROTOCOL == data5
                String imProtocol = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Im.PROTOCOL));
                // Im.DATA == data1
                String imId = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA));

                ret.add("IM Protocol : " + imProtocol);
                ret.add("IM ID : " + imId);
                break;

            // Get nickname
            case ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE:
                // Nickname.NAME == data1
                String nickName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Nickname.NAME));
                ret.add("Nick name : " + nickName);
                break;

            // Get organization data.
            case ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE:
                // Organization.COMPANY == data1
                String company = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
                // Organization.DEPARTMENT == data5
                String department = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DEPARTMENT));
                // Organization.TITLE == data4
                String title = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
                // Organization.JOB_DESCRIPTION == data6
                String jobDescription = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION));
                // Organization.OFFICE_LOCATION == data9
                String officeLocation = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION));

                ret.add("Company : " + company);
                ret.add("department : " + department);
                ret.add("Title : " + title);
                ret.add("Job Description : " + jobDescription);
                ret.add("Office Location : " + officeLocation);
                break;

            // Get phone number.
            case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
                // Phone.NUMBER == data1
                String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                // Phone.TYPE == data2
                int phoneTypeInt = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                String phoneTypeStr = getPhoneTypeString(phoneTypeInt);

                ret.add("Phone Number : " + phoneNumber);
                ret.add("Phone Type Integer : " + phoneTypeInt);
                ret.add("Phone Type String : " + phoneTypeStr);



                break;

            // Get sip address.
            case ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE:
                // SipAddress.SIP_ADDRESS == data1
                String address = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS));
                // SipAddress.TYPE == data2
                int addressTypeInt = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.TYPE));
                String addressTypeStr = getEmailTypeString(addressTypeInt);

                ret.add("Address : " + address);
                ret.add("Address Type Integer : " + addressTypeInt);
                ret.add("Address Type String : " + addressTypeStr);
                break;

            // Get display name.
            case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
                // StructuredName.DISPLAY_NAME == data1
                String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
                // StructuredName.GIVEN_NAME == data2
                String givenName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                // StructuredName.FAMILY_NAME == data3
                String familyName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));

                ret.add("Display Name : " + displayName);
                ret.add("Given Name : " + givenName);
                ret.add("Family Name : " + familyName);


                contactDTO.setDisplayName(displayName);

                break;

            // Get postal address.
            case ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE:
                // StructuredPostal.COUNTRY == data10
                String country = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                // StructuredPostal.CITY == data7
                String city = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                // StructuredPostal.REGION == data8
                String region = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
                // StructuredPostal.STREET == data4
                String street = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                // StructuredPostal.POSTCODE == data9
                String postcode = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                // StructuredPostal.TYPE == data2
                int postType = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));
                String postTypeStr = getEmailTypeString(postType);

                ret.add("Country : " + country);
                ret.add("City : " + city);
                ret.add("Region : " + region);
                ret.add("Street : " + street);
                ret.add("Postcode : " + postcode);
                ret.add("Post Type Integer : " + postType);
                ret.add("Post Type String : " + postTypeStr);
                break;

            // Get identity.
            case ContactsContract.CommonDataKinds.Identity.CONTENT_ITEM_TYPE:
                // Identity.IDENTITY == data1
                String identity = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Identity.IDENTITY));
                // Identity.NAMESPACE == data2
                String namespace = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Identity.NAMESPACE));

                ret.add("Identity : " + identity);
                ret.add("Identity Namespace : " + namespace);
                break;

            // Get photo.
            /*case ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE:
                // Photo.PHOTO == data15
                String photo = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO));
                // Photo.PHOTO_FILE_ID == data14
                String photoFileId = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_FILE_ID));

                ret.add("Photo : " + photo);
                ret.add("Photo File Id: " + photoFileId);
                break;*/

            // Get group membership.
            case ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE:
                // GroupMembership.GROUP_ROW_ID == data1
                int groupId = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID));
                ret.add("Group ID : " + groupId);
                break;

            // Get website.
            case ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE:
                // Website.URL == data1
                String websiteUrl = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Website.URL));
                // Website.TYPE == data2
                int websiteTypeInt = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Website.TYPE));
                String websiteTypeStr = getEmailTypeString(websiteTypeInt);

                ret.add("Website Url : " + websiteUrl);
                ret.add("Website Type Integer : " + websiteTypeInt);
                ret.add("Website Type String : " + websiteTypeStr);
                break;

            // Get note.
            case ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE:
                // Note.NOTE == data1
                String note = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                ret.add("Note : " + note);
                break;

        }



        return contactDTO;
    }

    // Return all raw_contacts _id in a list.
    private List<Integer> getRawContactsIdList()
    {
        List<Integer> ret = new ArrayList<Integer>();

        ContentResolver contentResolver = getContentResolver();

        // Row contacts content uri( access raw_contacts table. ).
        Uri rawContactUri = ContactsContract.RawContacts.CONTENT_URI;
        // Return _id column in contacts raw_contacts table.
        String queryColumnArr[] = {ContactsContract.RawContacts._ID};
        // Query raw_contacts table and return raw_contacts table _id.
        Cursor cursor = contentResolver.query(rawContactUri,queryColumnArr, null, null, null);
        if(cursor!=null)
        {
            cursor.moveToFirst();
            do{
                int idColumnIndex = cursor.getColumnIndex(ContactsContract.RawContacts._ID);
                int rawContactsId = cursor.getInt(idColumnIndex);
                ret.add(new Integer(rawContactsId));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }


    // Check whether user has phone contacts manipulation permission or not.
    private boolean hasPhoneContactsPermission(String permission)
    {
        boolean ret = false;

        // If android sdk version is bigger than 23 the need to check run time permission.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // return phone read contacts permission grant status.
            int hasPermission = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
            // If permission is granted then return true.
            if (hasPermission == PackageManager.PERMISSION_GRANTED) {
                ret = true;
            }
        }else
        {
            ret = true;
        }
        return ret;
    }

    // Request a runtime permission to app user.
    private void requestPermission(String permission)
    {
        String requestPermissionArray[] = {permission};
        ActivityCompat.requestPermissions(this, requestPermissionArray, 1);
    }

    // After user select Allow or Deny button in request runtime permission dialog
    // , this method will be invoked.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int length = grantResults.length;
        if(length > 0)
        {
            int grantResult = grantResults[0];

            if(grantResult == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(getApplicationContext(), "You allowed permission, please click the button again.", Toast.LENGTH_LONG).show();
            }else
            {
                Toast.makeText(getApplicationContext(), "You denied permission.", Toast.LENGTH_LONG).show();
            }
        }
    }







}
