package com.mixtape.tz.george;

public class Loans {

    String id;
    String amount;
    String interestRate;
    String gracePeriod;
    String dailyPenalty;
    String status;
    String created;
    String modified;
    String guarantorId;
    String memberId;
    String loanId;
    int guaranteeAccepted;

    public int getGuaranteeAccepted() {
        return guaranteeAccepted;
    }

    public void setGuaranteeAccepted(int guaranteeAccepted) {
        this.guaranteeAccepted = guaranteeAccepted;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGuarantorId() {
        return guarantorId;
    }

    public void setGuarantorId(String guarantorId) {
        this.guarantorId = guarantorId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String getDailyPenalty() {
        return dailyPenalty;
    }

    public void setDailyPenalty(String dailyPenalty) {
        this.dailyPenalty = dailyPenalty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
