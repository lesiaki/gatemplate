package com.mixtape.tz.george;

public interface Constant {


    String PHONE_NUMBER = "phone_number";
    String CACHED = "cached";

    String DEFAULT_CACHE_ID = "0";
    String DEFAULT_CACHE_USERID = "0";
    String DEFAULT_CACHE_CREATED = "0";
    String DEFAULT_CACHE_TTL = "0";
    String DEFAULT_CACHE_PHONE_NUMBER = "0";
    String DEFAULT_CACHE_FIREBASE_TOKEN = "0";
    String LOAN_AMOUNT = "loan_amount";
    String ID = "id";



    String AMOUNT = "amount";
    String INTEREST_RATE = "interestRate";
    String GRACE_PERIOD = "gracePeriod";
    String DAILY_PENALTY = "dailyPenalty";
    String STATUS = "status";
    String CREATED = "created";
    String MODIFIED = "modified";
    String DEFAULT_CACHE_NAME = "name";

    String ROOT_USER = "name";
    String ROOT_PASSWORD = "name";
    String ZERO = "0";


    String ERROR_100 = "ERROR-100";//Empty value
    String PLUS = "+";
    String ONE_AS_VALUE = "1";


    String ADD = "Add";
    String INVITE = "Invite";
    String ZERO_AS_VALUE = "0";
    String ZIPCODE = "ZipCode";
    String NIL = "Nil";
    String RESPONSE = "response";
    String LIMIT = "50";
    String DISPLAY_NAME = "display_name";
    String GUARANTER_ID = "guarantor_id";
    String LOAN_ID = "loan_id";
    String DATE = "date";
    String MEMBER_ID = "member_id";
    String TOKEN = "token";
    String ADMIN_CHANNEL_ID = "xyz";
    String NOTIFICATION_TYPE = "notification_type";
    String LOAN_RESPONSE = "loan_response";
    String LOAN_REQUEST = "loan_request";
    String ACTIVITY_TYPE = "activity_type";
    String PUSH_NOTIFICATION = "pust_notification";
    String TITLE = "title";
    String MESSAGE = "message";
    String IMG_URL_SAMPLE = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png";
    String DEFAULT_OTP = "0000";
    String DEFAULT_PASSWORD = "admin";
    String DEFAULT_EMAIL = "admin@amini.money";
    String FULLNAME = "fullname";
    String ACCOUNT_BALANCE = "account_balance";
    String ACCOUNT_NUMBER = "account_number";
    String SERVER_KEY = "key=AAAA1Ck1sJg:APA91bFxFZ1G8fRo-aw012WXmd3w1GKFHIoTJbrCqBllYpCzib1_ieWEV0fbSolSq9g5bRr8NiktPiAZgIJ7xmYBXyg7saO0XQGSpPFpbRO1biUBb9yTXbJEICrYY59Yfxqkne_ni0wt";
}
