package com.mixtape.tz.george;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class TempInviteeAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    List<Phonebook> contacts = Collections.emptyList();

    public TempInviteeAdapter(Context context, List<Phonebook> contacts) {
        this.contacts = contacts;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int i) {
        return contacts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null) {
            view = inflater.inflate(R.layout.custome_row_invitee, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.username_txtview);
            viewHolder.phone = (TextView) view.findViewById(R.id.phone_number_txtview);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.name.setText(contacts.get(i).getUsername());
        viewHolder.phone.setText(contacts.get(i).getPhone_number());

        return view;
    }

    private class ViewHolder {
        TextView name;
        TextView phone;
    }
}
