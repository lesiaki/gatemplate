package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FriendsProfileActivity extends AppCompatActivity {


    TextView textViewLoanAmount;
    TextView textViewReportDate;
    TextView textViewStatus;
    TextView textViewInterestAmount;
    TextView textViewNoOfInstallments;
    TextView textViewExpireDate;
    TextView textViewInterestRate;
    TextView textViewExpectedCollection;
    TextView textViewAccountNumber;
    TextView textViewRequestedAmount;
    TextView textViewFname;
    String TAG;
    Resources res;
    Cache cache;


    String amount;
    String loan_id;
    String guaranter_id;
    String status;
    String created;
    String member_id = "0";
    String input_amount;
    String friendToken;

    String accountBalance;
    String accountNumber;
    EditText inputAmount;
    Button buttonAccept;

    int x = 0;
    int y = 0;
    int z = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_profile);

        cache = new Cache(this);

        amount  = getIntent().getStringExtra(Constant.AMOUNT);
        loan_id  = getIntent().getStringExtra(Constant.LOAN_ID);
        guaranter_id  = getIntent().getStringExtra(Constant.GUARANTER_ID);
        status  = getIntent().getStringExtra(Constant.STATUS);
        created  = getIntent().getStringExtra(Constant.CREATED);
        member_id  = getIntent().getStringExtra(Constant.MEMBER_ID);
        accountBalance  = getIntent().getStringExtra(Constant.ACCOUNT_BALANCE);
        accountNumber  = getIntent().getStringExtra(Constant.ACCOUNT_NUMBER);


        TAG = getIntent().getClass().getSimpleName();

        res = getResources();

        x = Integer.parseInt(accountBalance);

        textViewLoanAmount = (TextView)findViewById(R.id.txtv_loan_amount);
        textViewReportDate = (TextView)findViewById(R.id.reporting_date);


        textViewInterestAmount = (TextView)findViewById(R.id.txtv_interest_amt);
        textViewNoOfInstallments = (TextView)findViewById(R.id.txtv_no_installments);
        textViewExpireDate = (TextView)findViewById(R.id.txtv_expire_date);
        textViewStatus = (TextView)findViewById(R.id.txtv_status);
        inputAmount = (EditText)findViewById(R.id.input_amount);

        textViewAccountNumber = (TextView)findViewById(R.id.txtv_ac_no);
        textViewExpectedCollection = (TextView)findViewById(R.id.txtv_expected_collection);
        textViewInterestRate = (TextView)findViewById(R.id.txtv_interest_rate);
        buttonAccept = findViewById(R.id.button_accept);

        textViewRequestedAmount = (TextView)findViewById(R.id.txtv_requested_amount);

        textViewFname = (TextView)findViewById(R.id.fname);



        textViewLoanAmount.setText(putCommer(accountBalance));
        inputAmount.setText(putCommer(amount));

        String account_number = String.format(res.getString(R.string.ac_no),accountNumber);
        textViewAccountNumber.setText(account_number);

        String report_date = String.format(res.getString(R.string.report_date),formatDate(created));
        textViewReportDate.setText(report_date);



        String interest = String.format(res.getString(R.string.interest),Constant.ZERO_AS_VALUE);
        textViewInterestRate.setText(interest);

        String requested_amount = String.format(res.getString(R.string.friend_amount),res.getString(R.string.tz),putCommer(amount));
        textViewRequestedAmount.setText(requested_amount);


        String interest_amount = String.format(res.getString(R.string.interest_amount),Constant.ZERO_AS_VALUE);
        textViewInterestAmount.setText(interest_amount);

        String no_of_installments = String.format(res.getString(R.string.no_of_installments),Constant.ZERO_AS_VALUE);
        textViewNoOfInstallments.setText(no_of_installments);


        String expected_colletion = String.format(res.getString(R.string.expected_colletion),Constant.ZERO_AS_VALUE);
        textViewExpectedCollection.setText(expected_colletion);


        final String loan_status = String.format(res.getString(R.string.loan_status),getLoanStatus(status));
        textViewStatus.setText(loan_status);


        ConnectionDetector connectionDetector = new ConnectionDetector(FriendsProfileActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new getMember(FriendsProfileActivity.this).execute();

        } else {
            Toast.makeText(FriendsProfileActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }



        inputAmount.addTextChangedListener(new TextWatcher() {

            boolean isManualChange = false;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                if (isManualChange) {
                    isManualChange = false;
                    textViewLoanAmount.setText(putCommer(Integer.toString(x)));
                    return;
                }

                try {
                    String value = s.toString().replace(",", "");
                    String reverseValue = new StringBuilder(value).reverse()
                            .toString();
                    StringBuilder finalValue = new StringBuilder();
                    for (int i = 1; i <= reverseValue.length(); i++) {
                        char val = reverseValue.charAt(i - 1);
                        finalValue.append(val);
                        if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                            finalValue.append(",");
                        }
                    }
                    isManualChange = true;
                    inputAmount.setText(finalValue.reverse());
                    inputAmount.setSelection(finalValue.length());

                    //deduct

                    int bal = x;

                    if (value.length() > 0) {
                        int number = Integer.parseInt(value.toString());
                        bal =x - number;
                    }

                    textViewLoanAmount.setText(putCommer(Integer.toString(bal)));


                } catch (Exception e) {
                    // Do nothing since not a number
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub



            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                /*int number = Integer.parseInt(s.toString());
                x += number;
                textViewBalance.setText(Integer.toString(x));*/

            }
        });

        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                input_amount = inputAmount.getText().toString();

                if(!input_amount.isEmpty()){

                    if( removeCommer(input_amount) <= removeCommer(amount)){
                        ConnectionDetector connectionDetector = new ConnectionDetector(FriendsProfileActivity.this);
                        if (connectionDetector.isInternetConnected()) {

                            new PostAccept(FriendsProfileActivity.this).execute();

                        } else {
                            Toast.makeText(FriendsProfileActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(FriendsProfileActivity.this, R.string.prompt_amount_accept, Toast.LENGTH_SHORT).show();

                    }


                }else{

                    Toast.makeText(FriendsProfileActivity.this, R.string.enter_amount_guarantee, Toast.LENGTH_SHORT).show();

                }




            }
        });


    }

    private String getLoanStatus(String status){
        String result;
        if(status.equals(Constant.ONE_AS_VALUE)){
            result = getString(R.string.approved);
        }else{
            result = getString(R.string.pending);
        }
        return result;
    }

    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }



        return amt;
    }

    private String formatDate(String dt){

        DateTimeFormatter inputFormatter = null;
        DateTimeFormatter outputFormatter = null;
        LocalDate date = null;
        String formattedDate = dt;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            date = LocalDate.parse(dt, inputFormatter);
            formattedDate = outputFormatter.format(date);

        }else{
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date2 = null;
            try {
                date2 = inputFormat.parse(dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            formattedDate = outputFormat.format(date2);

        }

        return formattedDate;
    }


    public class getMember extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public getMember(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    JSONObject jsonObject = new JSONObject();

                    try {

                        jsonObject.put("id", member_id);

                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    String token = new Cache(FriendsProfileActivity.this).getId();

                    Log.e("Token",token);

                    Log.e("member_id",member_id);

                    Request request = new Request.Builder()
                            .header("Authorization", token)
                            .get()
                            .url(Urls.GET_MEMBERS+member_id)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.GET_MEMBERS,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(FriendsProfileActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String id = Constant.ERROR_100;
                            String userId = Constant.ERROR_100;
                            String ttl = Constant.ERROR_100;
                            String created = Constant.ERROR_100;
                            String otpVerified = Constant.ERROR_100;
                            String fname = Constant.ERROR_100;
                            String lname = Constant.ERROR_100;
                            friendToken = Constant.ERROR_100;


                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ID) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                id = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_USER_ID) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_USER_ID)) {
                                userId = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_USER_ID);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_TTL) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_TTL)) {
                                ttl = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_TTL);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                created = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED)) {
                                otpVerified = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_OTP_VERIFIED);
                            }
                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_FNAME) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_FNAME)) {
                                fname = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_FNAME);
                            }

                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_LNAME) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_LNAME)) {
                                lname = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_LNAME);
                            }

                            if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN)) {
                                friendToken = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_FIREBASE_TOKEN);
                            }




                            String fullname = CapsFL(fname)+" "+CapsFL(lname);

                            textViewFname.setText(fullname);

                        }


                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                }
            }


        }
    }

    String  CapsFL(String word){
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }


    public class PostAccept extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostAccept(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.sending_req));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = null;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();

                    //{"loanId":23, "guarantorId":6, "amount":50000 }

                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("loanId", loan_id); //Not Yet
                        jsonObject.put("guarantorId", guaranter_id); //Not Yet
                        jsonObject.put("amount",input_amount ); //Done


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("Paramemter", jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_LOAN_GUARANTEES_ACCEPT)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_GUARANTEES_ACCEPT,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if(result != null){

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(FriendsProfileActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                            String id = Constant.ERROR_100;

                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;
                            String loan_id = Constant.ERROR_100;
                            String guarantor_id = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String memberId = Constant.ERROR_100;

                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_RESULT);

                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_LOAN_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_LOAN_ID)) {
                                loan_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_LOAN_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                amount = innerJson.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                                guarantor_id = innerJson.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                status = innerJson.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                created = innerJson.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                            }
                            if (innerJson.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !innerJson.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                modified = innerJson.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                            }


                            if(Integer.parseInt(status) > 0){



                                //update view

                                /*Send firebase notification to guaranter*/

                                ConnectionDetector connectionDetector = new ConnectionDetector(FriendsProfileActivity.this);
                                if (connectionDetector.isInternetConnected()) {

                                    new PostNotification(FriendsProfileActivity.this).execute();

                                } else {
                                    Toast.makeText(FriendsProfileActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                                }

                            }



                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(FriendsProfileActivity.this,getString(R.string.unable_to_accept),Toast.LENGTH_SHORT).show();

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(FriendsProfileActivity.this,getString(R.string.unable_to_accept),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(FriendsProfileActivity.this,getString(R.string.unable_to_accept),Toast.LENGTH_SHORT).show();
                }
            }


        }
    }


    public class PostNotification extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostNotification(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result = null;
            while (running) {

                try {


                    String server_key = Constant.SERVER_KEY;

                    String img_url = Constant.IMG_URL_SAMPLE;

                    OkHttpClient okHttpClient = new OkHttpClient();

                    JSONObject jsonObject = new JSONObject();

                    String fullname = CapsFL(cache.getFname())+" "+CapsFL(cache.getLname());

                    String message = fullname+": Nimepitisha kiasi cha  "+getString(R.string.tz)+ putCommer(input_amount);

                    try {
                        jsonObject.put("to", friendToken);
                        jsonObject.put("data",new JSONObject()
                                .put("title","Mkopo Umepitishwa")
                                .put("message",message)
                                .put("image-url",img_url)
                                .put(Constant.LOAN_AMOUNT,input_amount)
                                .put(Constant.LOAN_ID,loan_id)
                                .put(Constant.MEMBER_ID,cache.getUserId())
                                .put(Constant.ACTIVITY_TYPE,YourLoansActivity.class.getSimpleName())


                        );

                    }catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());


                    Log.e("Parameter",jsonObject.toString());

                    Request request = new Request.Builder()
                            .header("Authorization", server_key).addHeader("Content-Type","application/json")
                            .post(postData)
                            .url(Urls.POST_FCM_FIREBASE)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e("Result"+":"+Urls.POST_MEMBERS,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();


        }
    }

    private int removeCommer(String amount){
        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }

}
