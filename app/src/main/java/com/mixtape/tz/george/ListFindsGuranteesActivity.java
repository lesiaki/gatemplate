package com.mixtape.tz.george;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ListFindsGuranteesActivity extends AppCompatActivity implements ClickListernerToGuarantee {


    List<Phonebook> contacts = Collections.emptyList();
    List<Phonebook> tempData = Collections.emptyList();
    AdapterYourLoan adapter;
    RecyclerView recyclerView;
    TextView textViewBalance;
    TextView textViewLoanAmount;
    TextView textViewGuaranteeBalance;
    String TAG;
    String global_loan_amount;
    LinearLayout lyFooter;
    Resources res;
    String country_code;
    int mSelectedItem;
    TextView textViewInfo;



    String loanID;
    String guarantorId;
    String status;
    String created;
    String modified;
    String gAmount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finds_gutanters);
        recyclerView = (RecyclerView)findViewById(R.id.recyler_view);
        textViewBalance = (TextView)findViewById(R.id.txtview_balance);
        textViewGuaranteeBalance = (TextView)findViewById(R.id.txtv_gbalance);
        textViewLoanAmount = (TextView)findViewById(R.id.txtv_loan_amount);
        textViewInfo = (TextView)findViewById(R.id.txt_pw);
        textViewInfo.setVisibility(View.VISIBLE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            global_loan_amount = getIntent().getStringExtra(Constant.LOAN_AMOUNT);
            loanID = getIntent().getStringExtra(Constant.ID);

            Log.e("amount pass",global_loan_amount);
            Log.e("loanID pass",loanID);

        } catch (Exception e) {
            e.printStackTrace();
            global_loan_amount = getString(R.string.default_balance);
        }

        lyFooter = (LinearLayout)findViewById(R.id.footer);

        TAG = getIntent().getClass().getSimpleName();

        res = getResources();
        country_code = getString(R.string.tz);


        String message_ln_amt = String.format(res.getString(R.string.info_ln_amt), country_code, putCommer(global_loan_amount));
        textViewLoanAmount.setText(message_ln_amt);

        String message_balance = String.format(res.getString(R.string.info_balance), country_code,putCommer(global_loan_amount));
        textViewGuaranteeBalance.setText(message_balance);


        contacts = new ArrayList<>();



        ConnectionDetector connectionDetector = new ConnectionDetector(ListFindsGuranteesActivity.this);
        if (connectionDetector.isInternetConnected()) {

            new GetGuarantees(ListFindsGuranteesActivity.this).execute();

        } else {
            Toast.makeText(ListFindsGuranteesActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onItemClick(View view, int position, List<Phonebook> data) {

        switch (view.getId()){

            case R.id.button_request:

                if(removeCommer(calculateGAmount(textViewBalance.getText().toString())) < 1){

                    String alert_msg = String.format(res.getString(R.string.max_amt_reach));
                    Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();


                }else if(data.get(position).getAmount().equals("")){



                    EditText editText = (EditText)recyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.input_amount);
                    editText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

                    Toast.makeText(this, getString(R.string.prompt_guarantee_amount), Toast.LENGTH_SHORT).show();

                } else if(removeCommer(data.get(position).getAmount()) < 1){

                    Toast.makeText(this, getString(R.string.prompt_guarantee_amount), Toast.LENGTH_SHORT).show();

                }else{


                    if(removeCommer(data.get(position).getAmount()) > removeCommer(global_loan_amount)){

                        //Toast.makeText(this, getString(R.string.u_reach_ur_amt), Toast.LENGTH_SHORT).show();
                        String alert_msg = String.format(res.getString(R.string.max_blc_for_g), country_code, putCommer(calculateGAmount(textViewBalance.getText().toString())));
                        Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();

                    }
                    else if(removeCommer(getAmountRequested(data.get(position).getAmount())) > removeCommer(global_loan_amount)){

                        String alert_msg = String.format(res.getString(R.string.max_blc_for_g), country_code, putCommer(calculateGAmount(textViewBalance.getText().toString())));
                        Toast.makeText(this, alert_msg, Toast.LENGTH_SHORT).show();

                    }else{

                        mSelectedItem = position;
                        tempData = data;
                        gAmount = Integer.toString(removeCommer(data.get(position).getAmount()));
                        guarantorId = data.get(position).getUser_id();
                        status = data.get(position).getStatus();

                            ConnectionDetector connectionDetector = new ConnectionDetector(ListFindsGuranteesActivity.this);
                            if (connectionDetector.isInternetConnected()) {

                                new PostMembers(ListFindsGuranteesActivity.this).execute();

                            } else {
                                Toast.makeText(ListFindsGuranteesActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
                            }



                    }





                }


            break;
        }


    }

    @Override
    public void onItemLongClick(View view, int position, List<Phonebook> data) {

    }


    private String getAmountRequested(String amount){

        int initial_amount = 0;
        int final_amount = 0;
        int sum;

        String defult_amount = textViewBalance.getText().toString();

        if(!defult_amount.equals(getString(R.string.default_balance))){

             initial_amount = Integer.parseInt(defult_amount.replace(",","").trim());
             final_amount = Integer.parseInt(amount.replace(",","").trim());
            sum = initial_amount + final_amount;

        }else{

            sum = Integer.parseInt(amount.replace(",","").trim());
        }


        Log.e(TAG,"CalAmt:"+sum);
        return Integer.toString(sum);

    }


    private String calculateGAmount(String amount){

        int initial_amount = 0; //
        int final_amount = 0;
        int sum;

            initial_amount = Integer.parseInt(global_loan_amount.replace(",","").trim());
            final_amount = Integer.parseInt(amount.replace(",","").trim());
            sum = initial_amount - final_amount;

        return Integer.toString(sum);

    }


    private int removeCommer(String amount){
        String num = amount.replaceAll("[\\.$|,|;|']", "").trim();
        return Integer.parseInt(num);
    }


    private String putCommer(String amount){

        String amt = getString(R.string.default_balance);

        try {
            String value = amount.toString().replace(",", "");
            String reverseValue = new StringBuilder(value).reverse()
                    .toString();
            StringBuilder finalValue = new StringBuilder();
            for (int i = 1; i <= reverseValue.length(); i++) {
                char val = reverseValue.charAt(i - 1);
                finalValue.append(val);
                if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                    finalValue.append(",");
                }
            }

            amt = finalValue.reverse().toString();

        } catch (Exception e) {
            // Do nothing since not a number
        }



        return amt;
    }


    public class PostMembers extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public PostMembers(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.sending_req));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("Urls.POST_MEMBERS",Urls.POST_MEMBERS);


                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("loanId", loanID); //Not Yet
                        jsonObject.put("guarantorId", guarantorId); //Not Yet
                        jsonObject.put("amount",gAmount ); //Done
                        jsonObject.put("status", status); //Done
                        jsonObject.put("created", "2018-07-30T07:52:59.340Z");
                        jsonObject.put("modified", "2018-07-30T07:52:59.340Z");


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }

                    Log.e("gAmount parm",gAmount);
                    Log.e("guarantorId parm",guarantorId);

                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, jsonObject.toString());

                    Log.e("Paramemter", jsonObject.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .post(postData)
                            .url(Urls.POST_LOAN_GUARANTEES)
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_MEMBERS,result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();

                try {

                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject){

                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(ListFindsGuranteesActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{

                            String id = jsonObject.getString(Keys.VehicleKeys.KEY_JSON_ID);

                            if(Integer.parseInt(id) > 0){

                                //NOTE the id and redirect to sign-up



                                textViewBalance.setText(putCommer(getAmountRequested(tempData.get(mSelectedItem).getAmount())));
                                String message_balance = String.format(res.getString(R.string.info_balance), country_code, putCommer(calculateGAmount(textViewBalance.getText().toString())));
                                textViewGuaranteeBalance.setText(message_balance);
                                tempData.get(mSelectedItem).setgSelected(1);
                                adapter.notifyDataSetChanged();

                                //Toast.makeText(ListToGuranteesActivity.this,getString(R.string.sent),Toast.LENGTH_SHORT).show();


                            }else{
                                Toast.makeText(ListFindsGuranteesActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();

                            }

                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");
                        Toast.makeText(ListFindsGuranteesActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();

                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        Toast.makeText(ListFindsGuranteesActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    Toast.makeText(ListFindsGuranteesActivity.this,getString(R.string.unable_to_snd),Toast.LENGTH_SHORT).show();
                }



        }
    }

    private void showDialog(){

        String message = getString(R.string.alert_invite);

        AlertDialog.Builder dialog = new AlertDialog.Builder(ListFindsGuranteesActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.alert_invite_title));
        dialog.setMessage(message);
        dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                startActivity(new Intent(ListFindsGuranteesActivity.this,TempInviteesActivity.class));
                ListFindsGuranteesActivity.this.finish();
            }
        })
                .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ListToGuranteesActivity.this.finish();
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public class GetGuarantees extends AsyncTask<Void, Void, String> {

        private boolean running = true;
        private final ProgressDialog progressDialog;

        public GetGuarantees(Context context) {
            progressDialog = new ProgressDialog(context);

            progressDialog.setCancelable(true);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // actually could set running = false; right here, but I'll
                    // stick to contract.
                    cancel(true);
                }
            });

        }

        @Override
        protected void onPreExecute() {
            //progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        @Override
        protected String doInBackground(Void... string) {

            String result =  Constant.NIL;
            while (running) {

                try {


                    String root_username = "RootAdmin";
                    String root_password = "RootAdmin";
                    String credentials = root_username + ":" + root_password;

                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    String password_sha1 = new AeSimpleSHA1().getSHA1("sample");

                    OkHttpClient okHttpClient = new OkHttpClient();


                    Log.d("LOAN_REQUEST_URL",Urls.POST_LOAN_REQUEST);

                    JSONObject filterJson = new JSONObject();

                    Cache cache = new Cache(ListFindsGuranteesActivity.this);

                    //filter={"where": {"and":[{"memberId":"3"},{"status":"1"}]},"order":"created DESC", "limit":1}

                    try {
                        //filterJson.put("where", new JSONObject().put("loanId",loanID));
                        filterJson.put("where", new JSONObject().put("and",new JSONArray().put(new JSONObject().put("loanId",loanID)).put(new JSONObject().put("status",1))));
                        filterJson.put("order", "id DESC");
                        //filterJson.put("limit", "1");


                    } catch (JSONException e) {
                        Log.e(getString(R.string.invalid_json),e.toString());
                    }



                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

                    RequestBody postData = RequestBody.create(JSON, filterJson.toString());

                    Log.e("Parameter",filterJson.toString());

                    Request request = new Request.Builder()
                            //.header("Authorization", basic)
                            .get()
                            .url(Urls.POST_LOAN_GUARANTEES+"?filter="+filterJson.toString())
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                    running = false;

                } catch (Exception e) {running = false;}
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                Log.e(TAG+":"+Urls.POST_LOAN_GUARANTEES,result);

            } catch (Exception e) {
                e.printStackTrace();
            }



                try {

                    Object json = new JSONTokener(result).nextValue();


                    if (json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has(Keys.VehicleKeys.KEY_JSON_ERROR) && !jsonObject.isNull(Keys.VehicleKeys.KEY_JSON_ERROR)) {


                            JSONObject innerJson = jsonObject.getJSONObject(Keys.VehicleKeys.KEY_JSON_ERROR);

                            String statusCode = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STATUS_CODE);
                            String name = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_NAME);
                            String message = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_MESSAGE);
                            String code = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_CODE);
                            String stack = innerJson.getString(Keys.VehicleKeys.KEY_JSON_ERROR_STACK);

                            Toast.makeText(ListFindsGuranteesActivity.this,message,Toast.LENGTH_SHORT).show();


                        }else{


                        }

                    } else if (json instanceof JSONArray){

                        Log.e(TAG, "Data is J-AR ");

                        JSONArray jsonArray = new JSONArray(result);

                        if (jsonArray.length() > 0) {

                            textViewInfo.setVisibility(View.GONE);

                            //Default value for input
                            String id = Constant.ERROR_100;
                            String amount = Constant.ERROR_100;
                            String status = Constant.ERROR_100;
                            String created = Constant.ERROR_100;
                            String modified = Constant.ERROR_100;
                            String guarantorId = Constant.ERROR_100;

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    Phonebook phonebook = new Phonebook();

                                    //Check for json data if has KEY and KEY_VALUE is not Null

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_ID)) {
                                        id = object.getString(Keys.VehicleKeys.KEY_JSON_ID);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_AMOUNT) && !object.isNull(Keys.VehicleKeys.KEY_JSON_AMOUNT)) {
                                        amount = object.getString(Keys.VehicleKeys.KEY_JSON_AMOUNT);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID) && !object.isNull(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID)) {
                                        guarantorId = object.getString(Keys.VehicleKeys.KEY_JSON_GUARANTER_ID);
                                    }

                                    if (object.has(Keys.VehicleKeys.KEY_JSON_STATUS) && !object.isNull(Keys.VehicleKeys.KEY_JSON_STATUS)) {
                                        status = object.getString(Keys.VehicleKeys.KEY_JSON_STATUS);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_CREATED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_CREATED)) {
                                        created = object.getString(Keys.VehicleKeys.KEY_JSON_CREATED);
                                    }
                                    if (object.has(Keys.VehicleKeys.KEY_JSON_MODIFIED) && !object.isNull(Keys.VehicleKeys.KEY_JSON_MODIFIED)) {
                                        modified = object.getString(Keys.VehicleKeys.KEY_JSON_MODIFIED);
                                    }

                                    Log.e("Amount",amount);

                                    phonebook.setUser_id(id);
                                    phonebook.setAmount(amount);
                                    phonebook.setStatus(status);


                                    contacts.add(phonebook);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }


                            //Update Main View

                            adapter = new AdapterYourLoan(ListFindsGuranteesActivity.this,contacts);
                            adapter.notifyDataSetChanged();

                            //adapter.setOnLoanItemClickListener(ExistingLoanActivity.this);

                            recyclerView.setAdapter(adapter);

                            recyclerView.setLayoutManager(new LinearLayoutManager(ListFindsGuranteesActivity.this));

                            recyclerView.setItemAnimator(new DefaultItemAnimator());



                        } else {

                            textViewInfo.setText(getString(R.string.no_guarantor));
                        }



                    }else{

                        //Neither JsonObject Nor JsonArray

                        Log.e(TAG, "Data is Neither J-OB Nor J-AR");
                        textViewInfo.setText(getString(R.string.unable_to_update));


                    }

                } catch (Exception e) {

                    Log.e(TAG, "Json Fail "+e.toString());
                    textViewInfo.setText(getString(R.string.unable_to_update));
                }



        }
    }

}
