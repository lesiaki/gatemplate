package com.mixtape.tz.george;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import static android.support.constraint.Constraints.TAG;

//https://www.pluralsight.com/guides/push-notifications-with-firebase-cloud-messaging
//https://medium.com/android-school/firebaseinstanceidservice-is-deprecated-50651f17a148

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    NotificationManager notificationManager;
    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String IMAGE_URL_EXTRA = "imageUrl";

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("NEW_TOKEN",token);

        new Cache(this).setFirebase_token(token);


    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //scheduleJob();
            } else {
                // Handle message within 10 seconds
                //handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

        }

        NotificationTray(remoteMessage);

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.




    }



    /*
    * For Test through Postman in browser
    * https://fcm.googleapis.com/fcm/send
    *
    *
    * {
        "to":
        "eRyAkjf8vYA:APA91bH5X_3sPrlOvF3rfbqbk-NaemrFg3rTy-W2lBvQ3o09qb5uqXpc7kxeY-PmzIOyfzl1rDMIQ2IDZptH7FUiWXodXgq9OBM-jkPxHdjNC0db3xWaU3AqCEaD-H9ppYuAOJ6lnklK",
                "data": {
        "title": "I'd tell you a chemistry joke",
                "message": "but I know I wouldn't get a reaction",
                "image-url":
        "https://docs.centroida.co/wp-content/uploads/2017/05/notification.png"
        }
    }
    *
    * */






    private void NotificationTray(RemoteMessage remoteMessage){


        if(DashboardMainActivity.isAppRunning || CashoutActivity.isAppRunning
                ||SavingActivity.isAppRunning || YourLoansActivity.isAppRunning || ListOfFriendsIGuaranteeActivity.isAppRunning){
            //Some action

            Log.e(TAG,"The App is running....");

            Intent pushNotification = new Intent(Constant.PUSH_NOTIFICATION);
            //pushNotification.setAction(DashboardMainActivity.class.getName());
            pushNotification.putExtra(Constant.TITLE, remoteMessage.getData().get(Constant.TITLE));
            pushNotification.putExtra(Constant.MESSAGE, remoteMessage.getData().get(Constant.MESSAGE));
            pushNotification.putExtra(Constant.LOAN_AMOUNT, remoteMessage.getData().get(Constant.LOAN_AMOUNT));
            pushNotification.putExtra(Constant.MEMBER_ID, remoteMessage.getData().get(Constant.MEMBER_ID));
            pushNotification.putExtra(Constant.ACTIVITY_TYPE, remoteMessage.getData().get(Constant.ACTIVITY_TYPE));

            LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(pushNotification);


        }else{
            showNotification(remoteMessage);
        }


    }


    private void showNotification(RemoteMessage remoteMessage){

        Log.e("loan",remoteMessage.getData().get(Constant.LOAN_AMOUNT));
        Log.e("member_id",remoteMessage.getData().get(Constant.MEMBER_ID));
        Log.e("activity_type",remoteMessage.getData().get(Constant.ACTIVITY_TYPE).toString());

        String ACTIVITY = remoteMessage.getData().get(Constant.ACTIVITY_TYPE);

        Class<?> myClass = null;
        Activity myActivity = null;

        try {
            myClass = Class.forName(getPackageName()+"."+ACTIVITY);
            myActivity = (Activity) myClass.newInstance();
            Log.e("myActivity",myClass.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent notificationIntent = new Intent(this, myClass);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT);

        //You should use an actual ID instead
        int notificationId = new Random().nextInt(60000);


        Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("image-url"));

        Intent likeIntent = new Intent(this,LikeService.class);
        likeIntent.putExtra(NOTIFICATION_ID_EXTRA,notificationId);
        likeIntent.putExtra(IMAGE_URL_EXTRA,remoteMessage.getData().get("image-url"));
        PendingIntent likePendingIntent = PendingIntent.getService(this,
                notificationId+1,likeIntent,PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = null;
        try {
            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), defaultSoundUri);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, Constant.ADMIN_CHANNEL_ID)
                        .setLargeIcon(bitmap)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(remoteMessage.getData().get("title"))
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .setSummaryText(remoteMessage.getData().get("message"))
                                .bigPicture(bitmap))/*Notification with Image*/
                        .setContentText(remoteMessage.getData().get("message"))
                        .setAutoCancel(true)
                        //.setSound(defaultSoundUri)
                        .addAction(R.drawable.ic_navigate_next_black_24dp,
                                getString(R.string.notification_next),likePendingIntent)
                        .setContentIntent(pendingIntent);

        notificationManager.notify(notificationId, notificationBuilder.build());

    }


    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(){
        CharSequence adminChannelName = getString(R.string.notifications_loan_request_channel);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(Constant.ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }



}
