package com.mixtape.tz.george;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by georgejohn on 14/11/2017.
 */

public class DatabaseHelperPhonebook extends SQLiteOpenHelper {

    private static final String SQL_CREATE_DB = "CREATE TABLE "
            + Keys.VehicleKeys.TABLE_PHONEBOOK
            + "("
            + Keys.VehicleKeys.KEY_DB_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Keys.VehicleKeys.KEY_JSON_USER_ID+ " INTEGER NOT NULL ,"
            + Keys.VehicleKeys.KEY_JSON_USERNAME + "	VARCHAR(100) NOT NULL ,"
            + Keys.VehicleKeys.KEY_JSON_PHONE_NUMBER + "	VARCHAR(100) NOT NULL ,"
            + Keys.VehicleKeys.KEY_JSON_DATE + "	VARCHAR(100) NOT NULL ,"
            + Keys.VehicleKeys.KEY_JSON_STATUS + "	VARCHAR(100) NOT NULL "
            + ")";




    // A general SQL command for deleting a table
    private static final String SQL_DROP = "DROP TABLE IF EXISTS ";

    public DatabaseHelperPhonebook(Context context) {

        super(context, Keys.VehicleKeys.DB_NAME_PHNBK, null, Keys.VehicleKeys.DB_VERSION);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Simply replaces the old version of the database tables with the new one
        db.execSQL(SQL_DROP + Keys.VehicleKeys.TABLE_PHONEBOOK);
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_DB);

    }

}
