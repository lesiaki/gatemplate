package com.mixtape.tz.george;

import android.view.View;

import java.util.List;

/**
 * Created by georgejohn on 12/11/2017.
 */

public interface ExistingLoanOnItemClickListener {

    void onItemClick(View view, int position, List<Loans> data);
    void onItemLongClick(View view, int position, List<Loans> data);
}
