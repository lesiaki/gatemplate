package com.mixtape.tz.george;

/**
 * Created by george on 5/22/15.
 */
public interface Keys {

    interface VehicleKeys {

        /*The key Below are used to set Variable in Custome Row In Recycle View, Key found in SpinningFragment*/

        String KEY_JSON_ = "rooms";


        String KEY_CACHED_ID = "id";
        String KEY_CACHED_USERID = "user_id";
        String KEY_CACHED_CREATED = "created";
        String KEY_CACHED_TTL = "ttcl";

        String KEY_JSON_ERROR = "error";
        String KEY_JSON_ERROR_STATUS_CODE = "statusCode";
        String KEY_JSON_ERROR_NAME = "name";
        String KEY_JSON_ERROR_MESSAGE = "message";
        String KEY_JSON_ERROR_CODE = "code";
        String KEY_JSON_ERROR_STACK = "stack";

        String KEY_JSON_EXISTS = "exists";
        String KEY_JSON_ID = "id";
        String KEY_JSON_SENT = "sent";

        String KEY_JSON_USER_ID = "userId";
        String KEY_JSON_TTL = "ttl";
        String KEY_JSON_CREATED = "created";
        String KEY_JSON_OTP_VERIFIED = "otpVerified";
        String KEY_CACHED_PHONE_NUMBER = "phone_number";
        String KEY_CACHED_FIREBASE_TOKEN = "firebaseToken";
        String KEY_JSON_PHONE_VERIFIED = "phoneVerified";

        String KEY_JSON_AMOUNT = "amount";
        String KEY_JSON_INTEREST_RATE = "interestRate";
        String KEY_JSON_GRACE_PERIOD = "gracePeriod";
        String KEY_JSON_DAILY_PENALTY = "dailyPenalty";
        String KEY_JSON_STATUS = "status";
        String KEY_JSON_MODIFIED = "modified";

        String TABLE_PHONEBOOK = "phonebook";
        String KEY_DB_ID = "db_id";
        String KEY_JSON_USERNAME = "username";
        String KEY_JSON_PHONE_NUMBER = "phone_number";


        String DB_NAME_PHNBK = "db_phnbk";
        int DB_VERSION = 1;
        String KEY_CACHED_FNAME = "fname";
        String KEY_CACHED_LNAME = "lname";
        String KEY_JSON_DATE = "date";
        String KEY_JSON_CHANNEL_ID = "channelId";
        String KEY_JSON_PAYMENT_REF = "paymentRef";
        String KEY_JSON_PROVIDER_REF = "providerRef";


        String KEY_JSON_AMOUNT_DUE = "amountDue";
        String KEY_JSON_AMOUNT_PAID = "amountPaid";
        String KEY_JSON_PENALTY_AMOUNT = "penaltyAmount";
        String KEY_JSON_DATE_DUE = "dateDue";
        String KEY_JSON_DATE_PAID = "datePaid";
        String KEY_JSON_FNAME = "firstName";
        String KEY_JSON_LNAME = "lastName";
        String KEY_JSON_RESPONSE="response";
        String KEY_JSON_PHONE = "phone";
        String KEY_JSON_NAME = "name";
        String KEY_JSON_GUARANTER_ID = "guarantorId";
        String KEY_JSON_GUARANTOR = "guarantor";
        String KEY_JSON_LOAN_GUARANTEE = "loanGurantees";
        String KEY_JSON_LOAN_ID = "loanId";
        String KEY_JSON_ACCOUNT_NUMBER = "accountNumber";
        String KEY_JSON_ACCOUNT_BALANCE = "accountBalance";
        String KEY_JSON_GUARANTER_BALANCE = "guaranteeBalance";

        String KEY_JSON_LOAN = "loan";
        String KEY_JSON_MEMBER_ID = "memberId";
        String KEY_JSON_RESULT = "result";
        String KEY_JSON_FIREBASE_TOKEN = "firebaseToken";
        String KEY_CACHED_OTP = "otp";
        String KEY_CACHED_PASSWORD = "password";
        String KEY_CACHED_EMAIL = "email";
        String KEY_JSON_EMAIL = "email";
        String KEY_PLN_PHN = "phone";
        String ACK = "ack";
        String CODE = "code";
        String KEY_JSON_CODE = "code";
        String KEY_JSON_MESSAGE = "message";
        String KEY_JSON_RESPONSE_CODE = "responseCode";
    }

}
